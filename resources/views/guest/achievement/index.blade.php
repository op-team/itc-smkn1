@extends('guest.layouts.app')

@section('head_title') Prestasi @endsection

@section('content_title') Prestasi @endsection
@section('content_title') Semua @endsection

@section('content')

    <e class="content">
        <br />
    </e>
    <div class="columns is-multiline">
        @each('guest.achievement.list_item', $achievements, 'achievement', 'guest.achievement.empty')
    </div>

@endsection

@section('page_footer')
    {{ $achievements->links('guest.components.pagination') }}
@endsection

@push('scripts')
@endpush