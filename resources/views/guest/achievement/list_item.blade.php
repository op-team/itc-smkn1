<div class="column is-4">
    <div class="card">
        @if(count($achievement->media) > 0)
            <div class="card-image">
                <figure class="image is-4by3">
                    <img src="{{ $achievement->media()->first() }}" alt="">
                </figure>
            </div>
        @endif
        <div class="card-header">
            <h1 class="card-header-title">
                <a href="{{ route('guest.achievements.detail', ['slug_post' => $achievement->slug]) }}"><strong>{{ $achievement->title }}</strong></a>
            </h1>
        </div>
        <div class="card-content">
            <div class="content">
                {{ substr(strip_tags($achievement->body), 0, 80) . '. . .' }}
            </div>
        </div>
    </div>
</div>