@extends('guest.layouts.app')

@section('head_title') Prestasi - {{ $achievement->title }} @endsection
@section('content_title') {{ $achievement->title }} @endsection
@section('content_subtitle') <br /> <small>pada {{ $achievement->published_at }}</small> @endsection


@section('content')
    <br />
    <br />

    <p>
        {!! nl2br(stripcslashes($achievement->body)) !!}
    </p>

    <div class="card-footer">
        <br />
        <div class="card-content">
            <h4>Komentar</h4>
            <br />
            @component('guest.components.comment')
            @endcomponent
            @component('guest.components.comment')
            @endcomponent
        </div>
    </div>
@endsection