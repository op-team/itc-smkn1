<footer class="footer is-dark" style="margin-top: -40px;background: #2b5876;
background: -webkit-linear-gradient(to left, #2b5876 , #4e4376);
background: linear-gradient(to left, #2b5876 , #4e4376);">
    <div class="container footer custom-footer">
        <div class="content is-dark">
            <br /><br /><br />
            <div class="columns text-light">
                <div class="column">
                    <strong class="text-light">Laman Terkait</strong>
                    <ul>
                        <li><a href="{{ route('guest.members') }}" class="text-light">Anggota</a></li>
                        <li><a href="{{ route('guest.faqs') }}" class="text-light">Faqs</a></li>
                        <li><a href="{{ route('guest.contacts') }}" class="text-light">Kontak Kami</a></li>
                    </ul>
                </div>
                <div class="column">
                    <br />
                    <p class="text-light">Dibuat dengan &#9825; oleh anggota IT Club SMK Negeri 1 Surabaya</p>
                    <p class="text-light">
                        Copyright <strong><a href="{{ url('/')  }}" >IT Club SMK Negeri 1 Surabaya 2017 </a></strong>&copy; All Rights Reserved
                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>
