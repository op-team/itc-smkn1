<div>
    <div class="navbar-gradient"></div>
    <nav class="nav">
        <div class="container">
            <div class="nav-left">
                <a href="{{ route('guest.home') }}" class="nav-item">IT Club</a>
            </div>
            <div class="nav-center">
                <a href="{{ route('guest.posts') }}" class="nav-item is-tab {{ set_active('posts') }}">Tulisan</a>
                <a href="{{ route('guest.schedules') }}" class="nav-item is-tab {{ set_active('schedules') }}">Jadwal</a>
                <a href="{{ route('guest.galleries') }}" class="nav-item is-tab {{ set_active('galleries') }}">Galeri</a>
                <a href="{{ route('guest.achievements') }}" class="nav-item is-tab {{ set_active('achievements') }}">Prestasi</a>
            </div>
            <div class="nav-right nav-menu">
            @if(auth()->check() && auth()->user()->isMember())
            <span class="nav-item">
                <a href="{{ route('member.posts.create.form') }}" class="is-tab {{ set_active('member.posts.create.form') }}">
                    <span>Tulis cerita</span>
                </a>
            </span>
            <span class="nav-item">
                <a href="{{ route('member.dashboard.index') }}" class="is-tab {{ set_active('member.dashboard.index') }}">
                    <span>{{ auth()->user()->name }}</span>
                </a>
            </span>

            @else
            <span class="nav-item">
                <a href="{{ route('auth.member.login.page') }}" class="is-tab {{ set_active('auth') }}">
                    <span>Masuk</span>
                    <span class="icon">
                        <i class="fa fa-angle-right"></i>
                    </span>
                </a>
            </span>
            @endif
            </div>
        </div>
    </nav>
</div>