<nav class="nav is-transparent">
    <div class="container">
        <div class="nav-left">
            <a href="{{ route('guest.home') }}" class="nav-item text-light">IT Club</a>
        </div>
        <div class="nav-center">
            <a href="{{ route('guest.posts') }}" class="nav-item text-light">Tulisan</a>
            <a href="{{ route('guest.schedules') }}" class="nav-item text-light">Jadwal</a>
            <a href="{{ route('guest.galleries') }}" class="nav-item text-light">Galeri</a>
            <a href="{{ route('guest.achievements') }}" class="nav-item text-light">Prestasi</a>
        </div>
        <div class="nav-right nav-menu">
            @if(auth()->check() && auth()->user()->isMember())
                <span class="nav-item">
                    <a href="{{ route('member.posts.create.form') }}" class="text-light {{ set_active('member.posts.create.form') }}">
                        <span>Tulis cerita</span>
                    </a>
                </span>
                <span class="nav-item">
                <a href="{{ route('member.dashboard.index') }}" class="text-light {{ set_active('auth') }}">
                    <span>{{ auth()->user()->name }}</span>
                </a>
                </span>
            @else
                <span class="nav-item">
                <a href="{{ route('auth.member.login.page') }}" class="text-light {{ set_active('auth') }}">
                    <span>Masuk</span>
                    <span class="icon">
                        <i class="fa fa-angle-right"></i>
                    </span>
                </a>
            </span>
            @endif
        </div>
    </div>
</nav>