<head>
    <title>@yield('head_title')</title>

    <meta>
    <meta name="url" content="{{ url('/') }}">
    <meta name="author" content="IT CLUB SMKN 1 Surabaya">
    <meta name="viewport" width="width=device-width, initial-scale=1.0">
    @stack('meta')

    <link rel="stylesheet" href="{{ url(mix('css/app.css')) }}">
    @stack('styles')
</head>