<!DOCTYPE html>
<html lang="id">
@include('guest.layouts.partials.html_header')
<body>
    @include('guest.layouts.partials.navbar')

    <section class="section" style="transform: skewY(-2deg);background-color: #fff">
        <div class="container" style="background: transparent; transform: skewY(2deg)">
            <div class="box">
                <div class="heading">
                    <div class="content is-medium">
                        <div class="columns">
                            <div class="column">
                                <h1 class="is-marginless" style="text-transform: none"><strong>@yield('content_title')</strong><small style="text-transform: none">@yield('content_subtitle')</small></h1>
                            </div>
                            <div class="column">
                                @include('guest.components.search_bar')
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content">
                    @yield('content')
                </div>
                <div class="clearfix"></div>
            </div>
            @yield('page_footer')
        </div>
    </section>


    @include('guest.layouts.partials.footer')
    @include('guest.layouts.partials.scripts')
</body>
</html>