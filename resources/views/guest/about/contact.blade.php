@extends('guest.layouts.app')

@section('head_title') Kontak Kami @endsection

@section('content_title') Kontak @endsection

@push('styles')
<style>
    .contact {
        margin: 3rem 0;
    }
</style>
@endpush

@section('content')
    <div class="content">
        <div class="contact">
            <h4><strong>{{ "Kontak" }}</strong></h4>
            <p>{{ $contact }}</p>
        </div>
    </div>
@endsection