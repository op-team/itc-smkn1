@extends('guest.layouts.app')

@section('head_title') Frequently Asked Question @endsection

@section('content_title') Frequently Asked Question @endsection

@push('styles')
<style>
    .faqs {
        margin: 4rem 0;
    }
</style>
@endpush

@section('content')
<div class="content">
    @foreach($faqs as $key => $faq)
    <div class="faqs">
        <h4><strong>{{ $faq->question }}</strong></h4>
        <p>{{ $faq->answer }}</p>
    </div>
    <hr>
    @endforeach
</div>
@endsection