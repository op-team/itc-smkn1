@extends('guest.layouts.app')

@section('head_title') Semua Jadwal @endsection

@section('content_title') Jadwal @endsection
@section('content_subtitle') Semua @endsection

@push('styles')
<style>
    .card-image:hover {
        cursor: pointer !important;
    }
</style>
@endpush

@section('content')

    @include('guest.components.tags')

    <div class="columns is-multiline">

        @each('guest.schedule.list_item', $schedules, 'schedule', 'guest.schedule.empty')

    </div>

@endsection

@section('page_footer')
    {{ $schedules->links('guest.components.pagination') }}
@endsection

@push('scripts')
<script>
    function redirect(url) {
        window.location = url;
    }
</script>
@endpush