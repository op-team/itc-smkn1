<div class="column is-8 is-offset-2">
    <div class="card">
        <div class="card-content">
            <div class="is-pulled-right">
                <article class="message is-primary">
                    <div class="message-body">
                        <strong>{{ $schedule->end_date->diffForHumans() }}</strong>
                    </div>
                </article>
            </div>
            <h1>
                <a href="{{ route('guest.schedules.detail', ['slug' => $schedule->slug]) }}"><strong>{{ $schedule->title }}</strong></a>
            </h1>
            <p>{{ $schedule->created_at }}</p>
            <br>
            <div class="content brand">
                {{ substr(strip_tags($schedule->body), 0, 80) . '. . .' }}
            </div>
        </div>
    </div>
</div>