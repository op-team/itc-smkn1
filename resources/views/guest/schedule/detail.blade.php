@extends('guest.layouts.app')

@section('head_title') Nama kegiatan @endsection
@section('content_title') Nama kegiatan @endsection
@section('content_subtitle') <br /> <small>Dibuat pada {{ $schedule->created_at }}</small> @endsection

@section('content')
    <br />
    <div class="is-pulled-right" style="margin-top: -9%">
        <article class="message is-primary">
            <div class="message-body">
                <strong>{{ $schedule->end_date->diffForHumans() }}</strong>
            </div>
        </article>
    </div>
    <br />
    <br />
    {{ e($schedule->body) }}
    <br /> <br />
    <div class="card-footer">
        <div class="card-content">
            <h4>Komentar</h4>
            <article class="media">
                <figure class="media-left">
                    <p class="image is-64x64">
                        <img src="http://bulma.io/images/placeholders/128x128.png">
                    </p>
                </figure>
                <div class="media-content">
                    <div class="content">
                        <p>
                            <strong>John Smith</strong> <small>@johnsmith</small> <small>31m</small>
                            <br>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros, eu pellentesque tortor vestibulum ut. Maecenas non massa sem. Etiam finibus odio quis feugiat facilisis.
                        </p>
                    </div>
                    <nav class="level is-mobile">
                        <div class="level-left">
                            <a class="level-item">
                                <span class="icon is-small"><i class="fa fa-reply"></i></span>
                            </a>
                            <a class="level-item">
                                <span class="icon is-small"><i class="fa fa-retweet"></i></span>
                            </a>
                            <a class="level-item">
                                <span class="icon is-small"><i class="fa fa-heart"></i></span>
                            </a>
                        </div>
                    </nav>
                </div>
                <div class="media-right">
                    <button class="delete"></button>
                </div>
            </article>
        </div>
    </div>
@endsection