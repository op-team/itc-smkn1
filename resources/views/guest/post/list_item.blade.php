<div class="column is-8 is-offset-2">
    <div class="card">
        @if (count($post->media) > 0)
            <div class="card-image" onclick="redirect('{{ route('guest.posts.detail', ['slug' => $post->slug]) }}')">
                <figure class="image is-2by1" style="background: url('{{ $post->media()->first()['src'] }}') no-repeat center;background-size: cover;">
                </figure>
            </div>
        @endif
        <div class="card-content">
            <h1>
                <a href="{{ route('guest.posts.detail', ['slug' => $post->slug]) }}"><strong>{{ $post->title }}</strong></a>
            </h1>
            <p>oleh {{ $post->author->name }} pada {{ $post->published_at }}</p>
            <br>
            <div class="content brand">
                {{ substr(strip_tags($post->body), 0, 80) . '. . .' }}
            </div>
        </div>
    </div>
</div>