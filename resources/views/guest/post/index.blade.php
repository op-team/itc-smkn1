@extends('guest.layouts.app')

@section('head_title') Semua Post @endsection

@section('content_title') Post @endsection
@section('content_subtitle') Semua @endsection

@push('styles')
<style>
    .card-image:hover {
        cursor: pointer !important;
    }
</style>
@endpush

@section('content')

    @include('guest.components.tags')

    <div class="columns is-multiline">

        @each('guest.post.list_item', $posts, 'post', 'guest.post.empty')

    </div>
@endsection

@section('page_footer')
    {{ $posts->links('guest.components.pagination') }}
@endsection

@push('scripts')
<script>
    function redirect(url) {
        window.location = url;
    }
</script>
@endpush