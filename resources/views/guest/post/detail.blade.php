@extends('guest.layouts.app')

@section('head_title') Tulisan - {{ $post->title }} @endsection
@section('content_title') {{ $post->title }} @endsection
@section('content_subtitle') <br /><small>Ditulis oleh <a href="{{ route('guest.members.detail', ['username' => $post->author->username ]) }}">{{ $post->author->name }}</a> pada {{ $post->published_at }}</small> @endsection

@section('content')
    @can('update', $post)
    <div class="is-pulled-right" style="margin-top: -9%">
        <article class="message is-primary">
            <div class="message-body">
                <a href="{{ route('member.posts.edit.form', ['slug_post' => $post->slug]) }}">
                    <strong> Edit</strong>
                </a>
            </div>
        </article>
    </div>
    @endcan
    <br /> <br>

    <p>
        {!! nl2br(stripcslashes($post->body)) !!}
    </p>

    <br />
    <div class="card-footer">
        <div class="card-content">
            <h4>Komentar</h4>
            <article class="media">
                <figure class="media-left">
                    <p class="image is-64x64">
                        <img src="http://bulma.io/images/placeholders/128x128.png">
                    </p>
                </figure>
                <div class="media-content">
                    <div class="content">
                        <p>
                            <strong>John Smith</strong> <small>@johnsmith</small> <small>31m</small>
                            <br>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros, eu pellentesque tortor vestibulum ut. Maecenas non massa sem. Etiam finibus odio quis feugiat facilisis.
                        </p>
                    </div>
                    <nav class="level is-mobile">
                        <div class="level-left">
                            <a class="level-item">
                                <span class="icon is-small"><i class="fa fa-reply"></i></span>
                            </a>
                            <a class="level-item">
                                <span class="icon is-small"><i class="fa fa-retweet"></i></span>
                            </a>
                            <a class="level-item">
                                <span class="icon is-small"><i class="fa fa-heart"></i></span>
                            </a>
                        </div>
                    </nav>
                </div>
                <div class="media-right">
                    <button class="delete"></button>
                </div>
            </article>
        </div>
    </div>
@endsection