@extends('guest.layouts.app')

@section('head_title') Masuk sebagai anggota @endsection

@section('content_title') Masuk @endsection

@section('content')
    <div class="hero is-small">
        <div class="hero-body">
            <div class="columns">
                <div class="column is-5 is-offset-4">
                    <form action="{{ route('auth.member.login') }}" method="post">
                        {{ csrf_field() }}
                        <div class="field">
                            <label class="label">Nama pengguna</label>
                            <p class="control has-icon">
                                <input type="text" class="input {{ set_error('username') }}" name="username" placeholder="Nama pengguna">
                                <span class="icon">
                                    <i class="fa fa-user-circle"></i>
                                </span>
                            </p>
                            {!! get_error('username') !!}
                        </div>
                        <br />
                        <div class="field">
                            <label class="label">Kata sandi</label>
                            <p class="control has-icon">
                                <input type="password" class="input {{ set_error('password') }}" placeholder="Kata sandi" name="password">
                                <span class="icon">
                                    <i class="fa fa-key"></i>
                                </span>
                            </p>
                            {!! get_error('password') !!}
                        </div>
                        <br />
                        <div class="field">
                            <div class="control">
                                <label class="checkbox">
                                    <input type="checkbox">
                                    Ingat Saya
                                </label>
                            </div>
                        </div>
                        <div class="field pull-right">
                            <button class="button is-info">
                                <span class="icon">
                                    <i class="fa fa-sign-in"></i>
                                </span> <span>Masuk</span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection