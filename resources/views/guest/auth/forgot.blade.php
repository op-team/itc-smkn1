@extends('guest.layouts.app')

@section('head_title') Lupa Kata Sandi @endsection

@section('content_title') Lupa Kata Sandi @endsection

@section('content')
    <div class="hero is-small">
        <div class="hero-body">
            <div class="columns">
                <div class="column is-5 is-offset-4">
                    <form action="">
                        {{ csrf_field() }}
                        <div class="field">
                            <label class="label">Email</label>
                            <p class="control has-icon">
                                <input type="text" class="input" name="email" placeholder="Email">
                                <span class="icon">
                                    <i class="fa fa-user-circle"></i>
                                </span>
                            </p>
                        </div>
                        <br />
                        <div class="field pull-right">
                            <button class="button is-info">
                                <span class="icon">
                                    <i class="fa fa-sign-in"></i>
                                </span> <span>Kirim</span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection