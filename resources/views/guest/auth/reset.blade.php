@extends('guest.layouts.app')

@section('head_title') Atur Ulang Kata Sandi @endsection

@section('content_title') Atur Ulang Kata Sandi @endsection

@section('content')
    <div class="hero is-small">
        <div class="hero-body">
            <div class="columns">
                <div class="column is-5 is-offset-4">
                    <form action="">
                        {{ csrf_field() }}
                        <div class="field">
                            <label class="label">Kata Sandi</label>
                            <p class="control has-icon">
                                <input type="password" class="input" name="password" placeholder="Kata Sandi">
                                <span class="icon">
                                    <i class="fa fa-key"></i>
                                </span>
                            </p>
                        </div>
                        <br />
                        <div class="field">
                            <label class="label">Ketik Ulang Kata Sandi</label>
                            <p class="control has-icon">
                                <input type="password" class="input" name="password_confirmation" placeholder="Konfirmasi Kata Sandi">
                                <span class="icon">
                                    <i class="fa fa-key"></i>
                                </span>
                            </p>
                        </div>
                        <br />
                        <div class="field pull-right">
                            <button class="button is-info">
                                <span class="icon">
                                    <i class="fa fa-sign-in"></i>
                                </span><span>Ubah kata sandi</span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection