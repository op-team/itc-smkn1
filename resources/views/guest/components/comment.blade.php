<article class="media">
    <figure class="media-left">
        <p class="image is-64x64">
            <img src="{{ "http://img03.deviantart.net/cc6b/i/2013/319/5/b/male_cartoon_avatar_by_ahninniah-d6ib8p2.png" }}">
        </p>
    </figure>
    <div class="media-content">
        <div class="content">
            <p>
                <strong>{{ "Nama Orangnya" }}</strong> <small>{{ "Tanggalnya" }}</small>
                <br>
                {{ "Ini comment loh" }}
            </p>
        </div>
        <nav class="level is-mobile">
            <div class="level-left">
                <a class="level-item">
                    <span class="icon is-small"><i class="fa fa-reply"></i></span>
                </a>
                <a class="level-item">
                    <span class="icon is-small"><i class="fa fa-retweet"></i></span>
                </a>
                <a class="level-item">
                    <span class="icon is-small"><i class="fa fa-heart"></i></span>
                </a>
            </div>
        </nav>
    </div>
    <div class="media-right">
        <button class="delete"></button>
    </div>
</article>
