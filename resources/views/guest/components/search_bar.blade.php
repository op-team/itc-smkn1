<form action="">
    <div class="field has-addons has-addons-right">
        <p class="control">
            <input class="input" type="text" name="search" placeholder="Cari" {{ \Request::input('search', '') }}>
        </p>
        <p class="control">
            <a class="button is-primary has-icon">
                <span class="icon">
                    <i class="fa fa-search"></i>
                </span>
            </a>
        </p>
    </div>
</form>