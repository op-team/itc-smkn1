@if($paginator->hasPages())
<nav class="pagination is-info">
    <ul class="pagination-list">

        {{-- Previous Link --}}
        @if($paginator->onFirstPage())
            <li><a class="pagination-previous button is-info" disabled>&lt;</a></li>
        @else
            <li><a href="{{ $paginator->previousPageUrl() }}" class="pagination-previous button is-info">&lt;</a></li>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li><span class="pagination-ellipsis">&hellip;</span></li>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li><a class="pagination-link is-current">{{ $page }}</a></li>
                    @else
                        <li><a class="pagination-link" href="{{ $url }}">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach


        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li><a href="{{ $paginator->nextPageUrl() }}" class="pagination-next button is-info">&gt;</a></li>
        @else
            <li><a class="pagination-next button is-info" disabled>&gt;</a></li>
        @endif
    </ul>
</nav>      
@endif