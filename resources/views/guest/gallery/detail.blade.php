@extends('guest.layouts.app')

@section('head_title') Galeri - Judul galerinya @endsection

@push('styles')
<link rel="stylesheet" href="{{ url('/css/lightgallery.min.css') }}">
<style>
    .gallery-image {
        max-height: 320px;
    }
</style>
@endpush

@section('content_title') {{ $gallery->title }} @endsection
@section('content_subtitle') {{ $gallery->created_at }} @endsection

@section('content')
    <div class="content">
        <br />
        {!! $gallery->body !!}
    </div>
    <div id="gallery" class="columns is-multiline gallery">
        @each('guest.gallery.photos.list_item', $gallery->media, 'photo', 'guest.gallery.photos.empty')
    </div>
@endsection

@push('scripts')
<script src="{{ url('/js/lightgallery.min.js') }}"></script>
<script src="{{ url('/js/lg-thumbnail.min.js') }}"></script>
<script src="{{ url('/js/lg-zoom.min.js') }}"></script>
<script>
    lightGallery(document.getElementById('gallery'), {
        thumbnail: true
    });
</script>
@endpush