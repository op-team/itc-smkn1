<div class="column is-4">
    <div class="card">
        @if($gallery->has('media')->first())
            <div class="card-image">
                <figure class="image is-4by3">
                    <img src="{{ $gallery->media()->first()->src }}" alt="">
                </figure>
            </div>
        @endif
        <div class="card-header">
            <h1 class="card-header-title">
                <a href="{{ route('guest.galleries.detail', ['slug_gallery' => $gallery->slug]) }}"><strong>{{ $gallery->title }}</strong></a>
            </h1>
        </div>
        <div class="card-content">
            <div class="content brand">
                {!! $gallery->body !!}
            </div>
        </div>
    </div>
</div>