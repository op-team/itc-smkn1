@extends('guest.layouts.app')

@section('head_title') Galeri @endsection

@section('content_title') Album @endsection
@section('content_title') Semua @endsection

@section('content')
    <div class="content">
        <br />
    </div>
    <div class="columns is-multiline">

        @each('guest.gallery.list_item', $galleries, 'gallery', 'guest.gallery.empty')

    </div>
@endsection

@section('page_footer')
    {{ $galleries->links('guest.components.pagination') }}
@endsection

@push('scripts')
@endpush