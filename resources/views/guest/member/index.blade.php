@extends('guest.layouts.app')

@section('head_title') Anggota IT Club @endsection

@section('content_title') Anggota @endsection
@section('content_subtitle') Semua @endsection

@section('content')
    <br /><br />
    <div class="columns is-multiline">
        @each('guest.member.list_item', $members, 'member', 'guest.member.empty')
    </div>
@endsection

@section('page_footer')
    {{ $members->links('guest.components.pagination') }}
@endsection