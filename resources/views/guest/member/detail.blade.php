@extends('guest.layouts.app')

@section('content')
    <div class="columns">
        @include('guest.member.aside')
        <div class="column">
            <div class="content">
                <h1><strong>Tulisan</strong></h1>
            </div>
            <div class="columns is-multiline">
                @if(count($member->posts) > 0)
                    @foreach($member->posts as $post)
                    <div class="column is-10 is-offset-1">
                        <div class="card">
                            @if($post->has('media')->first())
                            <div class="card-image" onclick="redirect('{{ route('guest.posts.detail', ['slug' => $post->slug]) }}')">
                                <figure class="image is-2by1" style="background: url('http://www.gettyimages.com/gi-resources/images/Embed/new/embed2.jpg') no-repeat center;background-size: cover;">
                                </figure>
                            </div>
                            @endif
                            <div class="card-content">
                                <h1>
                                    <a href="{{ route('guest.posts.detail', ['slug' => $post->slug]) }}"><strong>{{ $post->title }}</strong></a>
                                </h1>
                                <p>Oleh <a href="{{ route('guest.members.detail', ['username' => $member->username]) }}">{{ $member->name }}</a> pada {{ $post->published_at }}</p>
                                <br>
                                <div class="content brand">
                                    {{ substr(strip_tags(e($post->body)), 0, 120) . ' . . .'}}
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                @else
                    <div class="column">
                        <div class="content has-text-centered">
                            Anggota belum memiliki karya
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection