<div class="column is-half">
    <div class="box">
        <article class="media">
            <figure class="media-left">
                <a href="{{ route('guest.members.detail', ['username' => $member->username]) }}">
                    <p class="image is-64x64">
                        <img src="{{ $member->avatar }}">
                    </p>
                </a>
            </figure>
            <div class="media-content">
                <div class="content">
                    <p>
                        <strong><a href="{{ route('guest.members.detail', ['username' => $member->username]) }}">{{ $member->name }}</a></strong>
                        <br/>
                        <span>{{ $member->grade . ' ' . $member->major->name . ' ' . $member->class_number }}</span>
                    </p>
                </div>
            </div>
        </article>
    </div>
</div>