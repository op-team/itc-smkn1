<div class="column is-3">
    <figure class="image is-128x128 is-circled" style="margin-left: auto;margin-right: auto">
        <img src="{{ $member->avatar }}" alt="">
    </figure>
    <h1 class="has-text-centered"><a href="{{ route('guest.members.detail', ['username' => $member->username]) }}">{{ $member->name }}</a></h1>
    <div class="box has-text-centered">
        <h3>{{ count($member->present) }}</h3>
        Kehadiran
    </div>
    <div class="box has-text-centered">
        <h3>{{ $member->posts()->count() }}</h3>
        Tulisan
    </div>
    <div class="box has-text-centered">
        <h3>9</h3>
        Prestasi
    </div>
</div>