    <!DOCTYPE html>
    <html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>IT CLUB - SMKN 1 Surabaya</title>

        <link rel="stylesheet" type="text/css" href="{{ url(mix('css/app.css')) }}">
        <link rel="stylesheet" href="{{ url('/css/flickity.min.css') }}">
        <style>
            body {
                font-size: 1.15em;
            }
            .nav-item:hover {
                color: #E0E0E0 !important;
            }
            .geometric {
                transform: skewY(-4deg);
                margin-top: -150px;
                height: 120vh;
                padding: 200px;
                background: #141E30; /* fallback for old browsers */
                background: -webkit-linear-gradient(to left, #141E30 , #243B55); /* Chrome 10-25, Safari 5.1-6 */
                background: linear-gradient(to left, #141E30 , #243B55); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
            }

            .landing-overlay {
                background: transparent !important;
            }
            .geometric-details {
                transform: skewY(-4deg);
                background: #ECE9E6; /* fallback for old browsers */
                background: -webkit-linear-gradient(to left, #ECE9E6 , #FFFFFF); /* Chrome 10-25, Safari 5.1-6 */
                background: linear-gradient(to left bottom, #ECE9E6 , #FFFFFF); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
            }
            .geometric-prestasi {
                transform: skewY(2deg);
                background: #E0EAFC; /* fallback for old browsers */
                background: -webkit-linear-gradient(to left, #E0EAFC , #CFDEF3); /* Chrome 10-25, Safari 5.1-6 */
                background: linear-gradient(to left, #E0EAFC , #CFDEF3); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
            }
            .carousel img {
                display: block;
                height: 300px;
            }

        </style>
    </head>
    <body>
        <section id="intro">
            <div class="hero is-fullheight geometric"></div>
            <div class="hero is-fullheight is-overlay landing-overlay text-light">
                <header>
                    @include('guest.layouts.partials.landing_navbar')
                </header>
                <div class="hero-body">
                    <div class="container">
                        <div class="columns">
                            <div class="column is-12-mobile is-12-mobile has-text-centered">
                                <h1 class="text-light text-title brand thin" style="letter-spacing: 10px">IT CLUB</h1>
                                <h2 class="text-title text-light brand thin" style="opacity:.5">Be the Future Leader</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="has-text-centered is-hidden-mobile" style="margin-top: -200px">
            <div class="marvel-device macbook" style="z-index: 4;transform: scale(0.7)">
                <div class="top-bar"></div>
                <div class="camera"></div>
                <div class="screen">
                    <iframe src="https://docs.google.com/forms/d/e/1FAIpQLSdbzdNooxinfxLwitKfPB0kT0UXJygB1gjy0QLfuP7NtJ71ng/viewform?embedded=true"
                            width="960" height="600" frameborder="0" marginheight="0" marginwidth="0">
                        Memuat...
                    </iframe>
                </div>
                <div class="bottom-bar"></div>
            </div>
        </div>

    <section id="details">
        <div class="container">
            <div class="columns">
                <div class="column">
                    <div class="column is-8">
                        <div class="hero is-medium">
                            <div class="hero-body" style="padding-top:0">
                                <div class="container">
                                    <h1 class="text-title brand thin">Apa itu <span class="text-title brand">IT Club</span>?</h1>
                                    <br>
                                    <p class="justify letter-spacing-1">
                                        IT CLUB adalah sebuah komunitas bagi pelajar SMK Negeri 1 Surabaya yang ingin mengembangkan pengetahuan dan keterampilan seputar teknologi dan informasi masa kini mulai dari alat dengan teknologi terbaru, bahasa pemrograman terupdate, sampai dengan editing gambar &amp; video kekinian.
                                    </p><br>
                                    <p class="justify letter-spacing-1">
                                        Semuanya dipelajari mulai dari dasar sampai mahir dengan pendampingan oleh Mentor - Mentor dari Dunia Usaha / Dunia Industri (DU/DI), Guru - Guru di bidang IT, Alumni IT CLUB dan Kakak kelas.
                                    </p><br>
                                    <p class="justify letter-spacing-1">
                                        Pendampingan dilakukan mulai dari proses pembelajaran, sharing ide dan knowledge, pembuatan produk, sampai dengan pemasaran produk yang dihasilkan secara berkelompok. IT CLUB menjadi wadah untuk pelajar mempelajari teknologi terbaru, mengembangkannya, dan berkontribusi untuk masyarakat.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="keren">
        <div class="hero is-medium geometric-details">
            <div class="hero-body" style="transform: skewY(4deg)">
                <div class="container">
                    <div class="columns">
                        <div class="column is-8">
                            <h1 class="text-title brand thin">Hal keren apa di <span class="text-title brand">IT Club</span>?</h1>
                            <br />
                            <p class="justify letter-spacing-1">
                                <ol>
                                    <li>Pembagian Divisi untuk menyesuaikan minat belajar dan juga untuk mengasah bakat anggota. Pada awal masuk IT CLUB, anggota boleh mengikuti 2 atau lebih sekaligus divisi agar bisa beradaptasi. IT CLUB sendiri memiliki 4 divisi, antara lain :</li><br>
                                    <ul style="padding-left:1em;list-style-type: circle">
                                        <li><strong>Programming</strong> (Pemrograman Web, Android, Desktop; Struktur logika pemrograman; Analisis sistem)</li>
                                        <li><strong>Networking</strong> (Pola jaringan; Membangun server sendiri; Keamanan data)</li>
                                        <li><strong>Design &amp; Animasi</strong> (Gambar dan Animasi 2D &amp; 3D; Design Layouting Aplikasi; Prototyping)</li>
                                        <li><strong>Videografi</strong> (Pembuatan Film; Editing Video; Tips &amp; Trick Videografi terbaru)</li>
                                    </ul>
                                    <br>
                                    <li>Workshop dari berbagai divisi, mulai dari Programming, Networking, Design &amp; Animasi dan Videografi. Workshop diisi oleh internal sampai dengan melibatkan pemateri dari alumni dan Dunia Usaha / Dunia Industri (DU/DI). Workshop biasanya dilakukan minimal 1 bulan sekali.</li><br>
                                    <li>Ruangan belajarnya ber-AC. Hehe.</li><br>
                                    <li>Diadakannya Program Inkubasi Bisnis &amp; Inovasi untuk membantu anggota membuat produk yang mempunyai impact besar dalam masyarakat mulai dari proses pembekalan, perencanaan ide, produksi, dan pemasaran. Pembimbing dan Mentor diisi oleh Sekolah, Dunia Usaha / Dunia Industri (DU/DI), dan Startup - Startup teknologi.</li><br>
                                <li>Dukungan mengikuti kompetisi - kompetisi di bidang teknologi mulai dari tingkat antar sekolah, kota, provinsi sampai dengan nasional.</li>
                                </ol>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="galeri">
        <div class="hero is-small">
            <div class="hero-body">
                <div class="container">
                    <div class="columns">
                        <div class="column has-text-centered">
                            <h1 class="text-title brand thin">Mau Liat Kegiatannya Member <span class="text-title brand">IT Club </span>?</h1>
                            <br /><br />
                            <div class="carousel">
                                @foreach($photos as $photo)
                                    <img src="{{ $photo->src }}" alt="">
                                @endforeach
                            </div>
                            <br /><br /><br /><br />
                            <a href="{{ route('guest.galleries') }}" class="button is-danger is-large shadow">
                                <span class="icon fa fa-play"></span>
                                <span>Galeri IT Club</span>
                            </a>
                            <br /><br /><br /><br />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="hero is-medium geometric-prestasi">
            <div class="hero-body" style="transform: skewY(-2deg)">
                <div class="container">
                    <div class="columns">
                        <div class="column is-12">
                            <h1 class="text-title brand thin has-text-centered">Mengukir prestasi dengan <span class="text-title brand">IT Club</span></h1>
                            <br /><br /><br />
                        <div class="columns">
                            @each('guest.achievement.list_item', $achievements, 'achievement', 'guest.achievement.empty')
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('guest.layouts.partials.footer')
<script type="text/javascript" src="{{ url(mix('js/app.js')) }}"></script>
<script src="{{ url('/js/flickity.min.js') }}"></script>
<script>
    var flickity = new Flickity('.carousel', {
        imagesLoaded: true,
        cellAlign: 'left',
        contain: true
    });
</script>
</body>
</html>
