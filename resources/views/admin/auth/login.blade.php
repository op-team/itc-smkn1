<!DOCTYPE html>
<html>
<head>
	<title>Login Pengurus</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('css/admin.css') }}">
</head>
<body>

<div class="hero is-dark is-bold is-fullheight is-fullwidth">
	<div class="hero-body">
		<div class="container">
			<div class="columns">
				<div class="column is-4 is-offset-4">
					<div class="hero-title">
						<h2 class="title is-2">Masuk</h2>
						<hr>
						<h3 class="subtitle">Pengurus</h3>
					</div>
					<br>
					<form action="{{ route('auth.admin.login') }}" method="post">
			            {{ csrf_field() }}
			            <div class="field">
			                <p class="control has-icon">
			                    <input type="text" class="input {{ set_error('username') }}" name="username" placeholder="Pengguna">
			                    <span class="icon">
			                        <i class="fa fa-user-circle"></i>
			                    </span>
			                </p>
			                {!! get_error('username') !!}
			            </div>
			            <br />
			            <div class="field">
			                <p class="control has-icon">
			                    <input type="password" class="input {{ set_error('password') }}" placeholder="Kata sandi" name="password">
			                    <span class="icon">
			                        <i class="fa fa-key"></i>
			                    </span>
			                </p>
			                {!! get_error('password') !!}
			            </div>
			            <br />
			            <div class="field pull-right">
			                <button class="button is-danger">
			                    <span class="icon">
			                        <i class="fa fa-sign-in"></i>
			                    </span> <span>Masuk</span>
			                </button>
			            </div>
			        </form>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="{{ asset('js/app.js') }}"></script>

{{-- Include Sweetalert --}}
@include('sweet::alert')
</body>
</html>