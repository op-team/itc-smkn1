@extends('admin.layouts.app')
@section('title') Kontak @endsection

@section('content')
    <div class="column is-12">
        <div class="box">
            <h1 class="title">Daftar Kontak</h1>
            <div class="columns">
                <div class="column">
                    <a href="{{ route('admin.contacts.create') }}" class="button is-success">
                    <span class="icon">
                        <i class="fa fa-plus"></i>
                    </span>
                        <span>
                        Buat Kontak
                    </span>
                    </a>
                </div>
            </div>
            <table class="table is-striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Nama Kontak</th>
                    <th>Kontak</th>
                    <th width="50">Ubah</th>
                    <th width="50">Hapus</th>
                </tr>
                </thead>
                <tbody>
                @if(count($contacts) > 0)
                    @foreach($contacts as $key => $contact)
                        <tr>
                            <td>{{ ($contacts->perPage() * ($contacts->currentPage() - 1)) + ($key + 1)  }}</td>
                            <td>{{ $contact->name }}</td>
                            <td>{{ $contact->value }}</td>
                            <td>
                                <a href="{{ route('admin.contacts.edit', ['key' => $contact->key]) }}" class="button is-warning">
                                    <span class="icon">
                                        <i class="fa fa-pencil"></i>
                                    </span>
                                    <span>Ubah</span>
                                </a>
                            </td>
                            <td>
                                <form action="{{ route('admin.contacts.destroy', ['key' => $contact->key]) }}" method="post">
                                    {{ csrf_field() }}
                                    {{ method_field("delete") }}
                                    <button class="button is-danger" onclick="return confirm('Yakin menghapus data ?')">
                                        <span class="icon">
                                            <i class="fa fa-trash-o"></i>
                                        </span>
                                        <span>Hapus</span>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="5">Belum ada kontak ditambahkan</td>
                    </tr>
                @endif
                </tbody>
            </table>
            {{ $contacts->links('guest.components.pagination') }}
        </div>
    </div>
@endsection