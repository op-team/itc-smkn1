@extends('admin.layouts.app')
@section('title') Ubah Kontak @endsection

@section('content')
    <div class="column is-6">
        <div class="box">
            <h1 class="title">Ubah Kontak </h1>
            <br />
            <form action="{{ route('admin.contacts.update', ['key' => $contact->key]) }}" method="post">
                {{ csrf_field() }}
                <p class="control">
                    <label for="" class="label">Nama Kontak</label>
                    <input type="text" class="input {{ set_error('name') }}" name="name" value="{{ $contact->name }}">
                    {!! get_error('name') !!}
                </p>
                <br/>
                <p class="control">
                    <label for="" class="label">Jawaban</label>
                    <input type="text" class="input {{ set_error('value') }}" name="value" value="{{ $contact->value }}">
                    {!! get_error('value') !!}
                </p>
                <div class="field pull-right">
                    <div class="control">
                        <button class="button is-primary">
                                <span class="icon">
                                    <i class="fa fa-plus"></i>
                                </span>
                            <span>Buat</span>
                        </button>
                    </div>
                </div>
                <div class="is-clearfix"></div>
        </form>
        </div>
    </div>
@endsection