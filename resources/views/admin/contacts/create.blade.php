@extends('admin.layouts.app')
@section('title') Buat Kontak @endsection

@section('content')
    <div class="column is-6">
        <div class="box">
            <h1 class="title">Buat Kontak</h1>
            <br />
            <form action="{{ route('admin.contacts.store') }}" method="post">
                {{ csrf_field() }}
                <p class="control">
                    <label for="" class="label">Nama Kontak</label>
                    <input type="text" class="input {{ set_error('name') }}" name="name" value="{{ old('name') }}">
                    {!! get_error('name') !!}
                </p>
                <br/>
                <p class="control">
                    <label for="" class="label">Isi Kontak</label>
                    <input type="text" class="input {{ set_error('value') }}" name="value" value="{{ old('value') }}">
                    {!! get_error('value') !!}
                </p>
                <div class="field pull-right">
                    <div class="control">
                        <button class="button is-primary">
                                <span class="icon">
                                    <i class="fa fa-plus"></i>
                                </span>
                            <span>Buat</span>
                        </button>
                    </div>
                </div>
                <div class="is-clearfix"></div>
            </form>
        </div>
    </div>
@endsection