@extends('admin.layouts.app')
@section('title') Pertanyaan yang sering diajukan @endsection

@section('content')
<div class="column is-12">
    <div class="box">
        <h1 class="title">Daftar FAQs</h1>
        <div class="columns">
            <div class="column">
                <a href="{{ route('admin.faqs.create') }}" class="button is-success">
                    <span class="icon">
                        <i class="fa fa-plus"></i>
                    </span>
                    <span>
                        Buat FAQ
                    </span>
                </a>
            </div>
            <div class="column">
                @include('guest.components.search_bar')
            </div>
        </div>
        <table class="table is-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Pertanyaan</th>
                    <th>Jawaban</th>
                    <th width="50">Ubah</th>
                    <th width="50">Hapus</th>
                </tr>
            </thead>
            <tbody>
                @if(count($faqs) > 0)
                    @foreach($faqs as $key => $faq)
                        <tr>
                            <td>{{ ($faqs->perPage() * ($faqs->currentPage() - 1)) + ($key + 1)  }}</td>
                            <td>{{ $faq->question }}</td>
                            <td>{{ strlen($faq->answer < 80) ? $faq->answer : substr($faq->answer, 0, 80) . " ..." }}</td>
                            <td>
                                <a href="{{ route('admin.faqs.edit', ['faq' => $faq->id]) }}" class="button is-warning">
                                    <span class="icon">
                                        <i class="fa fa-pencil"></i>
                                    </span>
                                    <span>Ubah</span>
                                </a>
                            </td>
                            <td>
                                <form action="{{ route('admin.faqs.destroy', ['faq' => $faq->id]) }}" method="post">
                                    {{ csrf_field() }}
                                    {{ method_field("delete") }}
                                    <button class="button is-danger" onclick="return confirm('Yakin menghapus data ?')">
                                        <span class="icon">
                                            <i class="fa fa-trash-o"></i>
                                        </span>
                                        <span>Hapus</span>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="5">Belum ada faqs ditambahkan</td>
                    </tr>
                @endif
            </tbody>
        </table>
        {{ $faqs->links('guest.components.pagination') }}
    </div>
</div>
@endsection