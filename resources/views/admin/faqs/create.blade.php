@extends('admin.layouts.app')
@section('title') Buat Faqs @endsection

@section('content')
    <div class="column is-6">
        <div class="box">
            <h1 class="title">Buat FAQ</h1>
            <br />
            <form action="{{ route('admin.faqs.store') }}" method="post">
                {{ csrf_field() }}
                    <p class="control">
                        <label for="" class="label">Pertanyaan</label>
                        <input type="text" class="input {{ set_error('question') }}" name="question" value="{{ old('question') }}">
                        {!! get_error('question') !!}
                    </p>
                    <br/>
                    <p class="control">
                        <label for="" class="label">Jawaban</label>
                        <textarea name="answer" id="answer" cols="30" rows="10" class="textarea {{ set_error('answer') }}"></textarea>
                        {!! get_error('answer') !!}
                    </p>
                    <div class="field pull-right">
                        <div class="control">
                            <button class="button is-primary">
                                <span class="icon">
                                    <i class="fa fa-plus"></i>
                                </span>
                                <span>Buat</span>
                            </button>
                        </div>
                    </div>
                    <div class="is-clearfix"></div>
            </form>
        </div>
    </div>
@endsection

@push('scripts')
    @ckeditor('answer', ['skin' => 'kama'])
@endpush