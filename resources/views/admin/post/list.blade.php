<?php
/**
 * @author <yustiko404@gmail.com> 5/2/2017 3:08 PM
 */
?>

@extends('admin.layouts.app')
@section('title') Dashboard @endsection

@section('content')
    <div class="column is-12">
        <div class="box">
            <div class="content">
                <table class="table">
                    <h3>Daftar Semua Tulisan</h3>
                    @include('guest.components.search_bar')
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Judul</th>
                            <th>Isi</th>
                            <th>Penulis</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($posts as $key => $post)
                        <tr>
                            <th>{{ $posts->firstItem() + $key }}</th>
                            <td>{{ str_limit($post->title, 20) }}</td>
                            <td>{!! strip_tags(str_limit($post->body, 50)) !!}</td>
                            <td>{{ $post->author->name }}</td>
                            <td>
                                <form action="{{ route('admin.post.publish', ['slug_post' => $post->slug]) }}"
                                      method="get"> {{ csrf_field() }}
                                        <button type="submit" class="button is-info">
                                            {{ ($post->published_at) ? 'Sembunyikan' : 'Tampilkan'}}
                                        </button>
                                </form>
                            </td>
                            <td>
                                <form action="{{ route('admin.post.edit', ['slug_post' => $post->slug]) }}"
                                      method="get"> {{ csrf_field() }}
                                        <button type="submit" class="button is-primary">ubah</button>
                                </form>
                            </td>
                            <td>
                                <form action="{{ route('admin.post.destroy', ['slug_post' => $post->slug]) }}"
                                      method="post"> {{ csrf_field() }}
                                        <button type="submit" class="button is-danger">Hapus</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $posts }}
            </div>
        </div>
    </div>
@endsection