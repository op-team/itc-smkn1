@extends('admin.layouts.app')
@section('title') Foto {{ $gallery->title }} @endsection

@section('content')
    <div class="column is-12">
        <div class="box">

            <a href="{{ route('admin.galleries') }}" class="button is-info">
                <span class="icon">
                    <i class="fa fa-chevron-circle-left"></i>
                </span>
                <span>Kembali</span>
            </a>
            <br><br>

            <h1 class="title">Foto {{ $gallery->title }}</h1>
            <div class="content" id="appendTarget">
                {!! $gallery->body !!}
                <a class="button is-success" onclick="append()">
                <span class="icon">
                    <i class="fa fa-plus"></i>
                </span>
                    <span>Tambahkan Foto</span>
                </a>
            </div>
            <div class="columns is-multiline">
            @if(count($gallery->media) > 0)
                    @foreach($gallery->media()->latest()->get() as $photo)
                        <div class="column is-4">
                            <div class="card">
                                <div class="card-image">
                                    <figure class="image">
                                        <img src="{{ $photo->src }}" alt="">
                                    </figure>
                                </div>
                                <div class="card-header" style="padding: 8px">
                                    <form action="{{ route('admin.galleries.photos.remove', ['slug_gallery' => $gallery->slug, 'slug' => $photo->slug]) }}" method="post">
                                        {{ csrf_field() }}
                                        <button class="button is-danger" onclick="return confirm('Hapus Foto dari galeri ?')">
                                            <span class="icon">
                                                <i class="fa fa-close"></i>
                                            </span>
                                            <span>Hapus Foto</span>
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="column">
                        <div class="content has-text-centered">
                            Belum ada album
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script>
    function append () {
        var appendTarget = document.getElementById("appendTarget");
        appendTarget.innerHTML =
            '<form action="{{ route('admin.galleries.photos.add', ['slug_gallery' => $gallery->slug]) }}" method="post" enctype="multipart/form-data">' +
                '{{ csrf_field() }}' +
                '<p class="control">' +
                    '<label for="" class="label">Judul</label>' +
                    '<input type="file" class="input {{ set_error('images[]') }}" name="images[]" multiple>' +
                '{!! get_error('images') !!}' +
                '</p>' +
                '<button type="submit" class="button is-success">' +
                    '<span class="icon"><i class="fa fa-plus"></i></span>' +
                    '<span>Tambahkan Foto</span>' +
                '</button>' +
            '</form>'
    }
</script>
@endpush