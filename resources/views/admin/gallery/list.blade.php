@extends('admin.layouts.app')
@section('title') Semua Galeri @endsection

@section('content')
    <div class="column is-12">
        <div class="box">
            <h1 class="title">Daftar Galeri</h1>
            <div class="columns">
                <div class="column">
                    <a href="{{ route('admin.galleries.create') }}" class="button is-success">
                    <span class="icon">
                        <i class="fa fa-plus"></i>
                    </span>
                        <span>
                        Buat Galeri
                    </span>
                    </a>
                </div>
                <div class="column">
                    @include('guest.components.search_bar')
                </div>
            </div>
            <table class="table is-striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Judul</th>
                    <th>Caption</th>
                    <th>Foto</th>
                    <th width="50">Ubah</th>
                    <th width="50">Hapus</th>
                </tr>
                </thead>
                <tbody>
                @if(count($galleries) > 0)
                    @foreach($galleries as $key => $gallery)
                        <tr>
                            <td>{{ ($galleries->perPage() * ($galleries->currentPage() - 1)) + ($key + 1)  }}</td>
                            <td>{{ $gallery->title }}</td>
                            <td>{{ strlen($gallery->body < 80) ? strip_tags($gallery->body) : strip_tags(substr($gallery->body, 0, 80) . " ...") }}</td>
                            <td><a href="{{ route('admin.galleries.photos', ['slug_gallery' => $gallery->slug   ]) }}">{{ count($gallery->media) }} Foto</a></td>
                            <td>
                                <a href="{{ route('admin.galleries.edit', ['slug_gallery' => $gallery->slug]) }}" class="button is-warning">
                                    <span class="icon">
                                        <i class="fa fa-pencil"></i>
                                    </span>
                                    <span>Ubah</span>
                                </a>
                            </td>
                            <td>
                                <form action="{{ route('admin.galleries.destroy', [ 'slug_gallery' => $gallery->slug ]) }}" method="post">
                                    {{ csrf_field() }}
                                    <button class="button is-danger" onclick="return confirm('Yakin menghapus data ?')">
                                        <span class="icon">
                                            <i class="fa fa-trash-o"></i>
                                        </span>
                                        <span>Hapus</span>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="5" class="has-text-centered">Belum ada galeri ditambahkan</td>
                    </tr>
                @endif
                </tbody>
            </table>
            {{ $galleries->links('guest.components.pagination') }}
        </div>
    </div>
@endsection

@push('script')
<script>
    console.log("hello");
</script>
@endpush