<!DOCTYPE html>
<html lang="id">
    {{-- Include HTML Header & styles--}}
    @include('admin.layouts.partials.header')
<body>

    <div class="columns" style="margin-bottom: 0;">

        {{-- Include Aside Navigation --}}
        @include('admin.layouts.partials.aside')

        <div class="column is-10" style="padding-left: 0">

            {{-- Include Mobile Navigation --}}
            @include('admin.layouts.partials.nav')

            <div style="padding: 12px">
                {{-- Content Goes Here --}}
                @yield('content')
            </div>
        </div>
    </div>

    {{-- Footer --}}
    @include('admin.layouts.partials.footer')

    {{-- Include scripts --}}
    @include('admin.layouts.partials.scripts')

    {{-- Include Sweetalert --}}
    @include('sweet::alert')
</body>
</html>