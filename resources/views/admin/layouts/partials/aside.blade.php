<aside class="column is-2 aside hero is-fullheight is-hidden-mobile is-dark">

    <div class="main" style="padding: 20px">
        <div class="title">Utama</div>

        <a href="{{ route('admin') }}" class="dashboard item ">
            <span class="icon"><i class="fa fa-home"></i></span>
            <span class="name">Dashboard</span>
        </a>

        <a href="{{ route('admin.member') }}" class="member item">
            <span class="icon"><i class="fa fa-user"></i></span>
            <span class="name">Anggota</span>
        </a>

        <a href="{{ route('admin.post') }}" class="post item">
            <span class="icon"><i class="fa fa-pencil"></i></span>
            <span class="name">Tulisan</span>
        </a>

        <a href="#" class="schedule item">
            <span class="icon"><i class="fa fa-calendar-times-o"></i></span>
            <span class="name">Jadwal</span>
        </a>

        <a href="{{ route('admin.achievements') }}" class="achievement item">
            <span class="icon"><i class="fa fa-star"></i></span>
            <span class="name">Prestasi</span>
        </a>

        <a href="{{ route('admin.galleries') }}" class="gallery item">
            <span class="icon"><i class="fa fa-image"></i></span>
            <span class="name">Galeri</span>
        </a>

        <br> <br>
        <div class="title">Web</div>
        <a href="{{ route('admin.faqs') }}" class="faqs item">
            <span class="icon"><i class="fa fa-file-o"></i></span>
            <span class="name">FAQs</span>
        </a>

        <a href="{{ url()->route('admin.contacts') }}" class="contact item">
            <span class="icon"><i class="fa fa-address-book"></i></span>
            <span class="name">Kontak</span>
        </a>

        <br> <br>
        <div class="title">Akun</div>
        <form action="{{ url()->route('auth.admin.logout') }}" class="contact item" method="post" id="logout"
            onclick="document.getElementById('logout').submit()" style="cursor: pointer">
            {{ csrf_field() }}
            <span class="icon"><i class="fa fa-sign-out"></i></span>
            <span class="name">Keluar</span>
        </form>
    </div>
</aside>