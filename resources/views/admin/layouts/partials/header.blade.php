<head>
    <title>@yield('title')</title>

    <link rel="stylesheet" href="{{ asset('css/admin.css') }}">
    @stack('styles')
</head>