<nav class="nav is-dark has-shadow" id="top">
    <div class="container">
        <div class="nav-left">
            <a class="nav-item" href="{{ route('admin.dashboard.list') }}">
                ADMIN
            </a>
        </div>
        <span class="nav-toggle">
        <span></span>
        <span></span>
        <span></span>
      </span>
        <div class="nav-right nav-menu is-hidden-tablet">
            <a class="nav-item is-tab is-active">
                Dashboard
            </a>
            <a class="nav-item is-tab">
                Activity
            </a>
            <a class="nav-item is-tab">
                Timeline
            </a>
            <a class="nav-item is-tab">
                Folders
            </a>
        </div>
    </div>
</nav>