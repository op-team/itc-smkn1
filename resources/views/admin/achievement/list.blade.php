@extends('admin.layouts.app')
@section('title') Semua Prestasi @endsection

@section('content')
    <div class="column is-12">
        <div class="box">
            <h1 class="title">Daftar Prestasi</h1>
            <div class="columns">
                <div class="column">
                    <a href="{{ route('admin.achievements.create') }}" class="button is-success">
                    <span class="icon">
                        <i class="fa fa-plus"></i>
                    </span>
                        <span>
                        Buat Prestasi
                    </span>
                    </a>
                </div>
                <div class="column">
                    @include('guest.components.search_bar')
                </div>
            </div>
            <table class="table is-striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Judul</th>
                    <th>Konten</th>
                    <th width="50">Ubah</th>
                    <th width="50">Hapus</th>
                </tr>
                </thead>
                <tbody>
                @if(count($achievements) > 0)
                    @foreach($achievements as $key => $achievement)
                        <tr>
                            <td>{{ ($achievements->perPage() * ($achievements->currentPage() - 1)) + ($key + 1)  }}</td>
                            <td>{{ $achievement->title }}</td>
                            <td>{{ strlen($achievement->body < 80) ? strip_tags($achievement->body) : strip_tags(substr($achievement->body, 0, 80) . " ...") }}</td>
                            <td>
                                <a href="{{ route('admin.achievements.edit', ['slug_achievement' => $achievement->slug]) }}" class="button is-warning">
                                    <span class="icon">
                                        <i class="fa fa-pencil"></i>
                                    </span>
                                    <span>Ubah</span>
                                </a>
                            </td>
                            <td>
                                <form action="{{ route('admin.achievements.destroy', ['slug_achievement' => $achievement->slug]) }}" method="post">
                                    {{ csrf_field() }}
                                    <button class="button is-danger" onclick="return confirm('Yakin menghapus data ?')">
                                        <span class="icon">
                                            <i class="fa fa-trash-o"></i>
                                        </span>
                                        <span>Hapus</span>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="5">Belum ada prestasi ditambahkan</td>
                    </tr>
                @endif
                </tbody>
            </table>
            {{ $achievements->links('guest.components.pagination') }}
        </div>
    </div>
@endsection