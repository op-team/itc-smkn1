@extends('admin.layouts.app')
@section('title') Buat Prestasi @endsection

@section('content')
    <div class="column is-6">
        <div class="box">

            <a href="{{ route('admin.achievements') }}" class="button is-info">
                <span class="icon">
                    <i class="fa fa-chevron-circle-left"></i>
                </span>
                <span>Kembali</span>
            </a>
            <br><br>

            <h1 class="title">Buat Prestasi</h1>
            <br />
            <form action="{{ route('admin.achievements.store') }}" method="post">
                {{ csrf_field() }}
                    <p class="control">
                        <label for="" class="label">Judul</label>
                        <input type="text" class="input {{ set_error('title') }}" name="title" value="{{ old('title') }}">
                        {!! get_error('title') !!}
                    </p>
                    <br/>
                    <p class="control">
                        <label for="" class="label">Konten</label>
                        <textarea name="body" id="body" cols="30" rows="10" class="textarea {{ set_error('body') }}"></textarea>
                        {!! get_error('body') !!}
                    </p>
                    <div class="field pull-right">
                        <div class="control">
                            <button class="button is-primary">
                                <span class="icon">
                                    <i class="fa fa-plus"></i>
                                </span>
                                <span>Buat</span>
                            </button>
                        </div>
                    </div>
                    <div class="is-clearfix"></div>
            </form>
        </div>
    </div>
@endsection

@push('scripts')
    @ckeditor('body', ['skin' => 'kama'])
@endpush