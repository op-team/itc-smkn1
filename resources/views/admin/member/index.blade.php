@extends('admin.layouts.app')
@section('title') Semua Anggota @endsection

@section('content')
    <div class="column is-12">
        <div class="box">
            <table class="table is-striped">
                <h1 class="title">Daftar Semua Anggota</h1>
                @include('guest.components.search_bar')
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nama</th>
                        <th>Kelas</th>
                        <th width="50">Ubah</th>
                        <th width="50">Hapus</th>
                    </tr>
                </thead>
                <tbody>
                @if(count($members) > 0)
                    @foreach($members as $key => $member)
                    <tr onclick="window.location.href = '{{ route('admin.member.detail', ['username' => $member->username]) }}'" style="cursor: pointer;">
                        <td>{{ ($members->perPage() * ($members->currentPage() - 1)) + ($key + 1) }}</td>
                        <td>{{ $member->name }}</td>
                        <td>{{ $member->grade . " " . $member->major->name . " " . $member->class_number }}</td>
                        <td>
                            <a href="{{ route('admin.member.edit.form', ['username' => $member->username]) }}" class="button is-warning">
                                <span class="icon">
                                    <i class="fa fa-pencil"></i>
                                </span>
                                <span>Ubah</span>
                            </a>
                        </td>
                        <td>
                            <form action="{{ route('admin.member.destroy', ['username' => $member->username]) }}" method="post">
                                {{ csrf_field() }}
                                {{ method_field('delete') }}
                                <button class="button is-danger">
                                    <span class="icon">
                                        <i class="fa fa-trash-o"></i>
                                    </span>
                                    <span>Hapus</span>
                                </button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
            {{ $members->links('guest.components.pagination') }}
        </div>
    </div>
@endsection