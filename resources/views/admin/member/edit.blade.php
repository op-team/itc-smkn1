@extends('admin.layouts.app')
@section('title') {{ $member->name }} @endsection

@section('content')
    <div class="column is-6">
        <div class="box">
            <a href="{{ route('admin.member') }}" class="button is-info">
                <span class="icon">
                    <i class="fa fa-chevron-circle-left"></i>
                </span>
                <span>Kembali</span>
            </a>

            <br><br>

            <h1 class="title">Ubah Info Anggota</h1>
            <h2 class="subtitle">{{ $member->name }}</h2>
            <form action="{{ route('admin.member.update', ['username' => $member->username]) }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{ method_field('PUT') }}

                <div class="field is-grouped">
                    <label for="">Foto</label>
                    <figure class="control image is-64x64">
                        <img src="{{ $member->avatar }}" alt="Image">
                    </figure>
                    <p class="control">
                        <input type="file" name="avatar" class="input">
                    </p>
                </div>

                <div class="field">
                    <label class="label">Nama</label>
                    <p class="control">
                        <input class="input" type="text" placeholder="Nama" value="{{ $member->name }}" name="name">
                    </p>
                </div>
                <br>
                <div class="field is-horizontal">
                    <label class="label">Kelas</label>
                    <span class="field-body">
                        <span class="select">
                            <select name="grade" title="grade">
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                            </select>
                        </span>
                        <span class="select">
                            <select name="major" id="">
                                <option value="">Pilih jurusan</option>
                                @foreach($majors as $major)
                                    <option value="{{ $major->id }}">{{ $major->name }}</option>
                                @endforeach
                            </select>
                        </span>
                        <span class="select">
                            <select name="class_number" id="">
                                <option value="">Pilih nomor kelas</option>
                                @for($i = 1; $i < 8; $i++)
                                    <option value="{{ $i }}">{{ $i }}</option>
                                @endfor
                            </select>
                        </span>
                    </span>
                </div>
                <br>
                <div class="field">
                    <label class="label">Alamat</label>
                    <p class="control">
                        <input class="input" type="text" placeholder="Alamat" value="{{ $member->address }}" name="address">
                    </p>
                </div>
                <br>
                <div class="field">
                    <label class="label">Telepon</label>
                    <p class="control">
                        <input class="input" type="text" placeholder="Telepon" value="{{ $member->phone }}" name="phone">
                    </p>
                </div>
                <br>
                <div class="field pull-right">
                    <button class="button is-warning">
                        <span class="icon">
                            <span class="fa fa-floppy-o"></span>
                        </span>
                        <span>Simpan</span>
                    </button>
                </div>
                <div class="is-clearfix"></div>
            </form>
        </div>
    </div>
@endsection