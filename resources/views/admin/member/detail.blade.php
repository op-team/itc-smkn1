@extends('admin.layouts.app')
@section('title') {{ $member->name }} @endsection

@section('content')
    <div class="column is-8">
        <div class="box">
            <a href="{{ route('admin.member') }}" class="button is-info">
                <span class="icon">
                    <i class="fa fa-chevron-circle-left"></i>
                </span>
                <span>Kembali</span>
            </a>

            <br><br>
            <div class="columns is-multiline">
                <div class="column is-12">
                    <article class="media">
                        <div class="media-left pull-right">
                            <figure class="image is-128x128">
                                <img src="{{ $member->avatar }}" alt="Image">
                            </figure>
                        </div>
                        <div class="media-content">
                            <div class="content">
                                <h1>{{ $member->name }}</h1>
                                <h5 style="margin-top: -10px">{{ $member->username }}</h5>
                            </div>
                        </div>
                    </article>
                </div>
            </div>
            <div class="column is-12" style="margin-top: -40px">
                <br><br>
                <table style="font-size: 1.2em;line-height: 2.0">
                    <tr>
                        <td width="100">Kelas</td>
                        <td>: {{ $member->grade . " " . $member->major->name . " " . $member->class_number }}</td>
                    </tr>
                    <tr>
                        <td>Alamat</td>
                        <td>: {{ $member->address }}</td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td>: {{ $member->email }}</td>
                    </tr>
                    <tr>
                        <td>Telepon</td>
                        <td>: {{ $member->phone }}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
@endsection