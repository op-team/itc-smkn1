@extends('guest.layouts.app')

@section('content')
    <div class="columns" style="margin-top: 40px">
        <div class="column is-3">
            <figure class="image is-128x128 is-circled" style="margin-left: auto;margin-right: auto">
                <img src="{{ Auth::user()->avatar }}" alt="">
            </figure>
            <h1 class="has-text-centered"><a href="{{ route('member.dashboard.index') }}">{{ auth()->user()->name }}</a></h1>
            @include('user.layouts.partials.sidebar')
        </div>
        <div class="column">
            <div class="content brand has-text-centered">
                <h1><strong>@yield('home_title')</strong></h1>
            </div>
            <div class="columns is-multiline" style="margin-top: 50px">
                @yield('home_content')
            </div>
        </div>
    </div>
@endsection