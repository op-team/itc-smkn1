<nav class="panel">
    <a class="panel-block {{ set_active('member.dashboard.index') }}" href="{{ route('member.dashboard.index') }}">
        <span class="panel-icon">
          <i class="fa fa-book"></i>
        </span>
        Kehadiran dan Poin
    </a>
    <a class="panel-block {{ set_active('member.dashboard.posts') }}" href="{{ route('member.dashboard.posts') }}">
        <span class="panel-icon">
          <i class="fa fa-pencil-square-o"></i>
        </span>
        Tulisan
    </a>
    <a class="panel-block {{ set_active('member.dashboard.account') }}" href="{{ route('member.dashboard.account') }}">
        <span class="panel-icon">
          <i class="fa fa-key"></i>
        </span>
        Akun
    </a>
    <form class="panel-block" action="{{ route('auth.member.logout') }}" method="post" id="logout"
          onclick="document.getElementById('logout').submit()" style="cursor: pointer">
        <span class="panel-icon">
          <i class="fa fa-sign-out"></i>
        </span>
        Keluar
    </form>
</nav>