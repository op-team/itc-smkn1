@extends('user.layouts.app')

@section('head_title') Dashboard @endsection
@section('home_title') Akun Saya @endsection

@section('home_content')
    <div class="column is-offset-1 is-7">
        <form action="" enctype="multipart/form-data" method="post">
            {{ csrf_field() }}
            <div class="field">
                <label class="label">Nama lengkap</label>
                <p class="control has-icon">
                    <input type="text" class="input {{ set_error('nama') }}" name="nama" placeholder="Nama lengkap" value="{{ auth()->user()->name }}">
                    <span class="icon">
                        <i class="fa fa-user-circle"></i>
                    </span>
                </p>
                {!! get_error('nama') !!}
            </div>
            <br />
            <div class="field">
                <label class="label">Profil</label>
                <p class="control">
                    <input type="file" class="{{ set_error('avatar') }}" name="avatar" placeholder="Ubah avatar">
                </p>
                <p class="help-block">* kosongi jika tidak ingin merubah profil</p>
            </div>
            <br />
            <div class="field">
                <label class="label">Nama pengguna</label>
                <p class="control has-icon">
                    <input type="text" class="input {{ set_error('username') }}" name="username" placeholder="Nama pengguna" value="{{ auth()->user()->username }}">
                    <span class="icon">
                        <i class="fa fa-user-circle"></i>
                    </span>
                </p>
                {!! get_error('username') !!}
            </div>
            <br />
            <div class="field pull-right">
                <button class="button is-danger">
                    <span class="icon">
                        <i class="fa fa-floppy-o"></i>
                    </span> <span>Simpan</span>
                </button>
            </div>
        </form>
    </div>
@endsection