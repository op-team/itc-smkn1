@extends('user.layouts.app')

@section('head_title') Dashboard @endsection
@section('home_title') Dashboard @endsection

@section('home_content')
    <div class="column">
        <nav class="level">
            <div class="level-item has-text-centered">
                <div>
                    <p class="heading">Kehadiran</p>
                    <p class="title">{{ $attendedSchedule }}</p>
                </div>
            </div>
            <div class="level-item has-text-centered">
                <div>
                    <p class="heading">Tulisan</p>
                    <p class="title">{{ $totalPost }}</p>
                </div>
            </div>
            <div class="level-item has-text-centered">
                <div>
                    <p class="heading">Prestasi</p>
                    <p class="title">789</p>
                </div>
            </div>
        </nav>
    </div>
@endsection