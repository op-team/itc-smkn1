@extends('user.layouts.app')

@section('head_title') Dashboard @endsection
@section('home_title') Tulisan
<div class="is-pulled-right">
	<article class="message is-primary">
		<div class="message-body">
			<a href="{{ route('member.posts.create.form') }}">
				<strong>Tulis cerita</strong>
			</a>
		</div>
	</article>
</div>
@endsection

@section('home_content')
	<div class="columns">

	</div>
	@if(count($posts) > 0)
		<div class="columns is-multiline">
			@foreach($posts as $key => $post)
				<div class="column is-offset-1">
					<div class="card">
						@if ($post->has('media')->first())
							<div class="card-image" onclick="redirect('{{ route('guest.posts.detail', ['slug' => $post->slug]) }}')">
								<figure class="image is-2by1" style="background: url('{{ $post->media()->first()->src }}') no-repeat center;background-size: cover;">
								</figure>
							</div>
						@endif
						<div class="card-content">
							<h1>
								<a href="{{ route('guest.posts.detail', ['slug' => $post->slug]) }}"><strong>{{ $post->title }}</strong></a>
							</h1>
							<p>oleh {{ $post->author->name }} pada {{ $post->published_at }}</p>
							<br>
							<div class="content brand">
								{{ substr(strip_tags($post->body), 0, 80) . '. . .' }}
							</div>
							<div class="pull-right" style="margin-left: 4px">
								<form action="{{ route('member.posts.delete', ['slug_post' => $post->slug]) }}" method="post">
									{{ csrf_field() }}
									<button class="button is-danger">
										<span class="icon">
											<i class="fa fa-trash"></i>
										</span>
									</button>
								</form>
							</div>
							<div class="pull-right">
								<a href="{{ route('member.posts.edit.form', ['slug_post' => $post->slug]) }}" class="button is-warning">
									<span class="icon">
										<i class="fa fa-pencil"></i>
									</span>
								</a>
							</div>
							<div class="is-clearfix"></div>
						</div>
					</div>
				</div>
			@endforeach
			@else
				<div class="content has-text-centered">
					Belum ada tulisan
				</div>
			@endif
		</div>
@endsection