@extends('guest.layouts.app')
@section('head_title') Tulis cerita @endsection

@section('content_title') Tulis Ceritamu @endsection
@push('styles')
<link href="https://cdn.quilljs.com/1.2.3/quill.snow.css" rel="stylesheet">
@endpush

@section('content')
    <br /> <br />
    <form action="{{ route('member.posts.create') }}" enctype="multipart/form-data" method="post">
        {{ csrf_field() }}
        <div class="field">
            <label class="label">Judul</label>
            <p class="control">
                <input type="text" class="input {{ set_error('title') }}" name="title" placeholder="Judul tulisanmu">
            </p>
            {!! get_error('title') !!}
        </div>
        <br />
        <div class="field">
            <label class="label">Konten</label>
            <p class="control">
                <div id="editor"></div>
            </p>
        </div>
        <br />
        <div class="field">
            <label class="label">Topik</label>
            <p class="control">
                <span class="select {{ set_error('tags') }}">
                    <select name="tags[]" id="" multiple="multiple">
                        @foreach($tags as $tag)
                            <option value="{{ $tag->id }}">{{ $tag->name }}</option>
                        @endforeach
                    </select>
                </span>
                {!! get_error('tags[]') !!}
                <p class="help-block">Tekan ctrl + klik untuk multi selection</p>
            </p>
        </div>
        <br />
        <div class="pull-right">
            <button class="button is-info">
                <span class="icon">
                    <i class="fa fa-plus"></i>
                </span> <span>Tambahkan</span>
            </button>
        </div>
        <br />
    </form>
@endsection

@push('scripts')
<script src="//cdn.quilljs.com/1.2.3/quill.min.js"></script>
<script>
    var toolbarOptions = [
        ['code-block'],
        [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
        [{ 'font': [] }],

        ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
        ['blockquote'],

        [{ 'header': 1 }, { 'header': 2 }],               // custom button values
        [{ 'list': 'ordered'}, { 'list': 'bullet' }],
        ['link', 'image', 'video'],      // superscript/subscript
        [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
        //[{ 'direction': 'rtl' }],                         // text direction

        [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
        [{ 'align': [] }],

//        ['clean']                                         // remove formatting button
    ];

    var quill = new Quill('#editor', {
        history: {          // Enable with custom configurations
            'delay': 2500,
            'userOnly': true
        },
        syntax: true,
        modules: {
            toolbar: toolbarOptions
        },
        theme: 'snow',
        placeholder: 'Cerita kamu'
    })
</script>
@endpush