<?php

namespace App\Http\Controllers\Member;

use App\Models\Accounts\User;
use App\Models\Contents\Schedule;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('alert');
    }

    /**
     * Show a user dashboard
     *
     * Name   : member.dashboard
     * Prefix : anggota
     *
     */
    public function index()
    {
        $this->getAbsent();
        return view('user.dashboard.index');
    }

    public function getAbsent()
    {
        $this->setData('totalSchedule', Schedule::count());
        $this->setData('attendedSchedule', auth()->user()->presentOn->count());
        $this->setData('totalPost', auth()->user()->posts->count());
    }

    public function posts ()
    {
        $this->setData('posts', auth()->user()->posts);
        return view('user.dashboard.post.index');
    }

    public function account ()
    {
        return view('user.dashboard.account.index');
    }
}
