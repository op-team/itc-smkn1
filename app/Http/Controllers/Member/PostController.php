<?php

namespace App\Http\Controllers\Member;

use App\Models\Additional\Tag;
use App\Models\Contents\Post;
use App\Modules\Post\CreateNewPost;
use App\Http\Controllers\Controller;
use App\Modules\Post\DeleteExistingPost;
use App\Modules\Post\UpdateExistingPost;
use Illuminate\Http\Request;

class PostController extends Controller
{

    protected $type = 'post';

    public function __construct()
    {
        parent::__construct();

        $this->middleware('alert')->only([
            'index',
            'showCreatePostForm',
            'showEditForm',
            'showCreatePostVideoForm'
        ]);
    }

    /**
     * show list of posts . all post is here including video post .
     * so , write a type of post in view and it will take some discussion
     * at post type video . will have a button to redirect at
     * [member.posts.video.edit]
     *
     * Remember . for create new post . it will have a 2 button .
     * create just a post or post video
     *
     * Name   : member.posts
     * Prefix : anggota/tulisan
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        // TODO : fill a view of post user page

        return view('', [
            'posts' => auth()->user()->posts
        ]);
    }

    /**
     * show creating post video form
     * Name   : member.posts.create-video.form
     * Prefix : /tulisan/tulis-video
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showCreatePostVideoForm()
    {
        // TODO : fill a view of create video form
        return view('');
    }

    /**
     * show a create form for post
     * Name   : member.posts.create.form
     * Prefix : anggota/tulisan/tulis
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showCreatePostForm()
    {
        return view('user.post.create', [
            'tags' => Tag::all()
        ]);
    }

    /**
     * show form for edit post
     * Name   : member.posts.edit.form
     * Prefix : anggota/posts/edit/{slug}
     *
     * @param Post $post
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showEditForm(Post $post)
    {
        $this->authorize('view', $post);
        return view('user.post.update', [
            'post' => $post,
            'tags' => Tag::all()
        ]);
    }

    /**
     * create a new posts
     * type can declare using config in post-type.public config
     *
     * Name   : member.posts.create
     * Prefix : anggota/create/{type}
     *
     * @param string $type
     * @return \Illuminate\Http\RedirectResponse
     * @throws \ReflectionException
     */
    public function create($type = 'post')
    {
        // TODO : fill a action while success or fail
        $create = $this->dispatch(new CreateNewPost(['type' => $type]));

        if ($create->success())
            return redirect()->back()->with('success', '');

        return redirect()->back()->with('errors', '');
    }

    /**
     * update exist post
     * Name   : member.posts.update
     * Prefix : anggota/update/{slug}
     *
     * @param Post $post
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Post $post)
    {
        // TODO : fill a action while success or fail
        $update = $this->dispatch(new UpdateExistingPost($post));

        if ($update->success())
            return redirect()->back()->with('success', '');

        return redirect()->back()->with('errors', '');
    }

    /**
     * delete post
     * Name   : member.posts.delete/{slug}
     * Prefix : anggota/create
     *
     * @param Post $post
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Post $post)
    {
        // TODO : fill a action while success or fail
        $delete = $this->dispatch(new DeleteExistingPost($post));

        if ($delete->success())
            return redirect()->back()->with('success', '');

        return redirect()->back()->with('errors', '');
    }
}
