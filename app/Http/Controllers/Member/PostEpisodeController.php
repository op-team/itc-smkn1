<?php

namespace App\Http\Controllers\Member;

use App\Models\Contents\Post;
use App\Http\Controllers\Controller;
use app\Modules\Post\DeleteExistingPost;
use App\Modules\PostEpisode\CreateNewEpisode;
use App\Modules\PostEpisode\UpdateExistingEpisode;

class PostEpisodeController extends Controller
{

    protected $type = 'post-video';


    /**
     * Show list of child video post
     * Name   : member.posts.episode.show
     * Prefix : anggota/tulisan/episode/{slug}
     *
     * @param Post $postParent
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @internal param Post $post
     */
    public function showList(Post $postParent)
    {
        $this->authorize('view', $postParent);

        // TODO : fill a view of show list of post video
        return view('', [
            'posts' => $postParent->child
        ]);
    }

    /**
     * show create children form
     * Name   : member.posts.episode.create.form
     * Prefix : anggota/tulisan/episode/{slug}/isi-episode
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showCreateForm()
    {
        // TODO : fill a view of create form for children post video
        return view();
    }

    /**
     * show create children form
     * Name   : member.posts.video.create.form
     * Prefix : anggota/tulisan/video/edit-episode/{slug}
     *
     * @param Post $post
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @internal param Post $postChild
     * @internal param Post $post
     */
    public function showEditForm(Post $post)
    {
        $this->authorize('view', $post);
        // TODO : fill a view of edit form for children post video
        return view('', [
            'post' => $post
        ]);
    }

    /**
     * Create a new post episode
     * Name   : member.posts.episode.create
     * Prefix : anggota/tulisan/episode/{slug}/isi-episode
     *
     * @param Post $postParent
     * @return \Illuminate\Http\RedirectResponse
     */
    public function create(Post $postParent)
    {
        // TODO : fill a action while success or fail
        $create = $this->dispatch(new CreateNewEpisode($postParent));

        if ($create->success())
            return redirect()->back()->with('success', '');

        return redirect()->back()->with('errors', '');
    }

    /**
     * Update episode
     * Name   : member.posts.episode.update
     * Prefix : anggota/tulisan/episode/edit-episode/{slug}
     *
     * @param Post $post
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Post $post)
    {
        // TODO : fill a action while success or fail
        $update = $this->dispatch(new UpdateExistingEpisode($post));

        if ($update->success())
            return redirect()->back()->with('success', '');

        return redirect()->back()->with('errors', '');
    }

    /**
     * delete episode
     * Name   : member.posts.episode.delete
     * prefix : anggota/tulisan/episode/delete-episode/{slug}
     *
     * @param Post $post
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Post $post)
    {
        // TODO : fill a action while success or fail
        $delete = $this->dispatch(new DeleteExistingPost($post));

        if ($delete->success())
            return redirect()->back()->with('success', '');

        return redirect()->back()->with('errors', '');
    }
}
