<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use app\Modules\Account\UpdateAccount;
use app\Modules\Account\UpdatePasswordAccount;

class AccountController extends Controller
{

    /**
     * show change profile form
     * Name   : member.account.profile.form
     * Prefix : anggota/akun
     *
     */
    public function showChangeProfileForm()
    {
        // TODO : fill a view of change profile
        return view();
    }

    /**
     * show Change Password Form
     * Name   : member.account.password.form
     * Prefix : anggota/akun/password
     *
     */
    public function showChangePasswordForm()
    {
        // TODO : fill a view of change password
        return view();
    }

    /**
     * process change profile
     * Name   : member.account.profile
     * Prefix : anggota/akun
     *
     */
    public function changeProfile()
    {
        $update = $this->dispatch(new UpdateAccount());

        // TODO : fill a success response
        if ($update->success())
            return redirect()->back()->with('success', '');

        return redirect()->back()->with('errors', '');
    }

    /**
     * process Change Password
     * Name   : member.account.password
     * Prefix : anggota/akun/password
     *
     */
    public function changePassword()
    {
        $update = $this->dispatch(new UpdatePasswordAccount());

        // TODO : fill a success response
        if ($update->success())
            return redirect()->back()->with('success', '');

        return redirect()->back()->with('errors', '');
    }
}
