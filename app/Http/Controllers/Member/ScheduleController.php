<?php

namespace App\Http\Controllers\Member;

use App\Models\Contents\Schedule;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ScheduleController extends Controller
{
    /**
     * show all schedule 
     * Name   : member.schedule
     * Prefix : anggota/jadwal
     * 
     */
    public function index()
    {
        $schedules = Schedule::orderBy('created_at', 'desc')->paginate();

       return view('guest.schedule.index', [
            'schedules' => $schedules
        ]);
    }

    /**
     * show detail schedule
     * Name   : member.schedule.show
     * Prefix : anggota/jadwal/{schedule}
     *
     * @param Schedule $schedule
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Schedule $schedule)
    {
        return view('guest.schedule.detail', [
            'schedule' => $schedule
        ]);
    }
}
