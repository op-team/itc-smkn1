<?php
/**
 * Created by PhpStorm.
 * User: Yustiko
 * Date: 4/28/2017
 * Time: 12:44 PM
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\Extra\Faq;
use App\Modules\CreateNewFaqs;
use App\Modules\DeleteExistingFaqs;
use App\Modules\UpdateExistingFaqs;

class FaqsController extends Controller
{
    /**
     * Show index for faqs path
     *
     * Path : /faqs
     * Name : admin.faqs
     * Method : index
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return $this->list();
    }

    /**
     * Show all list of faqs
     *
     * Path : /faqs/daftar-faqs
     * Name : admin.faqs.list
     * Method : list
     *
     * @param int $paginate
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function list($paginate = 15)
    {
        $faq = Faq::orderBy('created_at' , 'asc');

        if ($this->request->has('search'))
            $faq->search($this->request->input('search'));

        return view('admin.faqs.list', [
            'faqs' => $faq->paginate($paginate)
        ]);
    }

    /**
     * Show form for creating faqs
     *
     * Path : /faqs/buat
     * Name : admin.faqs.create
     * Method : Create
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.faqs.create');
    }

    /**
     * Show form for editing faqs
     *
     * Path : /faqs/ubah/{id}
     * Name : admin.faqs.edit
     * Method : edit
     *
     * @param Faq $faq
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Faq $faq)
    {
        return view('admin.faqs.edit', [
           'faq' => $faq
        ]);
    }

    /**
     * Method for storing new faqs
     *
     * Path : /faqs/store
     * Name : admin.faqs.store
     * Method : store
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $store = $this->dispatch(new CreateNewFaqs());

        if ($store->success())
            return redirect()->route('admin.faqs.list')->with('success', 'FAQ berhasil dibuat');

        return redirect()->back()->with('errors', 'Gagal membuat FAQ');
    }

    /**
     * Method for updating existing faqs
     *
     * Path : /faqs/update/{id}
     * Name : admin.faqs.update
     * Method : update
     *
     * @param Faq $faq
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Faq $faq)
    {
        $update = $this->dispatch(new UpdateExistingFaqs($faq));

        if ($update->success())
            return redirect()->route('admin.faqs.list')->with('success', 'FAQ berhasil diubah');

        return redirect()->back()->with('errors', 'Gagal menyimpan perubahan FAQ');
    }

    /**
     * Method for deleting existing faqs
     *
     * Path : /faqs/destroy/{id}
     * Name : admin.faqs.destroy
     * Method : destroy
     *
     * @param Faq $faq
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Faq $faq)
    {
        $destroy = dispatch(new DeleteExistingFaqs($faq));

        if ($destroy->success())
            return redirect()->route('admin.faqs.list')->with('success', 'FAQ berhasil dihapus');

        return redirect()->back()->with('errors', 'Gagal menghapus FAQ');
    }
}