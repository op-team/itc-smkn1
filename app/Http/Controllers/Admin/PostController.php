<?php
/**
 * @author
 * User: Yustiko <yustiko404@gmail.com>
 * Date: 4/7/2017
 * Time: 1:36 PM
 */

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\Contents\Post;
use App\Modules\Post\CreateNewPost;
use App\Modules\Post\DeleteExistingPost;
use App\Modules\Post\UpdateExistingPost;
use Carbon\Carbon;

class PostController extends Controller
{
    /**
     * set default property type in this controller\
     */
    protected $type = 'post';


    /**
     * set default prefix for this route (return list function)
     *
     * Name : admin.post
     * Path : /tulisan
     * Method : index
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return $this->statistic();
    }

    /**
     * show all list of post, and this page have 2 button
     * the button are for create post within or without video
     *
     * Name : admin.post.list
     * Path : /tulisan/daftar-tulisan
     * Method : list
     *
     * @param int $paginateItem
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function statistic($paginateItem = 15)
    {
        $post = Post::type($this->type);

        if ($this->request->has('search'))
            $post->search($this->request->input('search'));

        return view('admin.post.list', [
            'posts' => $post->paginate($paginateItem)
        ]);
    }

    /**
     * this function is to published post
     * if the post already published it will non published
     *
     * Name : admin.post.publish
     * Path : /tulisan/publis/{slug_post}
     * Method : publish
     *
     * @param Post $post
     * @return \Illuminate\Http\RedirectResponse
     */
    public function publish(Post $post)
    {
        if ($post->published_at == null) {
            $post->published_at = Carbon::now();
            $post->save();

            return redirect()->route('admin.post.list')->with('success', 'Tulisan berhasil dipublis');
        }

        $post->published_at = null;
        $post->save();

        return redirect()->back()->with('errors', 'Post failed to published');
    }

    /**
     * show form to create post without video
     *
     * Name : admin.post.create
     * Path : /tulisan/buat
     * method : create
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.post.create');
    }

    /**
     * show form to create post within video
     *
     * Name : admin.post.create.video
     * Path : /tulisan/buat/vidio
     * Method : createVideo
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createVideo()
    {
        return view(('admin.post.video.create'));
    }

    /**
     * show edit form for post content without video
     *
     * Name : admin.post.edit
     * Path : /tulisan/ubah/{slug_post}
     * Method : edit
     *
     * @param Post $post
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Post $post)
    {
        return view('admin.post.edit', [
            'posts' => $post
        ]);
    }

    /**
     * create new post
     * type declare by config post-type.public config
     *
     * Name : admin.post.store
     * Path : /tulisan/store/{type}
     * Method : store
     *
     * @param string $type
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store($type = 'post')
    {
           $store = $this->dispatch(new CreateNewPost(['type' => $type]));

           if ($store->success())
               return redirect()->route('admin.post.list')->with('success', 'Tulisan telah dibuat');

           return redirect()-back()->with('errors', 'creating post failed');
    }

    /**
     * update existing post binding with slug
     *
     * Name : admin.post.update
     * Path : /tulisan/update/{slug_post}
     * Method : update
     * @param Post $post
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Post $post)
    {
        $update = $this->dispatch(new UpdateExistingPost($post));

        if ($update->success())
            return redirect()->route('admin.post.list')->with('success', 'Tulisan berhasil diubah');

        return redirect()->route('admin.post.list')->with('errors', 'Tulisan gagal diubah');
    }

    /**
     * delete existing post binding with slug
     *
     * Name : admin.post.destroy
     * Path : /tulisan/destroy/{slug_post}
     * Method : destroy
     *
     * @param Post $post
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Post $post)
    {
        $destroy = $this->dispatch(new DeleteExistingPost($post));

        if ($destroy->success())
            return redirect()->route('admin.post.list')->with('success', 'Tulisan berhasil dihapus');

        return redirect()->route('admin.post.list')->with('success', 'Tulisan gagal dihapus');
    }
}