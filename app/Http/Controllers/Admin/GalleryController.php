<?php
/**
 * @author yustiko
 * User: <yustiko404@gmail.com>
 * Date: 4/19/2017
 * Time: 1:19 PM
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\Contents\Media;
use App\Models\Contents\Post;
use App\Modules\Media\CreateNewMedia;
use App\Modules\Media\DeleteExistingMedia;
use App\Modules\Post\CreateNewPost;
use App\Modules\Post\DeleteExistingPost;
use App\Modules\Post\UpdateExistingPost;

class GalleryController extends Controller
{
    /**
     * set default type used by this class
     *
     * @var string
     */
    protected $type = 'gallery';

    /**
     * set default path redirected to show all list
     *
     * Path : /galeri
     * Name : admin.galleries
     * Method : index
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return $this->list();
    }

    /**
     * show all list of gallery
     *
     * @param int $paginateItem
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function list($paginateItem = 15)
    {
        $gallery = Post::type($this->type);

        if ($this->request->has('search'))
            $gallery->search($this->request->input('search'));

        return view('admin.gallery.list', [
            'galleries' => $gallery->paginate($paginateItem)
        ]);
    }

    /**
     * show page to make new galeri
     *
     * Path : /galeri/buat
     * Name : admin.galleries.create
     * Method : create
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.gallery.create');
    }

    /**
     * show form to edit existing gallery
     *
     * Path : /galeri/ubah/{slug_gallery}
     * Name : admin.galleries.edit
     * method : edit
     *
     * @param Post $gallery
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Post $gallery)
    {
        return view('admin.gallery.edit', [
            'gallery' => $gallery->with('media')->first()
        ]);
    }

    /**
     * method for store new gallery
     *
     * Path : /galeri/store/{type?}
     * Name : admin.galleries.store
     * Method : store
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $store = $this->dispatch(new CreateNewPost(['type' => $this->type]));

        if ($store->success())
            return redirect()->route('admin.galleries')->with('success', 'Tambah galeri berhasil');

        return redirect()->back()->with('errors', 'make galeri has been failed');
    }

    /**
     * method to update existing gallery
     *
     * Path : galeri/update/{slug_gallery}
     * Name : admin.galleries.update
     * Method : update
     *
     * @param Post $gallery
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Post $gallery)
    {
        $update = $this->dispatch(new UpdateExistingPost($gallery));

        if ($update->success())
            return redirect()->back()->with('success', 'galeri berhasil diubah');

        return redirect()->back()->with('errors', 'update galeri has been failed');
    }

    /**
     * method for delete existing gallery
     *
     * Path : /galeri/destroy/{slug_gallery}
     * Name : admin.galleries.destroy
     * Method : destroy
     *
     * @param Post $gallery
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Post $gallery)
    {
        $destroy = $this->dispatch(new DeleteExistingPost($gallery));

        if ($destroy->success())
            return redirect()->route('admin.galleries')->with('success', 'galeri berhasil dihapus');

        return redirect()->back()->with('errors', 'delete galeri has been failed');
    }

    /**
     * method for show list image on gallery
     *
     * Path : /galeri/destroy/{slug_gallery}
     * Name : admin.galleries.photos.add
     * Method : addPhotos
     *
     * @param Post $gallery
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function photos (Post $gallery)
    {
        return view('admin.gallery.photos', [
            'gallery' => $gallery->load('media')
        ]);
    }

    /**
     * method for add image on gallery
     *
     * Path : /galeri/destroy/{slug_gallery}
     * Name : admin.galleries.photos.add
     * Method : addPhotos
     *
     * @param Post $gallery
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addPhotos (Post $gallery)
    {
        $store = $this->dispatch(new CreateNewMedia([], $gallery));
        if ($store->success())
            return redirect()->route('admin.galleries.photos', ['slug_gallery' => $gallery->slug ])->with('success', 'Gambar berhasil ditambahkan');

        return redirect()->back()->with('errors', 'Gagal menambahkan gambar');
    }

    /**
     * method for delete existing image gallery
     *
     * Path : /galeri/{slug_gallery}/photos/{id}/remove
     * Name : admin.galleries.remove
     * Method : removeImage
     *
     * @param Post $gallery
     * @param Media $media
     * @return \Illuminate\Http\RedirectResponse
     */
    public function removePhoto (Post $gallery, Media $media)
    {
        $destroy = $this->dispatch(new DeleteExistingMedia([], $gallery->media()->findOrfail($media->id)));

        if ($destroy->success())
            return redirect()->route('admin.galleries.photos', ['slug_gallery' => $gallery->slug ])->with('success', 'Gambar berhasil dihapus');

        return redirect()->back()->with('errors', 'delete galeri has been failed');
    }
}