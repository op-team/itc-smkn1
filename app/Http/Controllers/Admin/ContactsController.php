<?php

namespace App\Http\Controllers\Admin;

use App\Models\Extra\Setting;
use App\Modules\Setting\Contact\CreateNewContact;
use App\Modules\Setting\Contact\DeleteExistingContact;
use App\Modules\Setting\Contact\UpdateExistingContact;
use App\Http\Controllers\Controller;
use Hamcrest\Core\Set;

class ContactsController extends Controller
{
    
    /**
     * Show list all contacts
     * 
     * Path : /kontak
     * Name : admin.contacts
     * Method : GET
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index ()
    {
        $this->setData('contacts', Setting::contact()->paginate(10));
        return view('admin.contacts.list');
    }

    /**
     * Show create contacts form
     * 
     * Path : /kontak/buat
     * Name : admin.contacts.create
     * Method : GET
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create ()
    {
        return view('admin.contacts.create');
    }

    /**
     * Method for storing new faqs
     *
     * Path : /kontak/store
     * Name : admin.contacts.store
     * Method : post
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store ()
    {
        $store = $this->dispatch(new CreateNewContact());
        var_dump(session()->all());
        if ($store->success())
            return redirect()->route('admin.contacts')->with('success', 'Kontak berhasil dibuat');
        return redirect()->back()->with('errors', 'Gagal membuat Kontak');
    }

    /**
     * Method for storing new faqs
     *
     * Path : /kontak/ubah
     * Name : admin.contacts.edit
     * Method : get
     *
     * @param Setting $contact
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit (Setting $contact)
    {
        $this->setData('contact', $contact);
        return view('admin.contacts.edit');
    }

    /**
     * Method for updating existing contacts
     *
     * Path : /faqs/destroy/{id}
     * Name : admin.faqs.destroy
     * Method : destroy
     *
     * @param Setting $contact
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update (Setting $contact)
    {
        $update = $this->dispatch(new UpdateExistingContact($contact));

        if ($update->success())
            return redirect()->route('admin.contacts')->with('success', 'Kontak berhasil ubah');

        return redirect()->back()->with('errors', 'Gagal mengubah kontak');
    }

    /**
     * Method for deleting existing faqs
     *
     * Path : /kontak/destroy/{id}
     * Name : admin.contacts.destroy
     * Method : delete
     *
     * @param Setting $contact
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy (Setting $contact)
    {
        $delete = $this->dispatch(new DeleteExistingContact($contact));
        if ($delete->success())
            return redirect()->route('admin.contacts')->with('success', 'Kontak berhasil dihapus');

        return redirect()->back()->with('errors', 'Gagal menghapus kontak');
    }




}
