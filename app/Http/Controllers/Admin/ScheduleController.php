<?php
/**
 * @author
 * User: Yustiko <yustiko404@gmail.com>
 * Date: 4/13/2017
 * Time: 11:57 AM
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\Contents\Schedule;
use App\Modules\CreateNewSchedule;
use App\Modules\DeleteExistingSchedule;
use App\Modules\UpdateExistingSchedule;
use Carbon\Carbon;

class ScheduleController extends Controller
{
    /**
     * Show index
     * method for set default path
     *
     * Path : /jadwal
     * Name : admin.schedule
     * Method : index
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return $this->list();
    }

    /**
     * show list of all schedule content
     *
     * Path : /jadwal/semua-jadwal
     * Name : admin.schedule.list
     * Method : list
     *
     * @param int $paginateItem
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function list($paginateItem = 15)
    {
        $schedule = Schedule::OrderBy('published_at', 'asc');

        if ($this->request->has('search'))
            $schedule->search($this->request->input('search'));

        return view('admin.schedule.list', [
            'schedules' => $schedule->paginate($paginateItem)
        ]);
    }

    public function publish(Schedule $schedule)
    {
        if ($schedule->publised_at == null) {
            $schedule->published_at = Carbon::now();
            $schedule->save();

            return redirect()->route('admin.schedule.list')->with('success', 'jadwal berhasil di publis');
        }

        $schedule->published_at = null;
        $schedule->save();

        return redirect()->back()->with('errors', 'schedule failed to published');
    }

    /**
     * show form for creating schedule
     *
     * Path : /jadwal/buat
     * Name : admin.schedule.create
     * Method : create
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.schedule.create');
    }

    /**
     * show form for editing schedule content
     *
     * Path : /jadwal/ubah/{schedule}
     * Name : admin.schedule.edit
     * Method : edit
     *
     * @param Schedule $schedule
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Schedule $schedule)
    {
        return view('admin.schedule.edit', [
           'schedules' => $schedule
        ]);
    }

    /**
     * Method to store/save new schedule
     *
     * Path : /jadwal/store
     * Name : admin.schedule.store
     * Method : store
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $store = $this->dispatch(new CreateNewSchedule());

        if ($store->success())
            return redirect()->route('admin.schedule.list')->with('success', 'jadwal berhasil dibuat');

        return redirect()->back()->with('errors', 'creating schedule failed');
    }

    /**
     * Method for update existing schedule
     *
     * Path : /jadwal/update/{schedule}
     * Name : admin.schedule.update
     * Method : update
     *
     * @param Schedule $schedule
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Schedule $schedule)
    {
        $update = $this->dispatch(new UpdateExistingSchedule($schedule));

        if ($update->success())
            return redirect()->route('admin.schedule.list')->with('success', 'Jadwal berhasil diubah');

        return redirect()->back()->with('updating schedule failed');
    }

    /**
     * Method to destroy/delete existing schedule
     *
     * Path : /jadwal/destroy/{schedule}
     * Name : admin.schedule.destroy
     * Method : destroy
     *
     * @param Schedule $schedule
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Schedule $schedule)
    {
        $destroy = $this->dispatch(new DeleteExistingSchedule($schedule));

        if ($destroy->success())
            return redirect()->route('admin.schedule.list')->with('success', 'Jadwal berhasil dihapus');

        return redirect()->back()->with('deleting schedule failed');
    }
}