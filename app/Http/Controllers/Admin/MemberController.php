<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Guest\Shared\ItemShared;
use App\Models\Accounts\Major;
use App\Models\Accounts\User;
use app\Modules\Account\UpdateAccount;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MemberController extends Controller
{
    use ItemShared;
    /**
     * Constant value for per page pagination
     *
     * @constant PAGINATE_ITEM
     */
    const PAGINATE_ITEM = 20;

    /**
     * Get all member list
     *
     * Name : admin.member
     * Path : /anggota
     * Method : GET
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index (Request $request)
    {
        $members = User::member()->orderBy('name', 'asc');

        if ($request->has('search')) {
            $members->search($request->input('search'));
        }

        return view('admin.member.index', [
            'members' => $members->paginate(self::PAGINATE_ITEM)
        ]);
    }

    /**
     * Show create member form
     *
     * Name : admin.member.create.form
     * Path : /anggota/buat
     * Method : GET
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showCreateForm ()
    {
        $this->getAllMajors();
        return view('admin.member.create');
    }

    /**
     * Create a new member
     *
     * Name : admin.member.store
     * Path : /anggota/buat
     * Method : POST
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store (Request $request)
    {
        $create = true;
        if ($create) {
            alert()->success('Anggota berhasil ditambahkan');
            return redirect()->route('admin.member');
        }
        alert()->error('Anggota gagal ditambahkan');
        return redirect()->back();
    }

    /**
     * Get single item member edit page
     *
     * Name : admin.member.edit.form
     * Path : /anggota/{username}/edit
     * Method : GET
     *
     * @param Request $request
     * @param User $member
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showEditForm (Request $request, User $member)
    {
        $this->getAllMajors();
        return view('admin.member.edit', [
            'member' => $member,
        ]);
    }

    /**
     * Update single member data
     *
     * Name : admin.member.update
     * Path : /anggota/{username}
     * Method : PUT
     *
     * @param Request $request
     * @param User $member
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update (Request $request, User $member)
    {
        $update = true;
        if ($update) {
            alert()->success('Anggota berhasil diubah');
            return redirect()->route('admin.member');
        }
        alert()->error('Anggota gagal diubah');
        return redirect()->back();
    }

    /**
     * Show single item member detail
     *
     * Name : admin.member.detail
     * Path : /anggota/{username}
     * Method : GET
     *
     * @param User $member
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show (User $member)
    {
        return view('admin.member.detail', [
            'member' => $member
        ]);
    }

    /**
     * Delete existing member
     *
     * Name : admin.member.destroy
     * Path : /anggota/{username}
     * Method : DELETE
     *
     * @param User $member
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy (User $member)
    {
        $delete = true;
        if ($delete) {
            alert()->success('Anggota berhasil dihapus');
            return redirect()->route('admin.member');
        }
        alert()->error('Anggota gagal dihapus');
        return redirect()->back();
    }
}