<?php
namespace App\Http\Controllers\Admin;

use \App\Http\Controllers\Controller;
/**
 * Created by PhpStorm.
 * User: Yustiko
 * Date: 4/7/2017
 * Time: 11:13 AM
 */
class DashboardController extends Controller
{
    /**
     * Controller used by DashboardRoute
     * \App\Http\Routes\Admin\DashboardRoute
     */

    /**
     * function to show index of dashboard page
     *
     * Name: admin.dashboard
     * Prefix: /beranda
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.dashboard.list');
    }

    /**
     * function to show list of all content in dashboard page
     *
     * Name: admin.dashboard.list
     * Prefix: /beranda/statistik
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function statistic()
    {
        return view('admin.dashboard.list');
    }

    /**
     * set default page for admin redirect to dashboard
     *
     * Name: admin
     * Prefix: /
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function defaultPage()
    {
        return $this->index();
    }
}