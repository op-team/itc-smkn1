<?php
/**
 * @author
 * User: Yustiko <yustiko404@gmail.com>
 * Date: 4/12/2017
 * Time: 11:25 AM
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\Contents\Post;
use App\Modules\Post\CreateNewPost;
use App\Modules\Post\DeleteExistingPost;
use App\Modules\Post\UpdateExistingPost;

class AchievementController extends Controller
{
    /**
     * set default property type used by this class
     *
     * @var string
     */
    protected $type = 'achievement';

    /**
     * Show index
     *
     * Path : /prestasi
     * Name : admin.achievement.index
     * Method : index
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return $this->list();
    }

    /**
     * show all list of achievement content
     *
     * @param int $paginateItem
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function list($paginateItem = 15)
    {
        $achievement = Post::type($this->type)->latest();

        if ($this->request->has('search'))
            $achievement->search($this->request->input('search'));

        return view('admin.achievement.list', [
            'achievements' => $achievement->paginate($paginateItem)
        ]);
    }

    /**
     * show create form for create achievement content
     *
     * Path: /prestasi/buat
     * Name : admin.achievement.create
     * Method : create
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.achievement.create');
    }

    /**
     * Show edit form for editing achievement content
     *
     * Path : /prestasi/ubah/{slug_achievement}
     * Name : admin.achievement.edit
     * Method : edit
     *
     * @param Post $achievement
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @internal param Post $post
     */
    public function edit(Post $achievement)
    {
        return view('admin.achievement.edit', [
            'achievement' => $achievement
        ]);
    }

    /**
     * method for storing new achievement content
     *
     * Path : /prestasi/store/{type?}
     * Name : admin.achievement.store
     * Method : store
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $store = $this->dispatch(new CreateNewPost(['type' => $this->type]));

         if ($store->success())
            return redirect()->route('admin.achievements')->with('success', 'Prestasi berhasil dibuat');

        return redirect()->back()->withErrors('errors', 'creating achievement failed');
    }

    /**
     * method updating existing achievement content
     *
     * Path : /prestasi/update/{slug_achievement}
     * Name : admin.achievement.update
     * Method : update
     *
     * @param Post $achievement
     * @return \Illuminate\Http\RedirectResponse
     * @internal param Post $post
     */
    public function update(Post $achievement)
    {
        $update = $this->dispatch(new UpdateExistingPost($achievement));

        if ($update->success())
            return redirect()->route('admin.achievements')->with('success', 'Prestasi berhasil diubah');

        return redirect()->back()->withErrors('errors', 'Updating Achievement failed');
    }

    /**
     * Method for deleting existing achievement content
     *
     * Path : /prestasi/destroy/{slug_achievement}
     * Name : admin.achievement.destroy
     * method : destroy
     * @param Post $achievement
     * @return \Illuminate\Http\RedirectResponse
     * @internal param Post $post
     */
    public function destroy(Post $achievement)
    {
        $destroy = $this->dispatch(new DeleteExistingPost($achievement));

        if ($destroy->success())
            return redirect()->route('admin.achievements')->with('success', 'Prestasi berhasil dihapus');

        return redirect()->back()->withErrors('errors', 'Deleting Achievement failed');
    }
}