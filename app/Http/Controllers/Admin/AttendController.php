<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Accounts\User;
use App\Models\Contents\Schedule;
use App\Modules\CreateOrUpdateAttend;

/**
 * @author <yustiko404@gmail.com> 4/28/2017 1:58 PM
 */
class AttendController extends Controller
{
    /**
     * Show index
     * Method to show default path
     *
     * Path : /hadir
     * Name : admin.attend
     * Method : index
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return $this->list();
    }

    /**
     * Show all list of attending user
     *
     * Path : /hadir/daftar-hadir
     * Name : admin.attend.list
     * Method : list
     *
     * @param int $paginate
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function list($paginate = 15)
    {
        $schedule = Schedule::OrderBy('published_at', 'asc');

        if ($this->request->has('search'))
            $schedule->search($this->request->input('search'));

        return view('admin.attend.list', [
           'schedules' => $schedule->paginate($paginate)
        ]);
    }

    /**
     * method to show form for creating attending list
     *
     * Path : /hadir/buat/{schedule}
     * Name : admin.attend.create
     * Method : create
     *
     * @param Schedule $schedule
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Schedule $schedule)
    {
        $users = User::orderBy('grade', 'asc')->get();

        return view('admin.attend.create', [
            'schedule' => $schedule,
            'users' => $users
        ]);
    }

    /**
     * method to show form for editing list attending
     *
     * Path : /hadir/ubah/{schedule}
     * Name : admin.attend.edit
     * Method : edit
     *
     * @param Schedule $schedule
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Schedule $schedule)
    {
        $user = User::orderBy('grade', 'asc')->get();

        return view('admin.attend.edit', [
           'schedule' => $schedule,
           'users' => $user
        ]);
    }

    /**
     * method for saving attending user
     *
     * Path : /hadir/store/{schedule}
     * Name : admin.attend.store
     * Method : store
     *
     * @param Schedule $schedule
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Schedule $schedule)
    {
        $store = dispatch(new CreateOrUpdateAttend($schedule));

        if ($store->success())
            return redirect()->route('admin.attend.list')->with('success', 'daftar hadir berhasil dibuat');

        return redirect()->back()->with('errors', 'creating attending has been failed');
    }

    /**
     * method for updating attending user
     *
     * Path : /hadir/update/{schedule}
     * Name : admin.attend.update
     * Method : update
     *
     * @param Schedule $schedule
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Schedule $schedule)
    {
        $update = $this->dispatch(new CreateOrUpdateAttend($schedule));

        if ($update->success())
            return redirect()->route('admin.attend.list')->with('success', 'daftar hadir berhasil diubah');

        return redirect()->back()->with('errors', 'updating attending has been failed');
    }
}