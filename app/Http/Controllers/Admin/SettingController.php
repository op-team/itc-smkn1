<?php
/**
 * Created by PhpStorm.
 * User: Yustiko
 * Date: 4/25/2017
 * Time: 4:56 PM
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\Extra\Setting;
use App\Modules\Setting\ConfigurationSetting;
use app\Modules\Setting\DeleteExistingSetting;

class SettingController extends Controller
{
    /**
     * Show index
     *
     * Path : /pengaturan
     * Name : admin.setting
     * Method : index
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return $this->list();
    }

    /**
     * Show list of all setting
     *
     * Path : /pengaturan/semua-setting
     * Name : admin.setting.list
     * Method : list
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function list()
    {
        $setting = Setting::orderBy('key', 'asc')->get();
        return view('admin.setting.list', [
           'settings' => $setting
        ]);
    }

    /**
     * method to store and update setting
     *
     * Path : /pengaturan/store/{key}
     * Name : admin.setting.store
     * Method : store
     *
     * @param Setting $setting
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Setting $setting)
    {
        $store = $this->dispatch(new ConfigurationSetting($setting));

        if ($store->success())
            return redirect()->route('admin.setting.list')->with('success', 'Konfigurasi Pengaturan Berhasil');

        return redirect()->back()->with('errors', 'configuration setting has been failed');
    }

    /**
     * method to delete existing setting
     *
     * Path : /pengaturan/destroy/{key}
     * Name : admin.setting.destroy
     * Method : destroy
     *
     * @param Setting $setting
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Setting $setting)
    {
        $destroy = $this->dispatch(new DeleteExistingSetting($setting));

        if ($destroy->success())
            return redirect()->route('admin.setting.list')->with('success', 'Pengaturan berhasil dihapus');

        return redirect()->back()->with('errors', 'Deleting Setting failed');
    }
}