<?php

namespace App\Http\Controllers\Auth;

use App\Models\Accounts\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class MemberController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('guest', [
            'except' => 'logout'
        ]);
    }

    /**
     * Show a guest login form
     *
     * Name   : auth.member.logout
     * Prefix : member/login
     */
    public function showLoginForm()
    {
        return view('guest.auth.login');
    }

    /**
     * perform a login action
     *
     * Name   : auth.member.login
     * Prefix : member/login
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function login(Request $request)
    {
        $this->validate($request->all(), [
            'username' => 'required',
            'password' => 'required',
        ]);

        $login = Auth::attempt([
            'username' => $request->input('username'),
            'password' => $request->input('password'),
            'role' => User::MEMBER
        ], $request->input('remember'));

        if (!$login)
            return redirect()->back()->withErrors('Pengguna dan kata sandi tidak cocok');

        return redirect()->route('member.dashboard.index')->with('success', 'Login Berhasil');
    }

    /**
     * Logout member
     *
     * Name   : auth.member.logout
     * Prefix : member/logout
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }
}
