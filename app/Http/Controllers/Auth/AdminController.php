<?php

namespace App\Http\Controllers\Auth;

use App\Models\Accounts\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('guest', [
            'except' => 'logout'
        ]);
    }

    /**
     * Show a login form
     *
     * Name   : admin.login
     * Prefix : auth/admin/login.html
     */
    public function showLoginForm()
    {
        return view('admin.auth.login');
    }

    /**
     * perform a login action
     *
     * Name   : auth.admin.login
     * Prefix : auth/admin/login
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function login(Request $request)
    {
        $this->validate($request->all(), [
            'username' => 'required',
            'password' => 'required',
        ]);

        $login = Auth::attempt([
            'username' => $request->input('username'),
            'password' => $request->input('password'),
            'role' => User::ADMIN
        ], $request->input('remember'));

        if (!$login)
            return redirect()->back()->withErrors('username dan password tidak cocok');

        return redirect()->route('admin.dashboard')->with('basic', 'Selamat Datang!');
    }

    /**
     * Logout admin
     *
     * Name   : admin.logout
     * Prefix : auth/admin/logout
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }
}
