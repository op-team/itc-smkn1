<?php

namespace App\Http\Controllers\Guest;


use App\Http\Controllers\Guest\Shared\ItemShared;
use App\Models\Accounts\User;
use App\Http\Controllers\Controller;

class MemberController extends Controller
{

    use ItemShared;

    /**
     * show a member page in guest
     * Name   : guest.member
     * Prefix : member
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $this->getAllMember();
        return view('guest.member.index');
    }

    /**
     * Show a member personal information and contribution
     * Name   : guest.member.detail
     * Prefix : member/{username}
     *
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(User $user)
    {
        return view('guest.member.detail', [
            'member' => $user
        ]);
    }
}
