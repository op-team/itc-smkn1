<?php

namespace App\Http\Controllers\Guest;


use App\Http\Controllers\Guest\Shared\ItemShared;
use App\Models\Contents\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GalleryController extends Controller
{

    use ItemShared;

    /**
     * show all of available gallery
     * Name  : guest.gallery
     * Prefix: /gallery
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @internal param Request $request
     */
    public function index()
    {
        $this->getAllGallery();

        return view('guest.gallery.index');
    }

    /**
     * return a json for detail gallery
     * Name   : guest.gallery.detail
     * Prefix : /gallery/{slug}
     *
     * @param Post $post
     * @return mixed
     * @internal param $slug
     */
    public function show(Post $post)
    {
        return view('guest.gallery.detail', [
            'gallery' => $post
        ]);
    }
}
