<?php

namespace App\Http\Controllers\Guest;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Guest\Shared\ItemShared;
use App\Models\Contents\Media;
use App\Models\Contents\Post;

class HomeController extends Controller
{
    use ItemShared;

    /**
     * Show homepage of ITClub
     *
     * Name   : guest.home
     * Prefix : /
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $photos = Media::whereHas('post', function ($query) {
            $query->where('posts.type', 'gallery');
        })->inRandomOrder()->get();
        $this->setData('photos', $photos);
        $this->setData('achievements', Post::type('achievement')->latest()->take(3)->get());
        return view('guest.homepage');
    }

}
