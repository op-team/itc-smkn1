<?php

namespace App\Http\Controllers\Guest;

use App\Http\Controllers\Guest\Shared\ItemShared;
use App\Models\Contents\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AchievementController extends Controller
{

    use ItemShared;

    public function index ()
    {
        $this->getAllAchievement();

        return view('guest.achievement.index');
    }

    public function show (Post $post)
    {
        return view('guest.achievement.detail',[
            'achievement' => $post->type('achievement')->firstOrFail()
        ]);
    }
}
