<?php

namespace App\Http\Controllers\Guest;

use App\Http\Controllers\Guest\Shared\ItemShared;
use App\Http\Controllers\Controller;

class ExtraController extends Controller
{
    use ItemShared;

    public function faqs ()
    {
        $this->getFaqs();
        return view('guest.about.faqs');
    }

    public function contacts ()
    {
        $this->getSetting('contacts');
        return view('guest.about.contacts');
    }
}
