<?php

namespace App\Http\Controllers\Guest;

use App\Http\Controllers\Guest\Shared\ItemShared;
use App\Models\Additional\Comment;
use App\Models\Contents\Post;
use App\Http\Controllers\Controller;
use App\Modules\Comment\CreateNewComment;
use App\Modules\Comment\DeleteExistingComment;
use App\Modules\Like\LikeContent;
use App\Modules\Like\UnlikeContent;

class PostController extends Controller
{

    use ItemShared;

    /**
     * show all of published post
     * in the form search use input[name=search] for get search result
     *
     * Name  : guest.post
     * Prefix: /post
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @internal param Request $request
     */
    public function index()
    {
        $this->getAllPosts();

        return view('guest.post.index');
    }

    /**
     * show a page for detail post
     * Name   : guest.post.detail
     * Prefix : /post/{slug}
     *
     * @param Post $post
     * @return mixed
     * @internal param $slug
     */
    public function show(Post $post)
    {
        return view('guest.post.detail', [
            'post' => $post->isPublished()
        ]);
    }

    public function like(Post $post)
    {
        $like = $this->dispatch(new LikeContent([], $post));

        if ($like->success()) {
            return redirect()->back()->with('success', 'Disukai');
        }

        return redirect()->back()->with('errors', '500 (Error Server)');
    }

    public function unlike(Post $post)
    {
        $like = $this->dispatch(new UnlikeContent([], $post));

        if ($like->success()) {
            return redirect()->back()->with('success', 'Batal disukai');
        }

        return redirect()->back()->with('errors', '500 (Error Server)');
    }

    public function comment(Post $post)
    {
        $comment = $this->dispatch(new CreateNewComment([], $post));

        if ($comment->success()) {
            return redirect()->back()->with('success', 'Komentar terpublis');
        }

        return redirect()->back()->with('errors', '500 (Error Server)');
    }

    public function commentDelete(Post $post, Comment $comment)
    {
        $comment = $this->dispatch(new DeleteExistingComment([], $comment));

        if ($comment->success()) {
            return redirect()->back()->with('success', 'Komentar terhapus');
        }

        return redirect()->back()->with('errors', '500 (Error Server)');
    }
}
