<?php

namespace App\Http\Controllers\Guest;


use App\Http\Controllers\Guest\Shared\ItemShared;
use App\Http\Controllers\Controller;
use App\Models\Contents\Schedule;

class ScheduleController extends Controller
{

    use ItemShared;

    /**
     * Show page of ITClub schedule
     *
     * Name   : guest.schedules
     * Prefix : /schedules
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $this->getSchedules();

        return view('guest.schedule.index');
    }

    /**
     * Show page of ITClub schedule detail
     *
     * Name   : guest.schedules.show
     * Prefix : /schedules/{schedule}
     *
     * @param Schedule $schedule
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show (Schedule $schedule)
    {
        return view('guest.schedule.detail', [
            'schedule' => $schedule->isPublished()
        ]);
    }

}
