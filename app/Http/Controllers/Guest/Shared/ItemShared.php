<?php

namespace App\Http\Controllers\Guest\Shared;

use App\Models\Accounts\Major;
use App\Models\Accounts\User;
use App\Models\Contents\Post;
use App\Models\Contents\Schedule;
use App\Models\Extra\Faq;
use App\Models\Extra\Setting;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 26/03/17 , 14:35
 */
trait ItemShared
{
    /**
     * Retrieve all of published post
     * 
     * Retrieve : $posts 
     * 
     * @param int $paginateItem
     * @return mixed
     */
    public function getAllPosts($paginateItem = 15)
    {
        $posts = Post::orderBy('published_at')->with('media')->type('post')->published();
        if ($this->request->has('search'))
            $posts->search($this->request->input('search'));
        
        return $this->setData('posts', $posts->paginate($paginateItem));
    }

    /**
     * Receive all of achievement
     *
     * Retrieve : $achievements
     *
     * @param int $paginateItem
     * @return mixed
     */
    public function getAllAchievement($paginateItem = 15)
    {
        $posts = Post::orderBy('published_at')->type('achievement');
        if ($this->request->has('search'))
            $posts->search($this->request->input('search'));

        return $this->setData('achievements', $posts->paginate($paginateItem));
    }

    /**
     * Retrieve all of published gallery
     *
     * Retrieve : $galleries
     *
     * @param int $paginateItem
     * @return mixed
     */
    public function getAllGallery($paginateItem = 15)
    {
        $gallery = Post::orderBy('published_at')->type('gallery');
        if ($this->request->has('search'))
            $gallery->search($this->request->input('search'));

        return $this->setData('galleries', $gallery->paginate($paginateItem));
    }

    /**
     * Retrieve all of members
     *
     * Retrieve : $members
     * @param int $paginateItem
     * @return mixed
     */
    public function getAllMember($paginateItem = 20)
    {
        $members = User::member()->inRandomOrder();

        return $this->setData('members', $members->paginate($paginateItem));
    }

    /**
     * Retrieve all majors
     *
     * Retrieve : $majors
     *
     * @return mixed
     */
    public function getAllMajors ()
    {
        $majors = Major::orderBy('name', 'asc')->get();
        return $this->setData('majors', $majors);
    }

    /**
     * Retrieve schedules
     *
     * Retrieve : $schedules
     *
     * @param bool|integer $limit
     * @param integer $paginateItem
     * @return mixed
     */
    public function getSchedules($limit = false, $paginateItem = 15)
    {
        $schedules = Schedule::orderBy('start_date', 'desc')->published();
        (!$limit) ?: $schedules->limit($limit);

        return $this->setData('schedules', $schedules->paginate($paginateItem));
    }

    /**
     * Retrieve all of faqs in database
     *
     * Retrieve : $faqs
     *
     * @return mixed
     */
    public function getFaqs()
    {
        return $this->setData('faqs', Faq::all());
    }

    /**
     * Retrieve a value of requested key
     *
     * Retrieve : $(key as you want)
     *
     * @param $key
     * @return mixed
     */
    public function getSetting($key)
    {
        $value = Setting::where('key', $key)->firstOrfail();

        return $this->setData($key, $value->value);
    }

}