<?php

namespace App\Http\Controllers;

use App\Models\Accounts\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\View;
use Illuminate\Validation\ValidationException;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Validator;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $request;

    public function __construct()
    {
        $this->request = app('request');

        if (auth()->check())
            $this->validateTypeProperty();

    }

    public function validateTypeProperty()
    {
        if (property_exists($this, 'type')){
            $array = config('post-type.public');

            if (auth()->user()->role === User::ADMIN)
                array_push($array, config('post-type.protected'));

            if (!in_array($this->type, $array))
                throw new AuthorizationException("property {$this->type} is not available");
        }
    }

    /**
     * Set data for sharing with blade
     *
     * @param $key
     * @param $val
     * @return $this
     */
    public function setData($key, $val)
    {
        View::share($key, $val);
        return $this;
    }

    public function error($type, $details = null)
    {
        $code = 500;
        $message = 'Can\'t process data';
        switch ($type) {
            case 'form' : $code = 422; $message = 'Check your form field'; break;
            case 'server' : $message = 'Can\'t process data'; break;
            case 'upload' : $message = 'Can\'t upload file'; break;
            case 'exists' : $code = 422; $message = 'Data is exists'; break;
        }

        return response()->json([
            'message' => $message,
            'errors' => $details
        ], $code);
    }

    public function success($type)
    {
        $code = 200;
        $message = 'Data proceed';
        switch ($type) {
            case 'create' : $code = 201; $message = 'Data created'; break;
            case 'edit' : $message = 'Data updated'; break;
            case 'delete' : $message = 'Data deleted'; break;
        }

        return response()->json([
            'message' => $message
        ], $code);
    }

    public function validate(array $request, array $rules, array $messages = [], array $customAttributes = [])
    {
        $validator = Validator::make($request, $rules, $messages, $customAttributes);
        $response = redirect()->back()->withInput()->withErrors($validator);
//        $response = $this->error('form', $validator->errors());
        if ($validator->fails()) {
            throw new ValidationException($validator, $response);
        }
    }

    public function response ($data, $dataType, $includes = [], $isCollection = false)
    {
        $type = $isCollection ? 'collection' : 'item';
        $class = studly_case($dataType);
        $transformer = "App\\Transformers\\{$class}Transformer";

        if (method_exists($data, 'total')) {
            $paginator = $data;
            $data = $data->getCollection();
        }

        $response = fractal()
            ->{$type}($data->toArray(), new $transformer, strtolower($class))
            ->parseIncludes($includes);
        !isset ($paginator) ? : $response->paginateWith(new IlluminatePaginatorAdapter($paginator));

        return response()->json($response->toArray());
    }
}
