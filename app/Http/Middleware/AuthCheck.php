<?php

namespace App\Http\Middleware;

use App\Models\Accounts\User;
use Closure;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Support\Facades\Auth;

class AuthCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param $role
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        if (!Auth::check()) {
            $this->doIfCheckFailed();
        }

        $this->protectionRole($role);

        return $next($request);
    }

    /**
     * This method is so suck . if someone have some idea please tell me
     *
     * @param $role
     */
    protected function protectionRole($role)
    {
        switch ($role){
            case User::ADMIN :
                if (Auth::user()->role != User::ADMIN)
                    $this->doIfCheckFailed();
                break;

            case User::MEMBER :
                if (Auth::user()->role != User::MEMBER)
                    $this->doIfCheckFailed();
                break;

            default :
                $this->doIfCheckFailed();
                break;
        }
    }

    /**
     * declare action if checking failed
     *
     * @throws AuthenticationException
     */
    protected function doIfCheckFailed()
    {
        throw new AuthenticationException('Unauthenticated.');
    }
}
