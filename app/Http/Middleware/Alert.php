<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\ViewErrorBag;

class Alert
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->session()->has('success')) {
            alert()->success($request->session()->get('success'), 'Berhasil');
        }

        if ($request->session()->has('warning')) {
            alert()->warning($request->session()->get('warning'), 'Awas');
        }

        if ($request->session()->has('info')) {
            alert()->info($request->session()->get('info'), 'Info');
        }

        if ($request->session()->has('message')) {
            alert()->message($request->session()->get('message'), 'Pesan');
        }

        if ($request->session()->has('basic')) {
            alert()->basic($request->session()->get('basic'),'');
        }

        if ($request->session()->has('errors')) {
            $errors = $request->session()->get('errors');

            if ($errors instanceof ViewErrorBag) {
                /** @var $message ViewErrorBag*/
                $message = $this->prepareErrors($errors->getBag('default')->toArray());
                if (! $errors->count() == 0)
                    alert()->error((string) $message, 'Error!')->persistent();
            } else {
                alert()->error((string) $errors, 'Error!')->persistent();
            }
        }

        return $next($request);
    }

    /**
     * Retrieve the errors from ViewErrorBag.
     *
     * @param $errors
     *
     * @return string
     */
    private function prepareErrors($errors)
    {
        $errors = collect($errors);

        return $errors->flatten()->implode('');
    }
}
