<?php

namespace App\Http\Routes\Member;

use App\Core\Routing\SelfBindingRoute;
use App\Http\Controllers\Member\DashboardController;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 03/04/17 , 22:40
 */
class Dashboard extends SelfBindingRoute
{

    protected $prefix = '/dashboard';

    protected $name = 'dashboard';

    /**
     * Register routes handled by this class.
     *
     * @return void
     */
    public function register()
    {
        $this->router->get($this->prefix, [
            'uses' => $this->uses('index'),
            'as' => $this->name('index')
        ]);
        $this->router->get($this->prefix('tulisan'), [
            'uses' => $this->uses('posts'),
            'as' => $this->name('posts')
        ]);
        $this->router->get($this->prefix('akun'), [
            'uses' => $this->uses('account'),
            'as' => $this->name('account')
        ]);
    }

    public function controller()
    {
        return DashboardController::class;
    }
}