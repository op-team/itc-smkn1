<?php

namespace App\Http\Routes\Member;

use App\Core\Routing\SelfBindingRoute;
use App\Http\Controllers\Member\PostEpisodeController;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 05/04/17 , 16:19
 */
class PostEpisode extends SelfBindingRoute
{
    protected $prefix = 'tulisan/episode/';

    protected $name = 'posts.episode';

    /**
     * Register routes handled by this class.
     *
     * @return void
     */
    public function register()
    {
        $this->router->get($this->prefix('/edit-episode/{slug_post}'), [
            'uses' => $this->uses('showEditForm'),
            'as' => $this->name('edit.form')
        ]);

        $this->router->get($this->prefix('{slug_post}'), [
            'uses' => $this->uses('showList'),
            'as' => $this->name('show')
        ]);

        $this->router->get($this->prefix('{slug}/isi-episode'), [
            'uses' => $this->uses('showCreateForm'),
            'as' => $this->name('create.form')
        ]);

        $this->router->post($this->prefix('/edit-episode/{slug_post}'), [
            'uses' => $this->uses('update'),
            'as' => $this->name('update')
        ]);

        $this->router->post($this->prefix('/delete-episode/{slug_post}'), [
            'uses' => $this->uses('destroy'),
            'as' => $this->name('delete')
        ]);

        $this->router->post($this->prefix('{slug_post}/isi-episode'), [
            'uses' => $this->uses('create'),
            'as' => $this->name('create')
        ]);

    }

    public function controller()
    {
        return PostEpisodeController::class;
    }
}