<?php

namespace App\Http\Routes\Member;

use App\Core\Routing\SelfBindingRoute;
use App\Http\Controllers\Member\AccountController;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 07/04/17 , 13:08
 */
class Account extends SelfBindingRoute
{

    protected $prefix = 'akun';

    protected $name = 'account';

    /**
     * Register routes handled by this class.
     *
     * @return void
     */
    public function register()
    {
        $this->router->get($this->prefix, [
            'uses' => $this->uses('showChangeProfileForm'),
            'as' => $this->name('profile.form')
        ]);

        $this->router->get($this->prefix('password'), [
            'uses' => $this->uses('showChangePasswordForm'),
            'as' => $this->name('password.form')
        ]);

        $this->router->post($this->prefix, [
            'uses' => $this->uses('changeProfile'),
            'as' => $this->name('profile')
        ]);

        $this->router->post($this->prefix('password'), [
            'uses' => $this->uses('changePassword'),
            'as' => $this->name('password')
        ]);
    }

    public function controller()
    {
        return AccountController::class;
    }
}