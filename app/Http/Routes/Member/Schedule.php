<?php

namespace App\Http\Routes\Member;

use App\Core\Routing\SelfBindingRoute;
use App\Http\Controllers\Member\ScheduleController;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 08/04/17 , 11:34
 */
class Schedule extends SelfBindingRoute
{

    protected $prefix = 'jadwal';

    protected $name = 'schedule';

    /**
     * Register routes handled by this class.
     *
     * @return void
     */
    public function register()
    {
        $this->router->get($this->prefix, [
            'uses' => $this->uses('index'),
            'as' => $this->name
        ]);

        $this->router->get($this->prefix('{schedule}'), [
            'uses' => $this->uses('show'),
            'as' => $this->name('show')
        ]);
    }

    public function controller()
    {
        return ScheduleController::class;
    }
}