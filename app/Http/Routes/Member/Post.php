<?php

namespace App\Http\Routes\Member;

use App\Core\Routing\SelfBindingRoute;
use App\Http\Controllers\Member\PostController;
use App\Http\Controllers\Member\PostEpisodeController;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 04/04/17 , 9:59
 */
class Post extends SelfBindingRoute
{

    protected $prefix = 'tulisan';

    protected $name = 'posts';

    /**
     * Register routes handled by this class.
     *
     * @return void
     */
    public function register()
    {
        $this->router->get($this->prefix, [
            'uses' => $this->uses('index'),
            'as' => $this->name
        ]);

        $this->router->get($this->prefix('tulis'), [
            'uses' => $this->uses('showCreatePostForm'),
            'as' => $this->name('create.form')
        ]);

        $this->router->get($this->prefix('tulis-video'), [
            'uses' => $this->uses('showCreatePostVideoForm'),
            'as' => $this->name('create-video.form')
        ]);

        $this->router->get($this->prefix('/edit/{slug_post}'), [
            'uses' => $this->uses('showEditForm'),
            'as' => $this->name('edit.form')
        ]);

        $this->router->post($this->prefix('/create/{type?}'), [
            'uses' => $this->uses('create'),
            'as' => $this->name('create')
        ]);

        $this->router->post($this->prefix('/update/{slug_post}'), [
            'uses' => $this->uses('update'),
            'as' => $this->name('update')
        ]);

        $this->router->post($this->prefix('/delete/{slug_post}'), [
            'uses' => $this->uses('destroy'),
            'as' => $this->name('delete')
        ]);
    }

    public function controller()
    {
        return PostController::class;
    }
}