<?php

namespace App\Http\Routes\Guest;

use App\Core\Routing\SelfBindingRoute;
use App\Http\Controllers\Guest\MemberController;


class Member extends SelfBindingRoute
{

    protected $prefix = 'anggota';

    protected $name = 'guest.members';

    /**
     * Register routes handled by this class.
     *
     * @return void
     */
    public function register()
    {
        $this->router->get($this->prefix('/'), [
            'uses' => $this->uses('index'),
            'as' => $this->name,
        ]);

        $this->router->get($this->prefix('{username}'), [
            'uses' => $this->uses('show'),
            'as' => $this->name('detail')
        ]);
    }

    public function controller()
    {
        return MemberController::class;
    }
}