<?php

namespace App\Http\Routes\Guest;

use App\Core\Routing\SelfBindingRoute;
use App\Http\Controllers\Guest\ExtraController;
use App\Http\Controllers\Guest\Shared\ItemShared;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 29/03/17 , 21:01
 *
 * NOTE : write this route closure because is just have a one page
 */
class Extra extends SelfBindingRoute
{

    use ItemShared;

    protected $name = 'guest';

    /**
     * Register routes handled by this class.
     *
     * @return void
     */
    public function register()
    {
        $this->router->get('faqs', [
            'uses' => $this->uses('faqs'),
            'as' => $this->name('faqs'),
        ]);

        $this->router->get('kontak', [
            'uses' => $this->uses('contacts'),
            'as' => $this->name('contacts'),
        ]);
    }

    public function controller ()
    {
        return ExtraController::class;
    }
}