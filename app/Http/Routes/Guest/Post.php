<?php

namespace App\Http\Routes\Guest;

use App\Core\Routing\SelfBindingRoute;
use App\Http\Controllers\Guest\PostController;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 24/03/17 , 13:01
 */
class Post extends SelfBindingRoute
{

    protected $prefix = 'tulisan';

    protected $name = 'guest.posts';

    /**
     * Register routes handled by this class.
     *
     * @return void
     */
    public function register()
    {
        $this->router->get($this->prefix('/'), [
            'uses' => $this->uses('index'),
            'as' => $this->name
        ]);

        $this->router->get($this->prefix('/{slug_post}'), [
            'uses' => $this->uses('show'),
            'as' => $this->name('detail')
        ]);

        $this->router->post($this->prefix('/{slug_post}/like'), [
            'uses' => $this->uses('like'),
            'as' => $this->name('like')
        ]);

        $this->router->post($this->prefix('/{slug_post}/unlike'), [
            'uses' => $this->uses('unlike'),
            'as' => $this->name('unlike')
        ]);

        $this->router->post($this->prefix('/{slug_post}/comment'), [
            'uses' => $this->uses('comment'),
            'as' => $this->name('comment')
        ]);

        $this->router->post($this->prefix('/{slug_post}/delete-comment/{comment}'), [
            'uses' => $this->uses('deleteComment'),
            'as' => $this->name('delete-comment')
        ]);
    }

    public function controller()
    {
        return PostController::class;
    }
}