<?php
/**
 * Created by PhpStorm.
 * User: rendy
 * Date: 24/03/17
 * Time: 14:18
 */

namespace App\Http\Routes\Guest;


use App\Core\Routing\SelfBindingRoute;
use App\Http\Controllers\Guest\AchievementController;

class Achievement extends SelfBindingRoute
{
    protected $name = 'guest.achievements';
    protected $prefix = 'prestasi';

    public function register()
    {
        $this->router->get($this->prefix('/'), [
            'uses' => $this->uses('index'),
            'as' => $this->name
        ]);

        $this->router->get($this->prefix('/{post}'), [
            'uses' => $this->uses('show'),
            'as' => $this->name('detail')
        ]);
    }

    public function controller ()
    {
        return AchievementController::class;
    }
}