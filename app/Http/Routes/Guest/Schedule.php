<?php

namespace App\Http\Routes\Guest;

use App\Core\Routing\SelfBindingRoute;
use App\Http\Controllers\Guest\ScheduleController;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 26/03/17 , 14:28
 */
class Schedule extends SelfBindingRoute
{
    protected $prefix = 'jadwal';

    protected $name = 'guest.schedules';

    /**
     * Register routes handled by this class.
     *
     * @return void
     */
    public function register()
    {
        $this->router->get($this->prefix('/'), [
            'uses' => $this->uses('index'),
            'as' => $this->name
        ]);

        $this->router->get($this->prefix('/{schedule}'), [
            'uses' => $this->uses('show'),
            'as' => $this->name('detail')
        ]);
    }

    public function controller()
    {
        return ScheduleController::class;
    }
}