<?php

namespace App\Http\Routes\Guest;

use App\Core\Routing\SelfBindingRoute;
use App\Http\Controllers\Guest\GalleryController;


/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 24/03/17 , 13:01
 */
class Gallery extends SelfBindingRoute
{

    protected $prefix = 'galeri';

    protected $name = 'guest.galleries';

    /**
     * Register routes handled by this class.
     *
     * @return void
     */
    public function register()
    {
        $this->router->get($this->prefix('/'), [
            'uses' => $this->uses('index'),
            'as' => $this->name
        ]);

        $this->router->get($this->prefix('/{slug_gallery}'), [
            'uses' => $this->uses('show'),
            'as' => $this->name('detail')
        ]);
    }

    public function controller()
    {
        return GalleryController::class;
    }
}