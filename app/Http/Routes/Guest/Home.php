<?php

namespace App\Http\Routes\Guest;

use App\Core\Routing\SelfBindingRoute;
use App\Http\Controllers\Guest\HomeController;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 14/03/17 , 21:30
 */
class Home extends SelfBindingRoute
{

    protected $name = 'guest.home';

    /**
     * Register routes handled by this class.
     *
     * @return void
     */
    public function register()
    {
        $this->router->get($this->prefix('/'), [
            'uses' => $this->uses('index'),
            'as' => $this->name
        ]);

    }

    public function controller()
    {
        return HomeController::class;
    }
}