<?php

namespace App\Http\Routes;

use App\Core\Routing\SelfBindingRoute;
use App\Models\Accounts\User;
use App\Models\Additional\Comment;
use App\Models\Contents\Media;
use App\Models\Contents\Post;
use App\Models\Contents\Schedule;
use App\Models\Extra\Faq;
use App\Models\Extra\Setting;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 24/03/17 , 14:04
 */
class Binder extends SelfBindingRoute
{

    /**
     * Register routes handled by this class.
     *
     * @return void
     */
    public function register()
    {
        // This route is for binding a username route variable
        $this->router->bind('username', function ($value){
            return User::where('username', $value)->firstOrFail();
        });

        $this->router->bind('slug_post', function ($value){
            return Post::where('slug', $value)->type('post')->firstOrFail();
        });

        $this->router->bind('slug_gallery', function ($value){
            return Post::where('slug', $value)->type('gallery')->firstOrFail();
        });

        $this->router->bind('slug_achievement', function ($value){
            return Post::where('slug', $value)->type('achievement')->firstOrFail();
        });

        $this->router->model('slug', Post::class);

        // This route is for binding a schedule route variable
        $this->router->bind('schedule', function ($value){
            return Schedule::where('slug', $value)->firstOrFail();
        });

        // This route is for binding a media route variable
        $this->router->bind('media', function ($value) {
           return Media::where('slug', $value)->firstOrFail();
        });

        $this->router->bind('comment', function ($value) {
            return Comment::find($value);
        });

        // This route is for binding a setting route variable
        $this->router->bind('key', function ($value) {
            return  Setting::where('key', $value)->firstOrFail();
        });

        // This route is for binding a faqs route variable
        $this->router->bind('id', function ($value) {
           return Faq::where('id', $value)->firstOrFail();
        });
    }
}