<?php

namespace App\Http\Routes;

use App\Core\Routing\SelfBindingRoute;
use App\Models\Contents\Media as MediaModel;
use Illuminate\Support\Facades\Storage;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 19/04/17 , 22:37
 */
class Media extends SelfBindingRoute
{

    /**
     * Register routes handled by this class.
     *
     * @return void
     */
    public function register()
    {
        $this->router->get('media/{media}', [
            'as' => 'media',
            function(MediaModel $media) {
                $path = $media->src ;
                return response()->make(Storage::disk('public')->get($path))
                    ->header('Content-Type', Storage::disk('public')->MimeType($path))
                    ->header('Content-Length', Storage::disk('public')->size($path))
                    ->header('Last-Modified', Storage::disk('public')->lastModified($path))
                    ->header('Pragma', 'public')
                    ->header('Cache-Control', 'max-age=1600')
                    ->header('Expires', gmdate('D, d M Y H:i:s \G\M\T', time() + 1600));
            }
        ]);
    }
}