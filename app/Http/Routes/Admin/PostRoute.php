<?php
/**
 * Created by PhpStorm.
 * User: Yustiko
 * Date: 4/7/2017
 * Time: 1:41 PM
 */

namespace App\Http\Routes\Admin;


use App\Core\Routing\SelfBindingRoute;
use App\Http\Controllers\Admin\PostController;

class PostRoute extends SelfBindingRoute
{
    /**
     * make default name used by this route
     */
    protected $name = 'admin.post';

    /**
     * make default prefix used by this route
     */
    protected $prefix = 'tulisan';

    /**
     * Register routes handled by this class.
     *
     * @return void
     */
    public function register()
    {
        $this->router->get($this->prefix('/'), [
            'as' => $this->name,
            'uses' => $this->uses('index')
        ]);

        $this->router->get($this->prefix('/daftar-tulisan'), [
           'as' => $this->name('list'),
            'uses' => $this->uses('statistic')
        ]);

        $this->router->get($this->prefix('/buat'), [
           'as' => $this->name('create'),
            'uses' => $this->uses('create')
        ]);

        $this->router->get($this->prefix('/buat/vidio'), [
           'as' => $this->name('create.video'),
            'uses' => $this->uses('createVideo')
        ]);

        $this->router->get($this->prefix('/publis/{slug_post}'), [
            'as' => $this->name('publish'),
             'uses' => $this->uses('publish')
        ]);

        $this->router->get($this->prefix('/ubah/{slug_post}'), [
           'as' => $this->name('edit'),
            'uses' => $this->uses('edit')
        ]);

        $this->router->post($this->prefix('/store/{type?}'), [
           'as' => $this->name('store'),
            'uses' => $this->uses('store')
        ]);

        $this->router->post($this->prefix('/update/{slug_post}'), [
            'as' => $this->name('update'),
             'uses' => $this->uses('update')
        ]);

        $this->router->post($this->prefix('/destroy/{slug_post}'), [
           'as' => $this->name('destroy'),
            'uses' => $this->uses('destroy')
        ]);


    }

    /**
     * set default controller used by this class
     *
     * @return string
     */
    public function controller()
    {
        return PostController::class;
    }
}