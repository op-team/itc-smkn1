<?php

namespace App\Http\Routes\Admin;
use App\Core\Routing\SelfBindingRoute;
use App\Http\Controllers\Admin\FaqsController;

/**
 * @author <yustiko404@gmail.com> 4/28/2017 12:58 PM
 */
class FaqsRoute extends SelfBindingRoute
{
    /**
     * set default name used by this class
     *
     * @var string
     */
    protected $name = 'admin.faqs';

    /**
     * set default path used by this class
     *
     * @var string
     */
    protected $prefix = 'faqs';

    /**
     * Register routes handled by this class.
     *
     * @return void
     */
    public function register()
    {
        $this->router->get($this->prefix('/'), [
           'as' => $this->name,
           'uses' => $this->uses('index')
        ]);

        $this->router->get($this->prefix('/daftar-faqs'), [
            'as' => $this->name('list'),
            'uses' => $this->uses('list')
        ]);

        $this->router->get($this->prefix('/buat'), [
            'as' => $this->name('create'),
            'uses' => $this->uses('create')
        ]);

        $this->router->get($this->prefix('/ubah/{id}'), [
           'as' => $this->name('edit'),
           'uses' => $this->uses('edit')

        ]);

        $this->router->post($this->prefix('/store'), [
           'as' => $this->name('store'),
           'uses' => $this->uses('store')
        ]);

        $this->router->post($this->prefix('/update/{id}'), [
            'as' => $this->name('update'),
            'uses' => $this->uses('update')
        ]);

        $this->router->delete($this->prefix('/destroy/{id}'), [
            'as' => $this->name('destroy'),
            'uses' => $this->uses('destroy')
        ]);
    }

    /**
     * set default controller used by this class
     *
     * @return string
     */
    public function controller()
    {
        return FaqsController::class;
    }
}