<?php
/**
 * @author
 * User: Yustiko <yustiko404@gmail.com>
 * Date: 4/13/2017
 * Time: 11:36 AM
 */

namespace App\Http\Routes\Admin;


use App\Core\Routing\SelfBindingRoute;
use App\Http\Controllers\Admin\ScheduleController;

class ScheduleRoute extends SelfBindingRoute
{
    /**
     * set default name used by this class
     *
     * @var string
     */
    protected $name = 'admin.schedule';

    /**
     * set default path used by this class
     *
     * @var string
     */
    protected $prefix = 'jadwal';

    /**
     * Register routes handled by this class.
     *
     * @return void
     */
    public function register()
    {
        $this->router->get($this->prefix('/'), [
           'as' => $this->name,
            'uses' => $this->uses('index')
        ]);

        $this->router->get($this->prefix('/semua-jadwal'), [
           'as' => $this->name('list'),
            'uses' => $this->uses('list')
        ]);

        $this->router->get($this->prefix('/buat'), [
           'as' => $this->name('create'),
            'uses' => $this->uses('create')
        ]);

        $this->router->get($this->prefix('/ubah/{schedule}'), [
           'as' => $this->name('edit'),
            'uses' => $this->uses('edit'),
        ]);

        $this->router->get($this->prefix('/publis/{schedule}'), [
           'as' => $this->name('publish'),
            'uses' => $this->uses('publish')
        ]);

        $this->router->post($this->prefix('/store'), [
           'as' => $this->name('store'),
            'uses' => $this->uses('store')
        ]);

        $this->router->post($this->prefix('/update/{schedule}'), [
           'as' => $this->name('update'),
            'uses' => $this->uses('update')
        ]);

        $this->router->post($this->prefix('/destroy/{schedule}'), [
            'as' => $this->name('destroy'),
             'uses' => $this->uses('destroy')
        ]);
    }

    /**
     * set default controller used by this class
     *
     */
    public function controller()
    {
        ScheduleController::class;
    }
}