<?php
/**
 * Created by PhpStorm.
 * User: Yustiko
 * Date: 4/25/2017
 * Time: 4:57 PM
 */

namespace App\Http\Routes\Admin;


use App\Core\Routing\SelfBindingRoute;
use App\Http\Controllers\Admin\SettingController;

class SettingRoute extends SelfBindingRoute

{
    /**
     * set default name used by this class
     *
     * @var string
     */
    protected $name = 'admin.setting';

    /**
     * set default path for this route
     *
     * @var string
     */
    protected $prefix = 'pengaturan';

    /**
     * Register routes handled by this class.
     *
     * @return void
     */
    public function register()
    {
        $this->router->get($this->prefix('/'), [
            'as' => $this->name,
            'uses' => $this->uses('index')
        ]);

        $this->router->get($this->prefix('/semua-pengaturan'), [
            'as' => $this->name('list'),
            'uses' => $this->uses('index')
        ]);

        $this->router->get($this->prefix('/store/{key}'), [
            'as' => $this->name('store'),
            'uses' => $this->uses('store')
        ]);

        $this->router->get($this->prefix('/destroy/{key}'), [
            'as' => $this->name('destroy'),
            'uses' => $this->uses('destroy')
        ]);
    }

    /**
     * set default controller used by this class
     *
     * @return string
     */
    public function controller()
    {
        return SettingController::class;
    }
}