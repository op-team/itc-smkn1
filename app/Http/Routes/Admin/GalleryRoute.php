<?php
/**
 * @author yustiko
 * User: <yustiko404@gmail.com>
 * Date: 4/19/2017
 * Time: 1:21 PM
 */

namespace App\Http\Routes\Admin;


use App\Core\Routing\SelfBindingRoute;
use App\Http\Controllers\Admin\GalleryController;

class GalleryRoute extends SelfBindingRoute
{
    /**
     * seat default name property used by this class
     *
     * @var string
     */
    protected $name = 'admin.galleries';

    /**
     * set default prefix property used by this class
     *
     * @var string
     */
    protected $prefix = 'galeri';

    /**
     * Register routes handled by this class.
     *
     * @return void
     */
    public function register()
    {
        $this->router->get($this->prefix('/'), [
            'as' => $this->name,
            'uses' => $this->uses('index')
        ]);

        $this->router->get($this->prefix('/foto/{slug_gallery}'), [
            'as' => $this->name('photos'),
            'uses' => $this->uses('photos')
        ]);

        $this->router->post($this->prefix('/foto/{slug_gallery}/tambah'), [
            'as' => $this->name('photos.add'),
            'uses' => $this->uses('addPhotos')
        ]);

        $this->router->post($this->prefix('/foto/{slug_gallery}/hapus/{media}'), [
            'as' => $this->name('photos.remove'),
            'uses' => $this->uses('removePhoto')
        ]);

        $this->router->get($this->prefix('/buat'), [
           'as' => $this->name('create'),
            'uses' => $this->uses('create')
        ]);

        $this->router->get($this->prefix('/ubah/{slug_gallery}'), [
           'as' => $this->name('edit'),
            'uses' => $this->uses('edit')
        ]);

        $this->router->post($this->prefix('/store/{type?}'), [
            'as' => $this->name('store'),
             'uses' => $this->uses('store')
        ]);

        $this->router->post($this->prefix('/update/{slug_gallery}'), [
            'as' => $this->name('update'),
             'uses' => $this->uses('update')
        ]);

        $this->router->post($this->prefix('/destroy/{slug_gallery}'), [
            'as' => $this->name('destroy'),
             'uses' => $this->uses('destroy')
        ]);
    }

    /**
     * set default controller used by this class
     *
     * @return string
     */
    public function controller()
    {
        return GalleryController::class;
    }
}