<?php
/**
 * Created by PhpStorm.
 * User: rendy
 * Date: 03/06/17
 * Time: 15:40
 */

namespace App\Http\Routes\Admin;


use App\Core\Routing\SelfBindingRoute;
use App\Http\Controllers\Admin\MemberController;

class MemberRoute extends SelfBindingRoute
{
    protected $name = "admin.member";

    protected $prefix = "anggota";

    /**
     * Register routes handled by this class.
     *
     * @return void
     */
    public function register()
    {
       $this->router->get($this->prefix('/'), [
           'as' => $this->name,
           'uses' => $this->uses('index')
       ]);

       $this->router->get($this->prefix('/buat'), [
           'as' => $this->name('create.form'),
           'uses' => $this->uses('showCreateForm')
       ]);

       $this->router->post($this->prefix('/buat'), [
           'as' => $this->name('create'),
           'uses' => $this->uses('store')
       ]);

       $this->router->get($this->prefix('/{username}'), [
           'as' => $this->name('detail'),
           'uses' => $this->uses('show')
       ]);

        $this->router->get($this->prefix('/{username}/ubah'), [
            'as' => $this->name('edit.form'),
            'uses' => $this->uses('showEditForm')
        ]);

        $this->router->put($this->prefix('/{username}'), [
            'as' => $this->name('update'),
            'uses' => $this->uses('update')
        ]);

        $this->router->delete($this->prefix('/{username}'), [
            'as' => $this->name('destroy'),
            'uses' => $this->uses('destroy')
        ]);
    }

    public function controller ()
    {
        return MemberController::class;
    }

}