<?php
namespace App\Http\Routes\Admin;
/**
 * Created by PhpStorm.
 * User: Yustiko <yustiko404@gmail.com>
 * Date: 4/12/2017
 * Time: 11:22 AM
 */

use App\Core\Routing\SelfBindingRoute;
use App\Http\Controllers\Admin\AchievementController;

class AchievementRoute extends SelfBindingRoute
{
    /**
     * set default name for use this class
     *
     * @var string
     */
    protected $name = 'admin.achievements';

    /**
     * set default prefix for this route
     *
     * @var string
     */
    protected $prefix = 'prestasi';

    /**
     * Register routes handled by this class.
     *
     * @return void
     */
    public function register()
    {
        $this->router->get($this->prefix('/'), [
            'as' => $this->name,
            'uses' => $this->uses('index')
        ]);

        $this->router->get($this->prefix('/buat'), [
            'as' => $this->name('create'),
            'uses' => $this->uses('create')
        ]);

        $this->router->get($this->prefix('/ubah/{slug_achievement}'), [
            'as' => $this->name('edit'),
            'uses' => $this->uses('edit')
        ]);

        $this->router->post($this->prefix('/store/{type?}'), [
            'as' => $this->name('store'),
            'uses' => $this->uses('store')
        ]);

        $this->router->post($this->prefix('/update/{slug_achievement}'), [
            'as' => $this->name('update'),
            'uses' => $this->uses('update')
        ]);

        $this->router->post($this->prefix('/destroy/{slug_achievement}'), [
            'as' => $this->name('destroy'),
            'uses' => $this->uses('destroy')
        ]);
    }

    /**
     * Set default Controller used by this class
     *
     */
    public function controller()
    {
        return AchievementController::class;
    }
}