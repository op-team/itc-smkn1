<?php
/**
 * @author Rendy Ananta <rendy.ananta66@gmail.com>
 * at 09/07/17, 10:58
 */

namespace App\Http\Routes\Admin;


use App\Core\Routing\SelfBindingRoute;
use App\Http\Controllers\Admin\ContactsController;

class ContactRoute extends SelfBindingRoute
{
    /**
     * set default name used by this class
     *
     * @var string
     */
    protected $name = 'admin.contacts';

    /**
     * set default path for this route
     *
     * @var string
     */
    protected $prefix = 'kontak';

    /**
     * Register routes handled by this class.
     *
     * @return void
     */
    public function register()
    {
        $this->router->get($this->prefix('/'), [
            'as' => $this->name,
            'uses' => $this->uses('index')
        ]);

        $this->router->get($this->prefix('/buat'), [
            'as' => $this->name('create'),
            'uses' => $this->uses('create')
        ]);

        $this->router->get($this->prefix('/ubah/{key}'), [
            'as' => $this->name('edit'),
            'uses' => $this->uses('edit')

        ]);

        $this->router->post($this->prefix('/store'), [
            'as' => $this->name('store'),
            'uses' => $this->uses('store')
        ]);

        $this->router->post($this->prefix('/update/{key}'), [
            'as' => $this->name('update'),
            'uses' => $this->uses('update')
        ]);

        $this->router->delete($this->prefix('/destroy/{key}'), [
            'as' => $this->name('destroy'),
            'uses' => $this->uses('destroy')
        ]);
    }

    /**
     * set default controller used by this class
     *
     * @return string
     */
    public function controller()
    {
        return ContactsController::class;
    }

}