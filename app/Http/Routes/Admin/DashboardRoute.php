<?php

namespace App\Http\Routes\Admin;

use App\Core\Routing\SelfBindingRoute;
use App\Http\Controllers\Admin\DashboardController;
/**
 * @author yustiko <yustiko404@gmail.com>
 * at 03/04/17 , 22:41
 */
class DashboardRoute extends SelfBindingRoute
{
    /*
     * Make default name used by this route
     */
    protected $name = 'admin.dashboard';

    /*
     * Make default prefix used by this route
     */
    protected $prefix = 'beranda';
    /**
     * Register routes handled by this class.
     *
     * @return void
     */
    public function register()
    {
        $this->router->get($this->prefix('/'), [
           'as' => $this->name,
            'uses' => $this->uses('index')
        ]);

        $this->router->get($this->prefix('/statistik'), [
           'as' => $this->name('list'),
            'uses' => $this->uses('statistic'),
        ]);

        $this->router->get('/', [
           'as' => 'admin',
            'uses' => $this->uses('defaultPage'),
        ]);
    }

    /**
     * set default controller used by this route
     *
     * @return string
     */
    public function controller()
    {
        return DashboardController::class;
    }
}