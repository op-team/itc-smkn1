<?php

namespace App\Http\Routes\Admin;

use App\Core\Routing\SelfBindingRoute;
use App\Http\Controllers\Admin\AttendController;


/**
 * @author <yustiko404@gmail.com> 4/28/2017 2:00 PM
 */
class AttendRoute extends SelfBindingRoute
{
    /**
     * set default name used by this class
     *
     * @var string
     */
    protected $name = 'admin.attend';

    /**
     * set default path used by this class
     *
     * @var string
     */
    protected $prefix = 'hadir';
    /**
     * Register routes handled by this class.
     *
     * @return void
     */
    public function register()
    {
        $this->router->get($this->prefix('/'), [
            'as' => $this->name,
            'uses' => $this->uses('index')
        ]);

        $this->router->get($this->prefix('/daftar-hadir'), [
            'as' => $this->name('list'),
            'uses' => $this->uses('list')
        ]);

        $this->router->get($this->prefix('/buat/{schedule}'), [
            'as' => $this->name('create'),
            'uses' => $this->uses('create')
        ]);

        $this->router->get($this->prefix('/ubah/{schedule}'), [
            'as' => $this->name('edit'),
            'uses' => $this->uses('edit')
        ]);

        $this->router->post($this->prefix('/store/{schedule}'), [
            'as' => $this->name('store'),
            'uses' => $this->uses('store')
        ]);

        $this->router->post($this->prefix('/update/{schedule}'), [
            'as' => $this->name('update'),
            'uses' => $this->uses('update')
        ]);
    }

    /**
     * set default controller used by this class
     *
     * @return string
     */
    public function controller()
    {
        return AttendController::class;
    }
}