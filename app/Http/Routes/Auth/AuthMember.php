<?php

namespace App\Http\Routes\Auth;

use App\Core\Routing\SelfBindingRoute;
use App\Http\Controllers\Auth\MemberController;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 21/03/17 , 11:06
 */
class AuthMember extends SelfBindingRoute
{

    protected $prefix = 'anggota';

    protected $name = 'auth';

    /**
     * Register routes handled by this class.
     *
     * @return void
     */
    public function register()
    {

        $this->router->get($this->prefix('login'), [
            'uses' => $this->uses('showLoginForm'),
            'as' => $this->name('member.login.page'),
        ]);

        $this->router->post($this->prefix('login'), [
            'uses' => $this->uses('login'),
            'as' => $this->name('member.login'),
        ]);

        $this->router->post($this->prefix('logout'), [
            'uses' => $this->uses('logout'),
            'as' => $this->name('member.logout'),
        ]);

        $this->router->get('/lupa-kata-sandi', [
            'uses' => $this->uses('showResetForm'),
            'as' => $this->name('forgot_password')
        ]);
        $this->router->get('/atur-kata-sandi', [
            'uses' => $this->uses('resetPassword'),
            'as' => $this->name('reset_password')
        ]);
    }

    public function controller()
    {
        return MemberController::class;
    }
}