<?php

namespace App\Http\Routes\Auth;

use App\Core\Routing\SelfBindingRoute;
use App\Http\Controllers\Auth\AdminController;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 21/03/17 , 11:06
 */
class AuthAdmin extends SelfBindingRoute
{

    protected $prefix = 'pengurus';

    protected $name = 'auth';

    /**
     * Register routes handled by this class.
     *
     * @return void
     */
    public function register()
    {
        $this->router->get($this->prefix('login.html'), [
            'uses' => $this->uses('showLoginForm'),
            'as' => $this->name('admin.login.page'),
        ])->middleware('alert');

        $this->router->post($this->prefix('login'), [
            'uses' => $this->uses('login'),
            'as' => $this->name('admin.login'),
        ]);

        $this->router->post($this->prefix('logout'), [
            'uses' => $this->uses('logout'),
            'as' => $this->name('admin.logout'),
        ]);
    }

    public function controller()
    {
        return AdminController::class;
    }
}