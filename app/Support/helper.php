<?php

use App\Models\Extra\Setting;

function set_active ($name, $activeClass = 'is-active')
{
    $index = strpos(\Illuminate\Support\Facades\Route::currentRouteName(), $name);
    return $index > -1 ? $activeClass : '';
}

function set_error ($field)
{
    $errors = session('errors');

    if (isset($errors) && $errors instanceof \Illuminate\Support\ViewErrorBag)
        return $errors->has($field) ? 'is-danger' : '';

    return '';
}

function get_error ($input)
{
    $errors = session('errors');

    if (isset($errors) && $errors instanceof \Illuminate\Support\ViewErrorBag)
        return  '<p class="help is-danger">' . $errors->first($input) . '</span>';

    return '';
}

function get_setting ($key, $dummy = null)
{
    $setting = Setting::where('key', $key)->firstOrFail();
    if ($setting)
        return $setting->text;

    return $dummy;
}
