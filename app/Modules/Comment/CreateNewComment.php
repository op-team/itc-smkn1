<?php

namespace App\Modules\Comment;

use App\Core\Contracts\Database\ContentTableInterface;
use App\Core\Patch\JobsPatcher;
use App\Models\Additional\Comment;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 27/04/17 , 23:17
 */
class CreateNewComment extends JobsPatcher
{
    protected $validateRules = [
        'body' => 'required'
    ];

    public $model;

    public function __construct(array $inputs = [], ContentTableInterface $model)
    {
        parent::__construct($inputs);

        $this->model = $model;
    }

    /**
     * working space. play while handle is running
     *
     * @return bool|\Illuminate\Database\Eloquent\Model
     */
    public function run()
    {
        return $this->model->comments()->save(new Comment([
            'body' => $this->request->input('body'),
            'user_id' => $this->request->user()->id
        ]));
    }
}