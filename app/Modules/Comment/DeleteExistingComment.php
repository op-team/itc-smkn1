<?php

namespace App\Modules\Comment;

use App\Core\Patch\JobsPatcher;
use App\Models\Additional\Comment;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 27/04/17 , 23:18
 */
class DeleteExistingComment extends JobsPatcher
{
    public $model;

    public function __construct(array $inputs = [], Comment $model)
    {
        parent::__construct($inputs);

        $this->model = $model;
    }

    /**
     * working space. play while handle is running
     *
     * @return bool
     */
    public function run()
    {
        $this->authorize('delete', $this->model);

        return $this->model->delete();
    }
}