<?php

namespace App\Modules;

use App\Core\Patch\JobsPatcher;
use App\Models\Contents\Schedule;


/**
 * @author <yustiko404@gmail.com> 4/28/2017 4:23 PM
 */
class CreateOrUpdateAttend extends JobsPatcher
{
    protected $schedule;

    protected $validateRules = [
      'attended' => 'array'
    ];

    public function __construct(Schedule $schedule, array $inputs = [])
    {
        parent::__construct($inputs);

        $this->schedule = $schedule;
    }

    /**
     * working space. play while handle is running
     *
     * @return bool
     */
    public function run()
    {
        $user = $this->request->input('attended');

        $this->schedule->attend()->sync($user);

        return $this->schedule->save();
    }
}