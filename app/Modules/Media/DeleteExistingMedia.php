<?php

namespace App\Modules\Media;

use App\Core\Patch\JobsPatcher;
use App\Models\Contents\Media;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 20/04/17 , 10:21
 */
class DeleteExistingMedia extends JobsPatcher
{

    protected $media;

    public function __construct(array $inputs = [], Media $media)
    {
        parent::__construct($inputs);

        $this->media = $media;
    }

    /**
     * working space. play while handle is running
     *
     * @return bool
     */
    public function run()
    {
        return $this->media->delete();
    }
}