<?php

namespace App\Modules\Media;

use App\Core\Patch\JobsPatcher;
use App\Models\Contents\Media;
use App\Models\Contents\Post;
use Carbon\Carbon;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 20/04/17 , 10:21
 */
class CreateNewMedia extends JobsPatcher
{

    protected $validateRules = [
        'images.*' => 'image|mimes:jpeg,png,jpg,gif,svg'
    ];

    protected $hash;
    protected $post;

    public function __construct(array $inputs = [], Post $post)
    {
        parent::__construct($inputs);

        $this->hash = hash('crc32b', uniqid());
        $this->post = $post;
    }

    /**
     * working space. play while handle is running
     *
     * @return array|\Traversable
     */
    public function run()
    {
        return $this->post->media()->saveMany($this->uploadFile());
    }

    /**
     * Real perform upload file
     *
     * @return array
     */
    protected function uploadFile()
    {
        $uploaded = [];
        foreach ($this->request->images as $image) {
            $realImage = hash('crc32b', uniqid()).$image->getClientOriginalExtension();

            $image->storeAs($this->getUploadDirectory(), $realImage, 'public');
            array_push($uploaded, new Media([
                'user_id' => auth()->user()->id,
                'slug' => $this->getSlugByNameAndExt($image),
                'src' => $this->getUploadDirectory().'/'.$realImage,
            ]));
        }
        return $uploaded;
    }

    /**
     * generate wonderful slug
     *
     * @param $image
     * @return string
     */
    protected function getSlugByNameAndExt($image)
    {
        $str = $image->getClientOriginalName().Carbon::now().'-'
                .$image->getClientOriginalExtension();
        return str_slug($str);
    }

    /**
     * Get upload directory
     *
     * @return string
     */
    protected function getUploadDirectory()
    {
        return '/'.substr($this->hash, 0, 2).'/'.substr($this->hash, -2);
    }
}