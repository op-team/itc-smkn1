<?php

namespace app\Modules\Setting;

use App\Core\Patch\JobsPatcher;
use App\Models\Extra\Setting;

/**
 * @author yustiko <yustiko404@gmail.com>
 * at 05/04/17 , 14:34
 */
class DeleteExistingSetting extends JobsPatcher
{
    /**
     * set default property for model
     *
     * @var Setting $setting
     */
    protected $setting;

    /**
     * DeleteExistingPost constructor.
     *
     * @param Setting $setting
     * @param array $inputs
     */
    public function __construct(Setting $setting, array $inputs = [])
    {
        parent::__construct($inputs);

        $this->setting = $setting;
    }

    /**
     * working space. play while handle is running
     *
     * @return bool
     */
    public function run()
    {
        return $this->setting->delete();
    }
}