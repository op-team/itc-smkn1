<?php

namespace App\Modules\Setting\Contact;

use App\Core\Patch\JobsPatcher;
use App\Models\Extra\Setting;

/**
 * @author Rendy Ananta <rendy.ananta66@gmail.com>
 * at 21/06/17, 9:16
 */


class UpdateExistingContact extends JobsPatcher
{
    /**
     * set default validation rules used by this class
     *
     * @var array
     */
    protected $validateRules = [
        'name' => 'required',
        'value' => 'required'
    ];

    /**
     * set default property for using model
     *
     * @var Setting $setting
     */
    protected $setting;

    /**
     * UpdateExistingContact constructor.
     * @param array $inputs
     * @param Setting $setting
     */
    public function __construct(Setting $setting, array $inputs = [])
    {
        parent::__construct($inputs);
        $this->setting = $setting;
    }

    /**
     * working space. play while handle is running
     *
     * @return bool
     */
    public function run()
    {
        return $this->setting->update([
            'key' => 'contacts-' . $this->request->input('name'),
            'value' => $this->request->input('value')
        ]);
    }
}