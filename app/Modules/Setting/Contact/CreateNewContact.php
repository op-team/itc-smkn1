<?php

namespace App\Modules\Setting\Contact;

use App\Core\Patch\JobsPatcher;
use App\Models\Extra\Setting;

/**
 * @author Rendy Ananta <rendy.ananta66@gmail.com>
 * at 21/06/17, 9:16
 */


class CreateNewContact extends JobsPatcher
{
    /**
     * set default validation rules used by this class
     *
     * @var array
     */
    protected $validateRules = [
        'name' => 'required',
        'value' => 'required'
    ];

    /**
     * set default property for using model
     *
     * @var Setting $setting
     */
    protected $setting;

    public function run()
    {
        $this->setting = new Setting();

        $this->setting->fill([
            'key' => 'contacts-' . $this->request->input('name'),
            'value' => $this->request->input('value')
        ]);

        return $this->setting->save();
    }
}