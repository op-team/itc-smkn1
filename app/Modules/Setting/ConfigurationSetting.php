<?php

namespace App\Modules\Setting;

use App\Core\Patch\JobsPatcher;
use App\Models\Extra\Setting;

/**
 * @author yustiko <yustiko404@gmail.com>
 * at 05/04/17 , 10:32
 * @var Setting $setting
 */
class ConfigurationSetting extends JobsPatcher
{

    /**
     * set default validate rules used by this class
     *
     * @var array
     */
    protected $validateRules = [
        'key' => 'required|unique:setting',
        'text' => 'required'
    ];

    /**
     * set default property for using model
     *
     * @var Setting $setting
     */
    protected $setting;

    /**
     * working space. play while handle is running
     *
     * @return bool
     */
    public function run()
    {
        $this->setting = new Setting();
        $items = $this->setting->fill($this->request->only(array_keys($this->validateRules)));

        if ($this->setting->key == null)
            return $this->setting->save($items);

        return $this->setting->update($items);
    }

}