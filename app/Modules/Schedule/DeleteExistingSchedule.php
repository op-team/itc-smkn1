<?php
/**
 * @author
 * User: Yustiko <yustiko404@gmail.com>
 * Date: 4/13/2017
 * Time: 12:40 PM
 */

namespace App\Modules;


use App\Core\Patch\JobsPatcher;
use App\Models\Contents\Schedule;

class DeleteExistingSchedule extends JobsPatcher
{
    /**
     * set default property used by this class
     *
     * @var Schedule|null
     */
    protected $schedule = null;

    /**
     * DeleteExistingSchedule constructor.
     *
     * @param Schedule $schedule
     * @param array $input
     */
    public function __construct(Schedule $schedule, $input = array())
    {
        parent::__construct();

        $this->schedule = $schedule;
    }

    /**
     * working space. play while handle is running
     *
     * @return bool
     */
    public function run()
    {
        return $this->schedule->delete();
    }
}