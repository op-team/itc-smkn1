<?php
/**
 * @author
 * User: Yustiko <yustiko404@gmail.com>
 * Date: 4/13/2017
 * Time: 12:28 PM
 */

namespace App\Modules;


use App\Core\Patch\JobsPatcher;
use App\Models\Contents\Schedule;

class UpdateExistingSchedule extends JobsPatcher
{
    /**
     * set default property schedule used by this class
     *
     * @var Schedule|null
     */
    protected $schedule = null;

    /**
     * set default validate rules used by this class
     *
     * @var array
     */
    protected $validateRules = [
      'title' => 'required',
      'body' => 'required',
      'start_date' => 'required',
      'end_date' => 'required',
    ];

    /**
     * UpdateExistingSchedule constructor.
     *
     * @param Schedule $schedule
     * @param array $input
     */
    public function __construct(Schedule $schedule, array $input = [])
    {
        parent::__construct($input);

        $this->schedule = $schedule;
    }

    /**
     * working space. play while handle is running
     *
     * @return bool
     */
    public function run()
    {
        $schedule = $this->request->only(array_keys($this->validateRules));

        return $this->schedule->update($schedule);
    }
}