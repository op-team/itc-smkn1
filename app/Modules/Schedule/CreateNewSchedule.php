<?php
/**
 * @author
 * User: Yustiko <yustiko404@gmail.com>
 * Date: 4/13/2017
 * Time: 12:11 PM
 */

namespace App\Modules;


use App\Core\Patch\JobsPatcher;
use App\Models\Contents\Schedule;

class CreateNewSchedule extends JobsPatcher
{
    /**
     * set default property schedule used by this class
     *
     * @var null
     */
    protected $schedule = null;

    /**
     * set default validate rules used by this class
     *
     * @var array
     */
    protected $validateRules = [
        'title' => 'required',
        'body' => 'required',
        'start_date' => 'required',
        'end_date' => 'required'
    ];

    /**
     * working space. play while handle is running
     *
     * @return bool
     */
    public function run()
    {
        $this->schedule = new Schedule();

        $this->schedule->fill($this->request->only(array_keys($this->validateRules)));

        return $this->schedule->save();
    }
}