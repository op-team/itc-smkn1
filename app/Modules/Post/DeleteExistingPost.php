<?php

namespace App\Modules\Post;

use App\Core\Patch\JobsPatcher;
use App\Models\Contents\Post;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 05/04/17 , 14:34
 */
class DeleteExistingPost extends JobsPatcher
{
    /**
     * set default property for model
     *
     * @var Post|null
     */
    protected $post = null;

    /**
     * DeleteExistingPost constructor.
     *
     * @param Post $post
     * @param array $inputs
     */
    public function __construct(Post $post, array $inputs = [])
    {
        parent::__construct($inputs);

        $this->post = $post;
    }

    /**
     * working space. play while handle is running
     *
     * @return bool
     */
    public function run()
    {
        $this->authorize('delete', $this->post);

        return $this->post->delete();
    }
}