<?php

namespace App\Modules\Post;

use App\Core\Patch\JobsPatcher;
use App\Models\Contents\Post;
use App\Modules\Media\CreateNewMedia;
use App\Modules\Tag\AttachTag;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 05/04/17 , 10:32
 * @var Post $post
 */
class CreateNewPost extends JobsPatcher
{

    /**
     * set default validate rules used by this class
     *
     * @var array
     */
    protected $validateRules = [
        'type' => 'required',
        'title' => 'required|unique:posts',
        'body' => 'required'
    ];

    /**
     * set default property for using model
     *
     * @var Post $post
     */
    protected $post;

    /**
     * working space. play while handle is running
     *
     * @return bool
     */
    public function run()
    {
        $this->post = new Post();

        $this->post->author()->associate(auth()->user());
        $this->post->fill($this->request->only(array_keys($this->validateRules)));

        return $this->post->save();
    }

    /**
     * Set method callback after running this class
     *
     */
    public function callback()
    {
        if ($this->request->has('tags'))
            dispatch(new AttachTag([], $this->post));

        if ($this->request->hasFile('images'))
            dispatch(new CreateNewMedia([], $this->post));

    }

}