<?php

namespace App\Modules\Post;

use App\Core\Patch\JobsPatcher;
use App\Models\Contents\Post;
use App\Modules\Media\CreateNewMedia;
use App\Modules\Tag\AttachTag;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 05/04/17 , 14:26
 */
class UpdateExistingPost extends JobsPatcher
{
    /**
     * set default property variable for using model
     *
     * @var Post|null
     */
    protected $post = null;

    /**
     * set default validate rules used by this class
     *
     * @var array
     */
    protected $validateRules = [
        'title' => 'required',
        'body' => 'required'
    ];

    /**
     * UpdateExistingPost constructor.
     *
     * @param Post $post
     * @param array $inputs
     */
    public function __construct(Post $post, array $inputs = [])
    {
        parent::__construct($inputs);

        $this->post = $post;
    }

    /**
     * working space. play while handle is running
     *
     * @return bool
     */
    public function run()
    {
        $this->authorize('update', $this->post);

        $items = $this->request->only(array_keys($this->validateRules));
        return $this->post->update($items);
    }

    /**
     * set method callback after running this class
     *
     */
    public function callback()
    {
        if ($this->request->has('tags'))
            dispatch(new AttachTag([], $this->post));

        if ($this->request->has('images'))
            dispatch(new CreateNewMedia([], $this->post));
    }
}