<?php

namespace App\Modules\PostEpisode;

use App\Core\Patch\JobsPatcher;
use App\Models\Contents\Post;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 06/04/17 , 10:46
 */
class UpdateExistingEpisode extends JobsPatcher
{

    protected $post = null;

    protected $validateRules = [
        'embed_link' => 'required|url',
        'title' => 'required',
        'body' => 'required'
    ];

    public function __construct(Post $post, array $inputs = [])
    {
        parent::__construct($inputs);

        $this->post = $post;
    }

    /**
     * working space. play while handle is running
     *
     * @return bool
     */
    public function run()
    {
        $this->authorize('update', $this->post);

        $items = array_keys($this->validateRules);
        array_push($items, 'order');

        return $this->post->update($this->request->only($items));
    }
}