<?php

namespace App\Modules\PostEpisode;

use App\Core\Patch\JobsPatcher;
use App\Models\Contents\Post;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 06/04/17 , 10:45
 */
class CreateNewEpisode extends JobsPatcher
{

    protected $validateRules = [
        'embed_link' => 'required|url',
        'title' => 'required',
        'body' => 'required'
    ];

    protected $postParent;

    public function __construct(Post $postParent, array $inputs = [])
    {
        parent::__construct($inputs);
        $this->postParent = $postParent;
    }

    /**
     * working space. play while handle is running
     *
     * @return bool
     */
    public function run()
    {
        $value = $this->request->only(array_keys($this->validateRules));

        $episode = new Post($value);

        return $this->postParent->episode()->save($episode);
    }
}