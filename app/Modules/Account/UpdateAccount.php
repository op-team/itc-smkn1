<?php

namespace app\Modules\Account;

use App\Core\Patch\JobsPatcher;
use App\Models\Accounts\User;
use Illuminate\Auth\AuthenticationException;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 07/04/17 , 14:58
 */
class UpdateAccount extends JobsPatcher
{

    protected $validateRules = [
        'username' => 'required|unique:users',
        'email' => 'required|email|unique:users',
        'name' => 'required',
        'phone' => 'required',
        'major_id' => 'required|number',
        'grade' => 'required',
        'class_number' => 'required|number',
        'address' => 'required'
    ];

    /**
     * working space. play while handle is running
     * @return bool
     * @throws AuthenticationException
     */
    public function run()
    {
        /** @var User $user */
        $user = auth()->user();

        if (!$user)
            throw new AuthenticationException();

        return $user->update(
            $this->request->only(array_keys($this->validateRules))
        );
    }
}