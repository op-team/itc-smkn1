<?php

namespace app\Modules\Account;

use App\Core\Patch\JobsPatcher;
use App\Models\Accounts\User;
use Illuminate\Auth\AuthenticationException;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 07/04/17 , 14:59
 */
class UpdatePasswordAccount extends JobsPatcher
{

    protected $validateRules = [
        'password' => 'required|confirmed|min:8'
    ];

    /**
     * working space. play while handle is running
     * @return bool
     * @throws AuthenticationException
     */
    public function run()
    {
        /** @var User $user */
        $user = auth()->user();

        if (!$user)
            throw new AuthenticationException();

        $user->password = $this->request->input('password');

        return $user->save();
    }
}