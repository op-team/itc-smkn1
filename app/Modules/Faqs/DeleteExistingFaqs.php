<?php

namespace App\Modules;

use App\Core\Patch\JobsPatcher;
use App\Models\Extra\Faq;

/**
 * @author <yustiko404@gmail.com> 4/28/2017 1:29 PM
 */
class DeleteExistingFaqs extends JobsPatcher
{
    /**
     * set default property used by this class
     *
     * @var Faq
     */
    protected $faqs;

    /**
     * DeleteExistingFaqs constructor.
     *
     * @param Faq $faqs
     * @param array $input
     */
    public function __construct(Faq $faqs, $input = [])
    {
        parent::__construct($input);

        $this->faqs = $faqs;
    }

    /**
     * working space. play while handle is running
     *
     * @return bool
     */
    public function run()
    {
        return $this->faqs->delete();
    }
}