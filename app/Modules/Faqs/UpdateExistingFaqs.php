<?php

namespace App\Modules;

use App\Core\Patch\JobsPatcher;
use App\Models\Extra\Faq;

/**
 * @author <yustiko404@gmail.com> 4/28/2017 1:24 PM
 */
class UpdateExistingFaqs extends JobsPatcher
{
    /**
     * set default property used by this class
     *
     * @var Faq
     */
    protected $faqs;

    /**
     * set default validate rules used by this class
     *
     * @var array
     */
    protected $validateRules = [
        'question' => 'required|unique:faqs',
        'answer' => 'required'
    ];

    /**
     * UpdateExistingFaqs constructor.
     *
     * @param Faq $faqs
     * @param array $input
     */
    public function __construct(Faq $faqs, array $input = [])
    {
        parent::__construct($input);

        $this->faqs = $faqs;
    }

    /**
     * working space. play while handle is running
     *
     * @return bool
     */
    public function run()
    {
        $faqs = $this->request->only(array_keys($this->validateRules));

        return $this->faqs->update($faqs);
    }
}