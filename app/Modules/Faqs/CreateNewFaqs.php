<?php

namespace App\Modules;

use App\Core\Patch\JobsPatcher;
use App\Models\Extra\Faq;

/**
 * @author <yustiko404@gmail.com> 4/28/2017 1:20 PM
 */
class CreateNewFaqs extends JobsPatcher
{
    /**
     * set default property used by this class
     *
     * @var
     */
    protected $faqs;


    /**
     * set default validate rules used by this class
     *
     * @var array
     */
    protected $validateRules = [
      'question' => 'required|unique:faqs',
      'answer' => 'required'
    ];

    /**
     * working space. play while handle is running
     *
     * @return bool
     */
    public function run()
    {
        $this->faqs = new Faq();

        $this->faqs->fill($this->request->only(array_keys($this->validateRules)));

        return $this->faqs->save();
    }
}