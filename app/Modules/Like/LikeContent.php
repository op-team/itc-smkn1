<?php

namespace App\Modules\Like;

use App\Core\Contracts\Database\ContentTableInterface;
use App\Core\Patch\JobsPatcher;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 28/04/17 , 12:42
 */
class LikeContent extends JobsPatcher
{
    public $model;

    public function __construct(array $inputs = [], ContentTableInterface $model)
    {
        parent::__construct($inputs);

        $this->model = $model;
    }

    /**
     * working space. play while handle is running
     *
     * @return bool
     */
    public function run()
    {
        $this->model->likedBy()->attach($this->request->user()->id);

        return true;
    }
}