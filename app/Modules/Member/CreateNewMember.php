<?php

namespace App\Modules\Member;

use App\Core\Patch\JobsPatcher;
use App\Models\Accounts\User;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 04/06/17 , 22:58
 */
class CreateNewMember extends JobsPatcher
{

    public $user;

    protected $validateRules = [
        'name' => 'required',
        'email' => 'required|unique:users|email',
        'username' => 'required|unique:users'
    ];

    public function __construct(array $inputs = [])
    {
        parent::__construct($inputs);

        $this->user = new User();
    }

    /**
     * working space. play while handle is running
     *
     * @return bool
     */
    public function run()
    {
        $this->user->fill($this->request->all());

        return $this->user->save();
    }
}
