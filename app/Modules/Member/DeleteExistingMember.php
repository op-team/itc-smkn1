<?php

namespace App\Modules\Member;

use App\Core\Patch\JobsPatcher;
use App\Models\Accounts\User;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 04/06/17 , 22:59
 */
class DeleteExistingMember extends JobsPatcher
{

    public $user;

    public function __construct(array $inputs = [], User $user)
    {
        parent::__construct($inputs);

        $this->user = $user;
    }

    /**
     * working space. play while handle is running
     *
     * @return bool
     */
    public function run()
    {
        return $this->user->delete();
    }
}