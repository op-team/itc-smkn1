<?php

namespace App\Modules\Tag;

use App\Core\Contracts\Database\ContentTableInterface;
use App\Core\Patch\JobsPatcher;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 28/04/17 , 13:00
 */
class AttachTag extends JobsPatcher
{
    protected $validateRules = [
        'tags' => 'required|array'
    ];

    public $model;

    public function __construct(array $inputs = [], ContentTableInterface $model)
    {
        parent::__construct($inputs);

        $this->model = $model;
    }

    /**
     * working space. play while handle is running
     *
     * @return array|bool
     */
    public function run()
    {
        return $this->model->tags()->sync($this->request->only('tags'));
    }
}