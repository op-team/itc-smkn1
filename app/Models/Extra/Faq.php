<?php

namespace App\Models\Extra;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class Faq extends Model
{
    use SearchableTrait;

    /**
     * Define table name
     *
     * @var string table
     */
    protected $table = 'faqs';

    /**
     * Define fillable attributes on Faq Entity
     *
     * @var array $fillable
     */
    protected $fillable = [
        'question', 'answer'
    ];

    /**
     * Define searchable column
     *
     * @var array $searchable
     */
    protected $searchable = [
        'columns' => [
            'faqs.question' => 10,
            'faqs.answer' => 9,
        ]
    ];
}
