<?php

namespace App\Models\Extra;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $table = 'settings';

    protected $fillable = ['key', 'value'];

    /**
     * if that are contact, return contact name
     *
     * @return bool|null|string
     */
    public function getNameAttribute ()
    {

        return explode('-', $this->attributes['key'])[1];
    }

    /**
     * Check instance is contact or not
     *
     * @return bool
     */
    public function isContact ()
    {
        return strpos($this->attributes['key'], 'contact') ? true : false;
    }

    /**
     * get contact
     *
     * @param $query
     * @return mixed
     */
    public function scopeContact ($query)
    {
        return $query->where('key', 'like', '%contacts-%');
    }
}
