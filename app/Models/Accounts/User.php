<?php

namespace App\Models\Accounts;

use App\Models\Contents\Media;
use App\Models\Contents\Post;
use App\Models\Contents\Schedule;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Storage;
use Nicolaslopezj\Searchable\SearchableTrait;

class User extends Authenticatable
{
    use Notifiable, SearchableTrait;

    const ADMIN = "admin";
    const MEMBER = "member";

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role', 'username', 'phone',
        'grade', 'class_number', 'address', 'avatar',
    ];

    protected $with = [
        'major'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchable = [
        'columns' => [
            'users.name' => 3,
            'users.email' => 2,
            'users.username' => 2,
            'users.grade' => 1,
            'majors.name' => 1,
            'majors.slug' => 1
        ],
        'joins' => [
            'majors' => ['users.major_id', 'majors.id']
        ]
    ];

    /**
     * Create relation to major
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function major()
    {
        return $this->belongsTo(Major::class, 'major_id', 'id');
    }

    /**
     * Define a posts belong to user
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function posts()
    {
        return $this->hasMany(Post::class, 'user_id', 'id');
    }

    /**
     * Get a liked post by user
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function postsLiked()
    {
        return $this->morphedByMany(Post::class, 'likeable');
    }

    /**
     * Define a media belong to user
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function media()
    {
        return $this->hasMany(Media::class, 'user_id', 'id');
    }

    /**
     * Defining Avatar mutator if no avatar listed
     *
     * @return string
     */
    public function getAvatarAttribute ()
    {
        if ($this->attributes['avatar'] == null) {
            return url('defaults/user-avatar.png');
        }
        return url($this->attributes['avatar']);
    }

    /**
     * Define event attending and absent by user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function present()
    {
        return $this->belongsToMany(Schedule::class, 'attend');
    }

    public function getPresentOnAttribute($value)
    {
        return $this->present()->wherePivot('attended', 1)->get();
    }

    public function getAbsentOnAttribute($value)
    {
        return $this->present()->wherePivot('attended', 0)->withPivot('note')->get();
    }

    /**
     * Mutator for password to auto bcrypt
     *
     * @param $value
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

    /**
     * Scope for member role
     *
     * @param $query
     * @return mixed
     */
    public function scopeMember($query)
    {
        return $query->where('role', static::MEMBER);
    }

    /**
     * Check role is member or not
     *
     * @return boolean
     */
    public function isMember ()
    {
        return $this->attributes['role'] == self::MEMBER ? true : false;
    }

    /**
     * Check role is admin or not
     *
     * @return bool
     */
    public function isAdmin ()
    {
        return $this->attributes['role'] == self::ADMIN ? true : false;
    }

}
