<?php

namespace App\Models\Contents;

use App\Core\Eloquent\Orderable;
use App\Models\Accounts\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    use Orderable;

    protected $table = 'media';

    protected $fillable = [
        'user_id',
        'slug',
        'src'
    ];

    protected static function boot()
    {
        parent::boot();

        self::creating(function ($model){
            if (!$model->user_id && app()->environment('local', 'testing'))
                $model->user_id = User::inRandomOrder()->first()->id;

            if (!$model->slug)
                $model->slug = sha1(Carbon::now());
        });
    }

    /**
     * define a owner of media
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function post()
    {
        return $this->belongsTo(Post::class, 'post_id', 'id');
    }

}
