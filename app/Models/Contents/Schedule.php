<?php

namespace App\Models\Contents;

use App\Core\Contracts\Database\ContentTableInterface;
use App\Core\Eloquent\Orderable;
use App\Models\Accounts\User;
use App\Models\Additional\Comment;
use Illuminate\Database\Eloquent\Model;
use Symfony\Component\Translation\Exception\NotFoundResourceException;

class Schedule extends Model implements ContentTableInterface
{
    use Orderable;

    protected $table = 'schedules';

    protected $casts = [
        'published_at' => 'datetime',
        'start_date' => 'timestamp',
        'end_date' => 'timestamp',
    ];

    /**
     * Define who was attend and not attend in event
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function attend()
    {
        return $this->belongsToMany(User::class, 'attend');
    }

    public function getAttendedAttribute($value)
    {
        return $this->attend()->wherePivot('attended', 1)->get();
    }

    public function getNotAttendAttribute($value)
    {
        return $this->attend()->wherePivot('attended', 0)->withPivot('note')->get();
    }

    /**
     * Get all of the tags for the post.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }

    /**
     * Show who has like a schedule
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function likedBy()
    {
        return $this->morphToMany(User::class, 'likeable');
    }

    /**
     * Get comments for schedule
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    /**
     * cheat practice slug generator
     *
     * @param $value
     */
    public function setTitleAttribute($value)
    {
        $this->attributes['title'] = $value;
        $this->attributes['slug'] = str_slug($value);
    }

    /**
     * Scope for get published post
     *
     * @param $query
     * @return mixed
     */
    public function scopePublished($query)
    {
        return $query->whereNotNull('published_at');
    }

    public function isPublished()
    {
        if (!$this->attributes['published_at'])
            throw new NotFoundResourceException();

        return $this;
    }

}
