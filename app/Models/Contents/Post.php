<?php

namespace App\Models\Contents;

use App\Core\Contracts\Database\ContentTableInterface;
use App\Core\Eloquent\Orderable;
use App\Models\Accounts\User;
use App\Models\Additional\Comment;
use App\Models\Additional\Tag;
use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;
use Prophecy\Exception\Prediction\FailedPredictionException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Post extends Model implements ContentTableInterface
{

    use SearchableTrait, Orderable;

    protected $table = 'posts';
    
    protected $casts = [
        'published_at' => 'datetime'
    ];

    protected $with = [
        'author'
    ];

    protected $fillable = [
        'type', 'title', 'slug', 'body', 'embed_link', 'order', 'views', 'user_id'
    ];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchable = [
        'columns' => [
            'posts.title' => 1,
            'posts.body' => 2,
        ],
    ];

    /**
     * define a post author
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * Defining nested relationship for video
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function episode()
    {
        if ($this->type != 'post-video')
            throw new FailedPredictionException('this post is not video');

        return $this->hasMany($this, 'parent_id', 'id');
    }

    /**
     * define a media for posts
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function media()
    {
        return $this->hasMany(Media::class, 'post_id', 'id');
    }

    /**
     * Get all of the tags for the post.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }

    /**
     * Show who has like a post
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function likedBy()
    {
        return $this->morphToMany(User::class, 'likeable');
    }

    /**
     * Get comments for post
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    /**
     * Scope for type in post
     *
     * @param $query
     * @param $type
     * @return mixed
     */
    public function scopeType($query, $type)
    {
        return $query->where('type', $type);
    }

    /**
     * cheat practice slug generator
     *
     * @param $value
     */
    public function setTitleAttribute($value)
    {
        $this->attributes['title'] = $value;
        $this->attributes['slug'] = str_slug($value);
    }
    
    /**
     * Scope for get published post
     *
     * @param $query
     * @return mixed
     */
    public function scopePublished($query)
    {
        return $query->whereNotNull('published_at');
    }

    public function isPublished()
    {
        if (!$this->attributes['published_at'])
            throw new NotFoundHttpException();

        return $this;
    }
}
