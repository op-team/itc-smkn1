<?php

namespace App\Models\Additional;

use App\Core\Eloquent\HasCompositeKeys;
use App\Models\Accounts\User;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{

    protected $primaryKey = ['user_id', 'commentable_type', 'commentable_id'];

    protected $fillable = [
        'user_id', 'body'
    ];

    /**
     * Get author of the comment
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * Get all of the owning commentable models.
     */
    public function commentable()
    {
        return $this->morphTo();
    }

}
