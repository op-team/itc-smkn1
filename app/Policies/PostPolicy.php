<?php

namespace App\Policies;


use App\Models\Accounts\User;
use App\Models\Contents\Post;
use Illuminate\Auth\Access\HandlesAuthorization;

class PostPolicy
{
    use HandlesAuthorization;

    /**
     * Defining admin can do anything
     *
     * @param User $user
     * @param string $ability
     * @return bool
     */
    public function before(User $user, $ability)
    {
        if ($user->role === User::ADMIN)
            return true;
    }

    /**
     * Determine whether the user can create the post.
     *
     * @param User $user
     * @param Post $post
     * @return bool
     */
    public function create(User $user, Post $post)
    {
        return true;
    }

    /**
     * Determine whether the user can views the post.
     *
     * @param User $user
     * @param Post $post
     * @return bool
     */
    public function view(User $user, Post $post)
    {
        return $user->id === $post->user_id;
    }

    /**
     * Determine whether the user can update the post.
     *
     * @param User $user
     * @param Post $post
     * @return mixed
     */
    public function update(User $user, Post $post)
    {
        return $user->id === $post->user_id;
    }

    /**
     * Determine whether the user can delete the post.
     *
     * @param User $user
     * @param Post $post
     * @return mixed
     */
    public function delete(User $user, Post $post)
    {
        return $user->id === $post->user_id;
    }
}
