<?php

namespace App\Policies;

use App\Models\Accounts\User;
use App\Models\Additional\Comment;
use Illuminate\Auth\Access\HandlesAuthorization;

class CommentPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can create the post.
     *
     * @param User $user
     * @param Comment $comment
     * @return bool
     * @internal param Post $post
     */
    public function create(User $user, Comment $comment)
    {
        return true;
    }

    /**
     * Determine whether the user can views the post.
     *
     * @param User $user
     * @param Comment $comment
     * @return bool
     * @internal param Post $post
     */
    public function view(User $user, Comment $comment)
    {
        return $user->id === $comment->user_id;
    }

    /**
     * Determine whether the user can update the post.
     *
     * @param User $user
     * @param Comment $comment
     * @return mixed
     * @internal param Post $post
     */
    public function update(User $user, Comment $comment)
    {
        return $user->id === $comment->user_id;
    }

    /**
     * Determine whether the user can delete the post.
     *
     * @param User $user
     * @param Comment $comment
     * @return mixed
     * @internal param Post $post
     */
    public function delete(User $user, Comment $comment)
    {
        return $user->id === $comment->user_id;
    }
}
