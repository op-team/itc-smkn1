<?php

namespace App\Core\Contracts\Database;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 27/04/17 , 23:55
 */
Interface ContentTableInterface
{
    /**
     * Get all of the tags for the post.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function tags();

    /**
     * Show who has like a post
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function likedBy();

    /**
     * Get comments for post
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function comments();
}