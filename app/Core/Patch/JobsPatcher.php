<?php
namespace App\Core\Patch;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;

abstract class JobsPatcher
{
    use AuthorizesRequests,ValidatesRequests;

    const SUCCESS = 'success';
    const FAILED = 'failed';

    /** @var \Illuminate\Http\Request $request */
    protected $request = [];

    protected $inputs = [];

    protected $validateRules = [];

    protected $status = 'idle';

    /*
     * construction . give a extra input here as array
     */
    public function __construct(array $inputs = [])
    {
        $this->inputs = $inputs;
        $this->request = app('request');
        $this->request->merge($inputs);

    }

    /*
     * process handler . this method will be
     */
    public function handle()
    {
        $this->validate($this->request, $this->validateRules);
        $process = $this->run();
        $this->status = ($process) ? self::SUCCESS : self::FAILED ;

        $this->performCallback();

        return $this;
    }

    /**
     * running callback if available
     */
    public function performCallback()
    {
        if (method_exists($this, 'callback'))
            $this->callback();
    }

    /**
     * working space. play while handle is running
     *
     * @return bool
     */
    abstract public function run();

    /*
     * check while job is success
     * and execute a callback
     */
    public function success($callback = '')
    {
        if ($this->status === self::SUCCESS) {
            if (!$callback)
                return true;

            return $callback($this->request);
        }
        return false;
    }

    /*
     * check while job is failed
     * and execute a callback
     */
    public function failed($callback = '')
    {
        if ($this->status === self::FAILED) {
            if (!$callback)
                return true;

            return $callback($this->request);
        }
        return false;
    }
}