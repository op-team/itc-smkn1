<?php
/**
 * Created by PhpStorm.
 * User: rendy
 * Date: 7/14/17
 * Time: 8:52 PM
 */

namespace App\Core\Eloquent;


trait Orderable
{

    /**
     * Eloquent scope to order item latest first
     *
     * @param $query
     * @return mixed
     */
    public function scopeLatest ($query)
    {
        return $query->orderBy('created_at', 'desc');
    }

    /**
     * Eloquent scope to order item oldest first
     *
     * @param $query
     * @return mixed
     */
    public function scopeOldest ($query)
    {
        return $query->orderBy('created_at', 'asc');
    }
}