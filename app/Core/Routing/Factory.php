<?php

namespace App\Core\Routing;

use RuntimeException;
use Illuminate\Support\Collection;

/**
 * Route factory.
 *
 * @author      veelasky <veelasky@gmail.com>
 */
class Factory
{
    /**
     * Route groups.
     *
     * @var array
     */
    protected $routes = [];

    /**
     * Store key from make
     *
     * @var string
     */
    protected $key = '' ;

    /**
     * List of all options.
     *
     * @var array
     */
    protected $options = [];

    /**
     * Create new route group.
     *
     * @param $key
     * @param array $options
     * @return $this
     */
    public function make($key, array $options = [])
    {
        if (array_key_exists($key, $this->routes))
            throw new RuntimeException("Route Group with key: `$key` already exist.");

        $this->key = $key;
        $this->options[$key] = $options;

        return $this;
    }

    public function bind(array $items = [])
    {
        $this->routes[$this->key] = new Collection($items);
    }

    /**
     * Get options for specific route group.
     *
     * @param $key
     * @return mixed
     */
    public function getOptions($key)
    {
        return $this->options[$key];
    }

    /**
     * Get Route Group container by its key.
     *
     * @param $key
     *
     * @return mixed
     */
    public function get($key)
    {
        if (array_key_exists($key, $this->routes)) {
            return $this->routes[$key];
        }

        throw new RuntimeException("Route Group with key: `$key` doesn't exists.");
    }

    /**
     * Return all registered route
     *
     * @return array
     */
    public function all()
    {
        return $this->routes;
    }

    /**
     * Bootstrap routing service
     * Performing this in RouteServiceProvider
     *
     * @return void
     */
    public function boot()
    {
        foreach (config('routes.providers') as $route=>$option ){
            $this->make($route, $option)
                 ->bind(config('routes.bind')[$route]);
        }
    }

    /**
     * Define the routes for the application.
     * Performing this in RouteServiceProvider
     *
     * @return void
     */
    public function map()
    {
        foreach (array_keys($this->routes) as $key){
            app('router')->group($this->getOptions($key), function () use ($key) {
                $classes = $this->get($key);
                foreach ($classes as $class) {
                    $class::bind();
                }
            });
        }
    }
}
