<?php

namespace App\Core\Routing;

use Illuminate\Support\ServiceProvider as LaravelServiceProvider;

/**
 * Maclev Route Group Service Provider.
 *
 * @author      veelasky <veelasky@gmail.com>
 */
class RouteServiceProvider extends LaravelServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('itc.routing', function () {
            $factory = new Factory();

            return $factory;
        });

        $this->app->alias('itc.routing', Factory::class);
    }
}
