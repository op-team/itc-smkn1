<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Accounts\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'role' => 'member',
        'username' => $faker->unique()->userName,
        'phone' => $faker->phoneNumber,
        'grade' => rand(10, 12),
        'class_number' => rand(1, 5),
        'address' => $faker->address,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = 'secret',
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Models\Contents\Post::class, function (Faker\Generator $faker){
    $content = '';
    for ($i = 1; $i < 4; $i++) {
        $content .= $faker->paragraph(18) . ' \n ';
    }
    return [
        'user_id' => \App\Models\Accounts\User::inRandomOrder()->firstOrFail()->id,
        'title' => title_case($faker->sentence(8)),
        'body' => nl2br($content),
        'published_at' => \Carbon\Carbon::now()
    ];
});

$factory->define(App\Models\Contents\Schedule::class, function (Faker\Generator $faker){
    $time = Carbon\Carbon::now();
    return [
        'title' => $faker->text(20),
        'body' => $faker->realText(150),
        'start_date' => $time,
        'end_date' => $time->addHour(5),
    ];
});

$factory->define(App\Models\Extra\Faq::class, function (Faker\Generator $faker){
    return [
        'question' => $faker->text('10').' ?',
        'answer' => $faker->text(100)
    ];
});

$factory->define(App\Models\Additional\Comment::class, function(Faker\Generator $faker){
    return [
        'user_id' => \App\Models\Accounts\User::inRandomOrder()->firstOrFail()->id,
        'body' => $faker->realText(80),
    ];
});