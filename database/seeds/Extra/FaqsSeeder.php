<?php
namespace Seeder\Extra;

use App\Models\Extra\Faq;
use Illuminate\Database\Seeder;

class FaqsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Faq::class, 20)->create();
    }
}
