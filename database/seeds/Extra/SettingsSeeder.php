<?php
namespace Seeder\Extra;

use App\Models\Extra\Setting;
use Illuminate\Database\Seeder;

class SettingsSeeder extends Seeder
{

    protected $data = [
        [
            'key' => 'contacts',
            'value' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusantium alias asperiores aut autem consequatur cupiditate dolore ea, et explicabo hic id incidunt iure, modi molestias mollitia necessitatibus nobis officia pariatur perspiciatis praesentium quas reprehenderit saepe tempore totam velit voluptates? Accusamus amet aut, commodi cupiditate distinctio dolor, dolore doloremque esse eum harum hic illo impedit ipsum iste molestias nesciunt nisi nostrum odio optio praesentium quae quia quo repellat repudiandae sequi sit tempora tempore temporibus ut voluptates. A alias debitis dignissimos dolor est eveniet ex fugiat iste iusto maxime non nulla possimus quaerat quam quasi, quo sed soluta sunt velit, voluptatum.'
        ],
        [
            'key' => 'address',
            'value' => 'Jl. Smea No. 4',
        ]
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->data as $datum){
            Setting::create($datum);
        }
    }

    public function seedContact ()
    {

    }
}
