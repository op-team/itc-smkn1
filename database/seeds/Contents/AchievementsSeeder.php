<?php
namespace Seeder\Contents;

use App\Models\Additional\Tag;
use App\Models\Contents\Post;
use App\Models\Accounts\User;
use App\Models\Additional\Comment;
use Faker\Factory;
use Illuminate\Database\Seeder;

class AchievementsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        $users = User::member()->pluck('id')->toArray();

        factory(Post::class, 140)->create([
            'type' => 'achievement',
            'user_id' => User::where('role', User::ADMIN)->firstOrFail()->id
        ])->each(function ($post) use ($faker, $users) {
            $post->tags()->attach(Tag::inRandomOrder()->take(rand(1, 4))->get());
            $post->likedBy()->attach(User::inRandomOrder()->take($faker->randomElement($users))->get());
            $post->save();
        });
    }
}
