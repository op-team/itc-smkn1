<?php
namespace Seeder\Contents;

use App\Models\Accounts\User;
use App\Models\Contents\Schedule;
use Illuminate\Database\Seeder;

class ScheduleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Schedule::class, 80)->create()->each(function ($schedule){
            $schedule->attend()->attach($this->generateAttendingUser());
        });
    }

    protected function generateAttendingUser()
    {
        return User::inRandomOrder()->limit(50)->get();
    }
}
