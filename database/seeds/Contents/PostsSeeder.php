<?php
namespace Seeder\Contents;

use App\Models\Additional\Tag;
use App\Models\Contents\Media;
use App\Models\Contents\Post;
use App\Models\Accounts\User;
use App\Models\Additional\Comment;
use Faker\Factory;
use Illuminate\Database\Seeder;

class PostsSeeder extends Seeder
{
    var $postLength = 15;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        $users = User::member()->pluck('id')->toArray();

        $bar = $this->command->getOutput()->createProgressBar($this->postLength);
        for($i = 0; $i < $this->postLength; $i++) {
            $content = '';
            for ($j = 1; $j < 6; $j++) {
                $content .= $faker->paragraph(18, true) . ' \n ';
                if ($j %2 == 1) {
                    $src[$j]['src'] = "https://unsplash.it/720/360?image={$faker->randomNumber(2)}";
                    $content .= "<br /> <img src='{$src[$j]['src']}' width='100%'/> <br />";
                }
            }
            factory(Post::class)->create([
                'type' => 'post',
                'body' => $content,
            ])->each(function ($post) use ($faker, $users, $src){
                $post->media()->createMany(array_values($src));

                foreach ($faker->randomElements(Tag::inRandomOrder()->pluck('id')->toArray()) as $tag)
                    if (!$post->tags->contains($tag)) $post->tags()->attach($tag);

                foreach ($faker->randomElements(User::inRandomOrder()->pluck('id')->toArray()) as $liker)
                    if (!$post->likedBy->contains($liker)) $post->likedBy()->attach($liker);

                $post->save();
            });
            $bar->advance();
        }
        $bar->finish();
        $this->command->info(' - Post has been created successfully'.PHP_EOL);
    }
}
