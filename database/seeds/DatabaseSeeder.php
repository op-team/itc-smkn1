<?php

use Illuminate\Database\Seeder;
use Seeder\Accounts\MajorsSeeder;
use Seeder\Accounts\UsersSeeder;
use Seeder\Additional\TagsSeeder;
use Seeder\Contents\AchievementsSeeder;
use Seeder\Contents\PostsSeeder;
use Seeder\Contents\ScheduleSeeder;
use Seeder\Extra\FaqsSeeder;
use Seeder\Extra\SettingsSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(MajorsSeeder::class);
        $this->call(UsersSeeder::class);
        $this->call(TagsSeeder::class);
        $this->call(PostsSeeder::class);
        $this->call(AchievementsSeeder::class);
        $this->call(ScheduleSeeder::class);
        $this->call(FaqsSeeder::class);
        $this->call(SettingsSeeder::class);
    }
}
