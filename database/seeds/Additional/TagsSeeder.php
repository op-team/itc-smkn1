<?php

namespace Seeder\Additional;

use App\Models\Additional\Tag;
use Illuminate\Database\Seeder;

class TagsSeeder extends Seeder
{

    protected $tags = [
        'creativity',
        'tutorial',
        'funny',
        'daily'
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->tags as $tag)
            Tag::create(['name' => $tag]);
    }
}
