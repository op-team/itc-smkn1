<?php
namespace Seeder\Accounts;

use App\Models\Accounts\Major;
use Illuminate\Database\Seeder;

class MajorsSeeder extends Seeder
{

    protected $majors = [
        ['name' => 'Administrasi Perkantoran', 'slug' => 'apk'],
        ['name' => 'Akomodasi Perhotelan', 'slug' => 'aph'],
        ['name' => 'Akuntansi', 'slug' => 'ak'],
        ['name' => 'Broadcasting', 'slug' => 'bc'],
        ['name' => 'Desain Komunikasi Visual', 'slug' => 'dkv'],
        ['name' => 'Multimedia', 'slug' => 'mm'],
        ['name' => 'Pemasaran', 'slug' => 'pbr'],
        ['name' => 'Rekayasa Perangkat Lunak', 'slug' => 'rpl'],
        ['name' => 'Teknik Komputer Dan Jaringan', 'slug' => 'tkj'],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->majors as $major){
            Major::create($major);
        }
    }
}
