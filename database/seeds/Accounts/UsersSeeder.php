<?php
namespace Seeder\Accounts;

use App\Models\Accounts\Major;
use App\Models\Accounts\User;
use Faker\Factory;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $majors = Major::all()->pluck('id')->toArray();
        $faker = Factory::create();
        factory(User::class, 1)->create([
            'username' => 'bigboss',
            'role' => User::ADMIN,
        ])->each(function ($user) use ($faker, $majors) {
            $user->major()->associate(Major::find($faker->randomElement($majors)));
            $user->save();
        });

        factory(User::class, 80)->create()->each(function ($user) use ($faker, $majors){
            $user->major()->associate(Major::find($faker->randomElement($majors)));
            $user->save();
        });
    }
}
