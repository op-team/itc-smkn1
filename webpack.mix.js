const { mix } = require('laravel-mix');

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css')
   .sass('resources/assets/sass/admin.scss', 'public/css');

mix.copy('node_modules/flickity/dist/flickity.min.css', 'public/css/flickity.min.css')
   .copy('node_modules/flickity/dist/flickity.pkgd.min.js', 'public/js/flickity.min.js');

mix.copy('node_modules/lightgallery.js/dist/js/lightgallery.min.js', 'public/js/lightgallery.min.js')
   .copy('node_modules/lg-thumbnail.js/dist/lg-thumbnail.min.js', 'public/js/lg-thumbnail.min.js')
   .copy('node_modules/lg-zoom.js/dist/lg-zoom.min.js', 'public/js/lg-zoom.min.js')
   .copy('node_modules/lightgallery.js/dist/css/lightgallery.min.css', 'public/css/lightgallery.min.css')
   .copy('node_modules/lightgallery.js/dist/img/*.*', 'public/img')
   .copy('node_modules/lightgallery.js/dist/fonts/*.*', 'public/fonts/');

mix.copy('node_modules/chart.js/dist/Chart.bundle.min.js', 'public/js/Chart.js');
   	
