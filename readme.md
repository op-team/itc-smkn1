# Web IT Club SMK Negeri 1 Surabaya

> > Development Team :
> > * Rendy Ananta
> > * Muhajirin Ida Ilyas
> > * Ahmad Bagus Aditya Chandra
> > * Yustiko Daru Murti

# Installation

1. Clone this project and run `composer update` or `composer install`
2. Happy coding

# Environment

1. [Laravel 5.4](https://laravel.com/docs)
2. [Laravel Fractal](https://github.com/spatie/laravel-fractal)

# Scheme and feature list
[Link](https://docs.google.com/spreadsheets/d/1J_ArM_X2aGvFiyj0pHLKZCvTDIZudehx70buLS4H9tw/edit#gid=0)

# Copyright
[Greget Team](http://greget.in)
