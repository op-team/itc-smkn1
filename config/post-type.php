<?php
return [

    'public' => [
        'post',
        'post-video'
    ],

    'protected' => [
        'gallery',
        'achievement'
    ],

];