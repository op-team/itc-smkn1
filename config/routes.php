<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Routes class
    |--------------------------------------------------------------------------
    | put your route class here
    | route key must be declare in provider array as key
    |
    */
    'bind' => [
        'web' => [
            // Auth
            App\Http\Routes\Auth\AuthAdmin::class,
            App\Http\Routes\Auth\AuthMember::class,
            // Additional route
            App\Http\Routes\Binder::class,
            App\Http\Routes\Media::class,
            // Guest
            App\Http\Routes\Guest\Home::class,
            App\Http\Routes\Guest\Member::class,
            App\Http\Routes\Guest\Achievement::class,
            App\Http\Routes\Guest\Gallery::class,
            App\Http\Routes\Guest\Post::class,
            App\Http\Routes\Guest\Extra::class,
            App\Http\Routes\Guest\Schedule::class,

        ],
        'api' => [

        ],
        'admin' => [
            App\Http\Routes\Admin\DashboardRoute::class,
            App\Http\Routes\Admin\PostRoute::class,
            App\Http\Routes\Admin\MemberRoute::class,
            App\Http\Routes\Admin\FaqsRoute::class,
            App\Http\Routes\Admin\ContactRoute::class,
            App\Http\Routes\Admin\AchievementRoute::class,
            App\Http\Routes\Admin\GalleryRoute::class,
        ],
        'member' => [
            App\Http\Routes\Member\Dashboard::class,
            App\Http\Routes\Member\Post::class,
            App\Http\Routes\Member\PostEpisode::class,
            App\Http\Routes\Member\Account::class,
            App\Http\Routes\Member\Schedule::class,
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | Provider for Route
    |--------------------------------------------------------------------------
    | provider mean of initial application routes.
    | register you're own provider and option here
    | providers must have a child and its declare in routes array as key
    */
    'providers' => [
        'member' => [
            'middleware' => [
                'web', 'auth.check:member', 'alert'
            ],
            'prefix' => 'anggota',
            'as' => 'member.'
        ],
        'web' => [
            'middleware' => 'web'
        ],
        'api' => [
            'middleware' => 'api',
            'prefix' => 'api'
        ],
        'admin' => [
            'middleware' => [
                'web', 'auth.check:admin', 'alert'
            ],
            'prefix' => 'pengurus',
        ],
    ]
];